require './environment'

set :database, {
  adapter: 'postgresql',
  encoding: 'unicode',
  database: 'user_storage_db',
  pool: 5,
  username: 'postgres'
}

Application = Rack::Builder.new do
  map "/" do
    run Api::Root
  end
end

