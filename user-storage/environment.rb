require 'sinatra'
require 'sinatra/activerecord'
require 'grape'
require 'rake'
require 'securerandom'

ROOT_PATH = File.dirname(__FILE__)

begin
  app_dirs = %w[app]
  load_later = []

  Dir[*app_dirs.map { |dir| "#{File.dirname(__FILE__)}/#{dir}/**/*.rb" }].sort.each do |f|
    require f
  rescue
    load_later << f
  end

  load_later.each { |f| require f }
end
