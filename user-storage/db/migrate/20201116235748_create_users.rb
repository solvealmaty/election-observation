class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :phone,              null: false
      t.string :email,              null: false, default: ''
      t.string :encrypted_password, null: false, default: ''
      t.string :first_name,         null: false, default: ''
      t.string :last_name,          null: false, default: ''
      t.string :access_token
      t.string :verification_code
      t.string :session_id

      t.timestamps null: false
    end
  end
end
