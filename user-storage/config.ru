require File.expand_path('application', __dir__)
require 'rack/cors'

run Rack::URLMap.new('/' => Application)

use Rack::Cors do
  allow do
    origins '*'

    resource '*', headers: :any, methods: %i[post options]
  end
end

