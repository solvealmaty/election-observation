module Api
  module V1
    class Users < Grape::API
      format :json

      helpers do
        def authenticate!
          # TODO: figure out how to authenticate
        end
      end

      resources :users do
        desc "Get users"
        get do
          User.all
        end

        desc "Create user"
        params do
          requires :first_name, type: String
          requires :last_name, type: String
          requires :phone, type: String
          requires :email, type: String
        end
        post do
          # TODO: move user creation logic into service object
          user = User.new(params)

          if user.save
            status :created
          else
            {
              status: 403,
              errors: user.errors
            }
          end
        end
      end
    end
  end
end
