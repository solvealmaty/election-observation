require_relative './v1/base'

module Api
  class Root < Grape::API
    format :json
    default_format :json

    mount ::Api::V1::Base => '/v1'
  end
end
