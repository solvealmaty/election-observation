class User < ActiveRecord::Base
  enum role: {
    observer: 0
  }

  PHONE_FORMAT = /\A\d{11}\z/i.freeze

  validates :phone, :access_token, :email, presence: true, uniqueness: true
  validates :phone, format: { with: PHONE_FORMAT }

  before_validation :ensure_access_token!, on: :create
  before_validation :sanitize_phone!, on: :create

  def full_name
    "#{first_name} #{last_name}"
  end

  private

  def sanitize_phone!
    self.phone = phone.gsub(/\D/, '')
  end

  def ensure_access_token!
    self.access_token ||= generate_access_token
  end

  def generate_access_token
    loop do
      token = SecureRandom.urlsafe_base64(15).tr('lIO0', 'sxyz')
      break token unless User.find_by(access_token: token)
    end
  end
end
