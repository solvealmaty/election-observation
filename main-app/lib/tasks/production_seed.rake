namespace :db do
  namespace :production do
    desc 'Seed production envrionment'
    task :seed => :environment do
      Dir[Rails.root.join('db', 'seeds', 'production', '*.rb')].each do |file|
        load file
      rescue ActiveRecord::RecordInvalid => e
        puts e
        puts "Seed: #{file}"
      end
    end
  end
end
