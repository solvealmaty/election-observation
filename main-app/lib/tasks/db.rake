namespace :db do
  desc 'Drop all records and reload the schema'
  task recreate: ['db:drop', 'db:create', 'db:schema:load']
end
