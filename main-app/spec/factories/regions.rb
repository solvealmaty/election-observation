FactoryBot.define do
  factory :region do
    sequence(:name) { |n| "#{Faker::Address.state}-#{n}" }
    country
  end
end
