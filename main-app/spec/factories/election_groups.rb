FactoryBot.define do
  factory :election_group do
    name { Faker::Lorem.word }
    date { Time.zone.now.since(10.days) }
    description { Faker::Lorem.sentence  }
  end
end
