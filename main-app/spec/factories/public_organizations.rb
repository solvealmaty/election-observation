FactoryBot.define do
  factory :public_organization do
    name           { Faker::Name.name }
    iin_bin        { Faker::Number.number(digits: 12) }
    address        { Faker::Address.street_address }
    phone          { Faker::PhoneNumber.phone_number }
    social_network { Faker::Internet.url }

    country

    after :create do |org|
      create_list :user, 1, public_organization: org, role: 'representative'
    end
  end
end
