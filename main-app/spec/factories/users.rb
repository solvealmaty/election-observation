FactoryBot.define do
  factory :user do
    phone         { Faker::PhoneNumber.subscriber_number(length: 11) }
    email         { Faker::Internet.email }
    first_name    { Faker::Name.first_name }
    last_name     { Faker::Name.last_name }
    password      { '12345678' }
    access_token  { Devise.friendly_token(32) }
    confirmed_at  { Time.current }
    identifier    { Faker::Number.number(digits: 8) }

    trait :unconfirmed do
      confirmed_at { nil }
    end
    trait :coordinator do
      role { 'coordinator' }
    end
    trait :representative do
      role { 'representative' }
    end
  end
end
