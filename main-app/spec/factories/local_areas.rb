FactoryBot.define do
  factory :local_area do
    name { Faker::Address.city }
    region
  end
end
