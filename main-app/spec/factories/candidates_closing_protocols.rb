FactoryBot.define do
  factory :candidates_closing_protocol do
    votes { Faker::Number.number(digits: 3) }

    candidate
    closing_protocol
  end
end
