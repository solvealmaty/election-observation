FactoryBot.define do
  factory :election do
    sequence(:name) { |n| "#{Faker::Lorem.word}-#{n}" }
    date { Time.zone.now.since(10.days) }
    status { 'draft' }

    election_group

    trait :with_stations do
      after :create do |election|
        create_list :station, 3, elections: [election], election_group: election.election_group
      end
    end

    trait :with_candidates do
      after :create do |election|
        create_list :candidate, 3, election: election
      end
    end

    trait :ready do
      status { 'ready' }
    end
  end
end
