FactoryBot.define do
  factory :candidate do
    name { Faker::Name.name }
    info { Faker::Lorem.word }
  end
end
