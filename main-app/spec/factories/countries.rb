FactoryBot.define do
  factory :country do
    sequence(:name) { |n| "#{Faker::Address.country}-#{n}" }
  end
end
