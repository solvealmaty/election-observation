FactoryBot.define do
  factory :station do
    sequence(:name) { |n| "УИК №#{n}" }
    sequence(:address) { |n| "#{Faker::Address.street_address}, #{n}" }
    local_area
  end
end
