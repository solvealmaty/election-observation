FactoryBot.define do
  factory :closing_protocol do
    images { [Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/test.jpg'), 'image/jpeg')] }
    status { :unverified }
    total_voters { rand(1000) }
    active_voters { rand(1000) }
    absentee_certificate_voters { rand(1000) }
    outdoors_voters { rand(1000) }
    total_bulletins { rand(1000) }
    voters_bulletins { rand(1000) }
    invalid_bulletins { rand(1000) }
    canceled_bulletins { rand(1000) }
    conflict { false }
    deleted { false }

    station
    election
    user

    after(:build) do |protocol|
      protocol.candidates = protocol.election.candidates
    end
  end
end
