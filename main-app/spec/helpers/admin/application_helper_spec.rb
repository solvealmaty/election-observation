require 'rails_helper'

describe Admin::ApplicationHelper do
  describe '#election_ready?' do
    context 'when election is new' do
      let(:election) { create(:election) }

      it 'returns message that Stations and Candidates not added' do
        expect(election_ready?(election)).to eq '<div><p class="readiness-check">Нет кандидатов</p><p class="readiness-check">Нет участков</p></div>'
      end
    end

    context 'when stations added' do
      let(:election) { create(:election, :with_stations) }

      it 'returns message that Candidates not added' do
        expect(election_ready?(election)).to eq '<div><p class="readiness-check">Нет кандидатов</p><p class="readiness-check"></p></div>'
      end
    end

    context 'when candidates added' do
      let(:election) { create(:election, :with_candidates) }

      it 'returns message that Stations not added' do
        expect(election_ready?(election)).to eq '<div><p class="readiness-check"></p><p class="readiness-check">Нет участков</p></div>'
      end
    end

    context 'when ready' do
      let(:election) { create(:election, :ready) }

      it 'returns' do
        expect(election_ready?(election)).to eq '<i class="fa fa-check-square-o checkmark-icon"></i>'
      end
    end
  end
end
