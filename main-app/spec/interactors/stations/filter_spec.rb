describe Stations::Filter do
  describe '.call' do
    let!(:election_group)   { create(:election_group) }
    let!(:country)          { create(:country) }
    let!(:regions)          { create_list(:region, 2, country: country) }
    let!(:local_area1)      { create(:local_area, region: regions.first) }
    let!(:local_area2)      { create(:local_area, region: regions.second) }
    let!(:elections)        { create_list(:election, 3, :with_candidates, :with_stations, election_group: election_group) }
    let!(:elections2)       { create_list(:election, 3, :with_candidates, :with_stations, election_group: election_group) }
    let!(:orgs)             { create_list(:public_organization, 3) }
    let!(:affiliated_orgs)  { create_list(:public_organization, 3) }
    let!(:observers)        { create_list(:user, 6, :observer) }
    let(:observer)                { create(:user, :observer) }
    let!(:region1_stations) { elections.flat_map(&:stations).sort_by(&:id) }
    let!(:region2_stations) { elections2.flat_map(&:stations).sort_by(&:id) }
    let(:certificate_path)        { Rails.root.join('spec', 'fixtures', 'test.jpg') }

    before do
      region1_stations.each do |station|
        station.update(local_area: local_area1)
        station.reload
      end

      region2_stations.each do |station|
        station.update(local_area: local_area2)
        station.reload
      end

      orgs.union(affiliated_orgs).each.with_index do |org, index|
        election_group.public_organizations << org
        org.users << observers[index]
      end

      observers.each.with_index do |obs, index|
        region1_stations[index].observers << obs
      end

      orgs.each.with_index do |org, index|
        org.candidates << elections[index].candidates.first

        if index == 0
          observers_station = org.users.observer[0].observers_stations[0]
          observers_station.update(certificate: File.open(certificate_path), certification: 'approved')
          observers_station.reload
        end

        if index == 1
          observers_station = org.users.observer[0].observers_stations[0]
          observers_station.update(certificate: File.open(certificate_path))
          observers_station.reload
        end
      end

      affiliated_orgs.each.with_index do |org, index|
        org.candidates << elections[index].candidates.first

        if index == 0
          observers_station = org.users.observer[0].observers_stations[0]
          observers_station.update(certificate: File.open(certificate_path), certification: 'approved')
          observers_station.reload
        end

        if index == 1
          observers_station = org.users.observer[0].observers_stations[0]
          observers_station.update(certificate: File.open(certificate_path))
          observers_station.reload
        end
      end
    end

    context 'when observer has not station AND org type - affiliated AND observer type - certified' do
      it 'returns stations with affiliated and not certified observers' do
        result = ::Stations::Filter.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'affiliated',
          observer_type: 'certified',
          region_id: local_area1.region_id,
          observer_id: observer.id,
          local_area_id: nil,
          station_ids: nil
        )

        filtered_station = Station.includes(observers_stations: [:user])
                                      .where(local_area: local_area1,
                                             observers_stations:
                                               {
                                                 users: { public_organization_id: affiliated_orgs.pluck(:id) },
                                                 certification: 1
                                               }
                                      ).first
        region_stations = Station.where(local_area: local_area1).where.not(id: filtered_station.id)
        filtered_stations_info = [{
                                    id: filtered_station.id,
                                    name: filtered_station.name,
                                    coordinates: filtered_station.coordinates,
                                    address: filtered_station.address,
                                    fullAddress: filtered_station.full_address,
                                    orgs: 'Не указан',
                                    orgType: "Аффилирована с кандидатами",
                                    observerType: "С подтвержденным удостоверением",
                                    isObserverStation: false
                                  }]
        expect(result.stations_info[:filteredStationsInfo]).to eq filtered_stations_info
        expect(result.stations_info[:regionStationsInfo].count).to eq region_stations.count
      end
    end

    context 'when observer has station AND org type - all AND observer type - certified' do
      it 'returns stations with affiliated and not certified observers' do
        result = ::Stations::Filter.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: '',
          observer_type: 'certified',
          region_id: local_area1.region_id,
          observer_id: observer.id,
          local_area_id: nil,
          station_ids: nil
        )

        filtered_stations = Station.includes(observers_stations: [:user])
                             .where(local_area: local_area1,
                                    observers_stations:
                                      {
                                        certification: 1
                                      }
                             )
        region_stations = Station.where(local_area: local_area1).where.not(id: filtered_stations.pluck(:id))
        filtered_stations_info = filtered_stations.map do |station|
          {
            id: station.id,
            name: station.name,
            coordinates: station.coordinates,
            address: station.address,
            fullAddress: station.full_address,
            orgs: 'Не указан',
            orgType: "Все",
            observerType: "С подтвержденным удостоверением",
            isObserverStation: false
          }
        end

        expect(result.stations_info[:filteredStationsInfo]).to eq filtered_stations_info
        expect(result.stations_info[:regionStationsInfo].count).to eq region_stations.count
      end
    end

    context 'when station has 2 observers AND org type - unaffiliated AND observer type - all' do
      it 'returns stations with unaffiliated and not certified observers' do
        org = affiliated_orgs[0]
        org.users << observer
        filtered_stations = Station.includes(observers_stations: [:user])
                              .where(local_area: local_area1,
                                     observers_stations:
                                       {
                                         users: { public_organization_id: orgs.pluck(:id) }
                                       }
                              )
        station = filtered_stations[0]
        observer.stations << station

        correct_filtered_stations = filtered_stations.where.not(id: [station.id])
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))
        current_observer = correct_filtered_stations[0].observers[0]

        filtered_stations_info = correct_filtered_stations.map do |station|
          is_observer_station = station.observers.include?(current_observer) ? true : false
          {
            id: station.id,
            name: station.name,
            coordinates: station.coordinates,
            address: station.address,
            fullAddress: station.full_address,
            orgs: 'Не указан',
            orgType: "Неаффилирована с кандидатами",
            observerType: "Все",
            isObserverStation: is_observer_station
          }
        end

        result = ::Stations::Filter.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'unaffiliated',
          observer_type: '',
          region_id: local_area1.region_id,
          observer_id: current_observer.id,
          local_area_id: nil,
          station_ids: nil
        )

        expect(result.stations_info[:filteredStationsInfo]).to eq filtered_stations_info
        expect(result.stations_info[:regionStationsInfo].count).to eq correct_region_stations.count
      end
    end
  end
end
