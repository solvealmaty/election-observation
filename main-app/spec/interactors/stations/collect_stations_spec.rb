describe Stations::CollectStations do
  describe '.call' do
    let!(:election_group) { create(:election_group) }
    let!(:country) { create(:country) }
    let!(:regions) { create_list(:region, 2, country: country) }
    let!(:local_area1) { create(:local_area, region: regions.first) }
    let!(:local_area2) { create(:local_area, region: regions.second) }
    let!(:elections) { create_list(:election, 3, :with_candidates, :with_stations, election_group: election_group) }
    let!(:elections2) { create_list(:election, 3, :with_candidates, :with_stations, election_group: election_group) }
    let!(:orgs) { create_list(:public_organization, 3) }
    let!(:affiliated_orgs) { create_list(:public_organization, 3) }
    let!(:observers) { create_list(:user, 6, :observer) }
    let(:observer) { create(:user, :observer) }
    let!(:region1_stations) { elections.flat_map(&:stations).sort_by(&:id) }
    let!(:region2_stations) { elections2.flat_map(&:stations).sort_by(&:id) }
    let(:certificate_path) { Rails.root.join('spec', 'fixtures', 'test.jpg') }

    before do
      region1_stations.each do |station|
        station.update(local_area: local_area1)
        station.reload
      end

      region2_stations.each do |station|
        station.update(local_area: local_area2)
        station.reload
      end

      orgs.union(affiliated_orgs).each.with_index do |org, index|
        election_group.public_organizations << org
        org.users << observers[index]
      end

      observers.each.with_index do |obs, index|
        region1_stations[index].observers << obs
      end

      orgs.each.with_index do |org, index|
        org.candidates << elections[index].candidates.first

        if index == 0
          observers_station = org.users.observer[0].observers_stations[0]
          observers_station.update(certificate: File.open(certificate_path), certification: 'approved')
          observers_station.reload
        end

        if index == 1
          observers_station = org.users.observer[0].observers_stations[0]
          observers_station.update(certificate: File.open(certificate_path))
          observers_station.reload
        end
      end

      affiliated_orgs.each.with_index do |org, index|
        org.candidates << elections[index].candidates.first

        if index == 0
          observers_station = org.users.observer[0].observers_stations[0]
          observers_station.update(certificate: File.open(certificate_path), certification: 'approved')
          observers_station.reload
        end

        if index == 1
          observers_station = org.users.observer[0].observers_stations[0]
          observers_station.update(certificate: File.open(certificate_path))
          observers_station.reload
        end
      end
    end

    context 'when stations has 1 observer only AND org type - affiliated AND observer type - all' do
      it 'returns stations with affiliated and not certified observers' do
        affiliated_observers_stations = Station
          .includes(observers_stations: [:user])
          .where(local_area: local_area1,
                 observers_stations:
                   {
                     users: {public_organization_id: affiliated_orgs.pluck(:id)}
                   }
          )
        station = affiliated_observers_stations.first
        observer.update(public_organization: orgs.first)
        observer.stations << station

        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'affiliated',
          observer_type: '',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )


        correct_region_stations = Station.where(local_area: local_area1).where.not(id: affiliated_observers_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq affiliated_observers_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end

    context 'when stations has 1 observer only AND org type - affiliated AND observer type - not certified' do
      it 'returns stations with affiliated and not certified observers' do
        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'affiliated',
          observer_type: 'no_certificate',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations = Station.includes(observers_stations: [:user])
          .where(local_area: local_area1,
                 observers_stations:
                   {
                     users: {public_organization_id: affiliated_orgs.pluck(:id)},
                     certification: 0,
                     certificate: nil
                   }
          )
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end

    context 'when stations has 1 observer only AND org type - affiliated AND observer type - certified' do
      it 'returns stations with affiliated and not certified observers' do
        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'affiliated',
          observer_type: 'certified',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations = Station.includes(observers_stations: [:user])
          .where(local_area: local_area1,
                 observers_stations:
                   {
                     users: {public_organization_id: affiliated_orgs.pluck(:id)},
                     certification: 1
                   }
          )
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end

    context 'when stations has 1 observer only AND org type - affiliated AND observer type - uncertified' do
      it 'returns stations with affiliated and not certified observers' do
        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'affiliated',
          observer_type: 'uncertified',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations = Station.includes(observers_stations: [:user])
          .where(local_area: local_area1,
                 observers_stations:
                   {
                     users: {public_organization_id: affiliated_orgs.pluck(:id)},
                     certification: [0, 2]
                   }
          ).where.not(observers_stations: {certificate: nil})
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end

    context 'when stations has 1 observer only AND org type - unaffiliated AND observer type - all' do
      it 'returns stations with unaffiliated and not certified observers' do
        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'unaffiliated',
          observer_type: '',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations = Station.includes(observers_stations: [:user])
          .where(local_area: local_area1,
                 observers_stations:
                   {
                     users: {public_organization_id: orgs.pluck(:id)}
                   }
          )
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end

    context 'when stations has 1 observer only AND org type - unaffiliated AND observer type - not certified' do
      it 'returns stations with affiliated and not certified observers' do
        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'unaffiliated',
          observer_type: 'no_certificate',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations = Station.includes(observers_stations: [:user])
          .where(local_area: local_area1,
                 observers_stations:
                   {
                     users: {public_organization_id: orgs.pluck(:id)},
                     certification: 0,
                     certificate: nil
                   }
          )
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end

    context 'when stations has 1 observer only AND org type - unaffiliated AND observer type - certified' do
      it 'returns stations with unaffiliated and not certified observers' do
        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'unaffiliated',
          observer_type: 'certified',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations = Station.includes(observers_stations: [:user])
          .where(local_area: local_area1,
                 observers_stations:
                   {
                     users: {public_organization_id: orgs.pluck(:id)},
                     certification: 1
                   }
          )
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end

    context 'when stations has 1 observer only AND org type - unaffiliated AND observer type - uncertified' do
      it 'returns stations with affiliated and not certified observers' do
        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'unaffiliated',
          observer_type: 'uncertified',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations = Station.includes(observers_stations: [:user])
          .where(local_area: local_area1,
                 observers_stations:
                   {
                     users: {public_organization_id: orgs.pluck(:id)},
                     certification: [0, 2]
                   }
          ).where.not(observers_stations: {certificate: nil})
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end

    context 'when station has 2 observers AND org type - unaffiliated AND observer type - all' do
      it 'returns stations with unaffiliated and not certified observers' do
        org = affiliated_orgs[0]
        org.users << observer
        filtered_stations = Station.includes(observers_stations: [:user])
          .where(local_area: local_area1,
                 observers_stations:
                   {
                     users: {public_organization_id: orgs.pluck(:id)}
                   }
          )
        station = filtered_stations[0]
        observer.stations << station

        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: 'unaffiliated',
          observer_type: '',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations = filtered_stations.where.not(id: [station.id])
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end

    context 'when station has 2 observers AND org type - all AND observer type - uncertified' do
      it 'returns stations with unaffiliated and not certified observers' do
        org = orgs[0]
        org.users << observer
        filtered_stations = Station.includes(observers_stations: [:user])
          .where(local_area: local_area1,
                 observers_stations:
                   {
                     users: {public_organization_id: orgs.union(affiliated_orgs).pluck(:id)},
                     certification: [0, 2]
                   }
          ).where.not(observers_stations: {certificate: nil})
        station = filtered_stations[0]
        observer.stations << station
        observers_station = observer.observers_stations[0]
        observers_station.update(certificate: File.open(certificate_path), certification: 'approved')
        observers_station.reload

        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: '',
          observer_type: 'uncertified',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations = filtered_stations.where.not(id: [station.id])
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end

    context 'org type - all AND observer type - all' do
      it 'returns stations with unaffiliated and not certified observers' do
        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [],
          public_organization_type: '',
          observer_type: '',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations_ids = Station.where(local_area: local_area1).pluck(:id).sort

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations_ids
        expect(result.region_stations.pluck(:id).sort).to eq []
      end
    end

    context 'org type - all AND observer type - all AND public_organization_ids are defined' do
      it 'returns stations with unaffiliated and not certified observers' do
        org = orgs[0]
        result = ::Stations::CollectStations.call(
          election_group: election_group,
          public_organization_ids: [org.id],
          public_organization_type: '',
          observer_type: '',
          region_id: local_area1.region_id,
          local_area_id: nil,
          station_ids: nil
        )

        correct_filtered_stations = org.users.observer[0].stations
        correct_region_stations = Station.where(local_area: local_area1).where.not(id: correct_filtered_stations.pluck(:id))

        expect(result.filtered_stations.pluck(:id).sort).to eq correct_filtered_stations.pluck(:id).sort
        expect(result.region_stations.pluck(:id).sort).to eq correct_region_stations.pluck(:id).sort
      end
    end
  end
end
