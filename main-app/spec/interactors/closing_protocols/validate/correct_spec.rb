describe ClosingProtocols::Validate::Correct do
  describe '.call' do
    let!(:election_group) { create(:election_group) }
    let!(:election) { create(:election, election_group: election_group) }
    let!(:candidates) { create_list(:candidate, 3, election: election) }
    let!(:station) { create(:station, election_group: election_group) }
    let!(:user) { create(:user, :observer) }

    context 'with incorrect attributes' do
      let!(:incorrect_protocol) { create(:closing_protocol, election: election, station: station, user: user) }

      it 'updates status to incorrect' do
        result = ::ClosingProtocols::Validate::Correct.call(protocol: incorrect_protocol)
        expect(result.status).to eq :incorrect
      end
    end

    context 'with correct attributes' do
      let(:identical_params) do
        {
          candidates_closing_protocols_attributes:
            [
              {candidate_id: candidates[0].id, votes: 20},
              {candidate_id: candidates[1].id, votes: 60},
              {candidate_id: candidates[2].id, votes: 70}
            ],
          total_voters: 300,
          active_voters: 200,
          absentee_certificate_voters: 50,
          outdoors_voters: 50,
          total_bulletins: 150,
          voters_bulletins: 75,
          invalid_bulletins: 50,
          canceled_bulletins: 75
        }
      end
      let!(:correct_protocol) do
        ClosingProtocol.create(identical_params.merge(election: election, station: station, user: user))
      end

      it 'does not update status to incorrect' do
        result = ::ClosingProtocols::Validate::Correct.call(protocol: correct_protocol)
        expect(result.status).to eq :unverified
      end
    end
  end
end
