describe ClosingProtocols::Approve::Organize do
  describe '.call' do
    let!(:election_group) { create(:election_group) }
    let!(:election) { create(:election, election_group: election_group) }
    let!(:candidates) { create_list(:candidate, 3, election: election) }
    let!(:station) { create(:station, election_group: election_group) }
    let!(:users) { create_list(:user, 2, :observer) }
    let!(:closing_protocols) { create_list(:closing_protocol, 2, election: election, station: station, user: users.first) }

    context 'without existing identicals' do
      it 'updates status only for given protocol' do
        ::ClosingProtocols::Approve::Organize.call(protocol: closing_protocols.first)

        expect(closing_protocols.first.reload.correct?).to be_truthy
        expect(closing_protocols.last.reload.correct?).to be_falsey
      end
    end

    context 'with existing identicals' do
      let(:identical_params) do
        {
          candidates_closing_protocols_attributes:
            [
              {candidate_id: candidates[0].id, votes: 20},
              {candidate_id: candidates[1].id, votes: 60},
              {candidate_id: candidates[2].id, votes: 70}
            ],
          total_voters: 300,
          active_voters: 200,
          absentee_certificate_voters: 50,
          outdoors_voters: 50,
          total_bulletins: 150,
          voters_bulletins: 75,
          invalid_bulletins: 50,
          canceled_bulletins: 75
        }
      end
      let!(:identical_protocols) do
        users.map do |user|
          ClosingProtocol.create(identical_params.merge(election: election, station: station, user: user))
        end
      end

      it 'updates status for identical protocols' do
        ::ClosingProtocols::Approve::Organize.call(protocol: identical_protocols.first)

        expect(identical_protocols.first.reload.correct?).to be_truthy
        expect(identical_protocols.last.reload.correct?).to be_truthy
      end
    end
  end
end
