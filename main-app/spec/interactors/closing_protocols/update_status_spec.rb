describe ClosingProtocols::UpdateStatus do
  describe '.call' do
    let!(:election_group) { create(:election_group) }
    let!(:election) { create(:election, election_group: election_group) }
    let!(:candidates) { create_list(:candidate, 3, election: election) }
    let!(:station) { create(:station, election_group: election_group) }
    let!(:user) { create(:user, :observer) }
    let!(:closing_protocol) { create(:closing_protocol, election: election, station: station, user: user) }

    context 'with valid params' do
      it 'updates status to correct' do
        ::ClosingProtocols::UpdateStatus.call(protocol: closing_protocol, status: :correct)
        expect(closing_protocol.reload.correct?).to be_truthy
      end
    end

    context 'with invalid params' do
      it 'updates status to correct' do
        ::ClosingProtocols::UpdateStatus.call(protocol: closing_protocol, status: nil)
        expect(closing_protocol.reload.correct?).to be_falsey
      end
    end
  end
end
