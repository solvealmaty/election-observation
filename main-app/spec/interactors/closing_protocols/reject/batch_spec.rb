describe ClosingProtocols::Reject::Batch do
  subject(:result) { ::ClosingProtocols::Reject::Batch.call(protocol: protocol) }

  describe '.call' do
    let!(:election_group) { create(:election_group) }
    let!(:election) { create(:election, election_group: election_group) }
    let!(:candidates) { create_list(:candidate, 3, election: election) }
    let!(:station) { create(:station, election_group: election_group) }
    let!(:user) { create(:user, :observer) }
    let!(:closing_protocols) { create_list(:closing_protocol, 3, election: election, station: station, user: user) }
    let(:protocol) { closing_protocols.first }

    context 'user created multiple protocols' do
      it 'rejects given protocol' do
        expect(protocol.incorrect?).to be_falsey

        expect { result }.to change { protocol.reload.status }
        expect(protocol.incorrect?).to be_truthy
      end

      it 'rejects all protocols of the user' do
        closing_protocols.each do |protocol|
          expect(protocol.incorrect?).to be_falsey
        end

        expect { result }.to change { protocol.reload.status }

        closing_protocols.each do |protocol|
          expect(protocol.reload.incorrect?).to be_truthy
        end
      end
    end
  end
end
