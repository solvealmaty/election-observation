describe ClosingProtocols::Create do
  include ActionDispatch::TestProcess::FixtureFile

  describe '.call' do
    let!(:country) { create(:country) }
    let!(:region) { create(:region, country: country) }
    let!(:local_area) { create(:local_area, region: region) }
    let!(:election_group) { create(:election_group) }
    let!(:elections) { create_list(:election, 2, election_group: election_group) }
    let!(:election1_candidates) { create_list(:candidate, 3, election: elections.first) }
    let!(:election2_candidates) { create_list(:candidate, 3, election: elections.last) }
    let!(:stations) { create_list(:station, 2, local_area: local_area, election_group: election_group) }
    let!(:user) { create(:user, :observer) }
    let!(:user2) { create(:user, :observer) }

    let(:election) { elections.first }
    let(:user_station) { stations.first }

    let(:image_file) { fixture_file_upload(Rails.root.join('spec/fixtures/test.jpg')) }
    let(:correct_protocol_params) do
      {
        station_id: user_station.id,
        election_id: election.id,
        candidates_closing_protocols_attributes:
          [
            {candidate_id: election1_candidates[0].id, votes: 20},
            {candidate_id: election1_candidates[1].id, votes: 60},
            {candidate_id: election1_candidates[2].id, votes: 70}
          ],
        total_voters: 300,
        active_voters: 200,
        absentee_certificate_voters: 50,
        outdoors_voters: 50,
        total_bulletins: 150,
        voters_bulletins: 75,
        invalid_bulletins: 50,
        canceled_bulletins: 75,
        images: [image_file]
      }
    end

    let(:correct_protocol_params2) do
      {
        total_voters: 400,
        active_voters: 300,
        invalid_bulletins: 150
      }
    end

    let(:incorrect_protocol_params) do
      {
        station_id: user_station.id,
        election_id: election.id,
        candidates_closing_protocols_attributes:
          [
            {candidate_id: election1_candidates[0].id, votes: 50},
            {candidate_id: election1_candidates[1].id, votes: 90},
            {candidate_id: election1_candidates[2].id, votes: 100}
          ],
        total_voters: 100,
        active_voters: 50,
        absentee_certificate_voters: 50,
        outdoors_voters: 50,
        total_bulletins: 50,
        voters_bulletins: 100,
        invalid_bulletins: 50,
        canceled_bulletins: 100,
        images: [image_file]
      }
    end

    before do
      district = region.create_electoral_district(level: 2)
      district.candidates << election.candidates
      district.elections << election
      district.save
      district.reload
    end

    context 'with correct params' do
      context 'given correct params' do
        subject(:result) { ::ClosingProtocols::Create.call(params: correct_protocol_params, current_user: user) }

        it 'creates a new closing protocol' do
          expect { result }.to change(ClosingProtocol, :count).from(0).to(1)
        end

        it 'creates multiple candidates closing protocols objects' do
          expect { result }.to change(CandidatesClosingProtocol, :count)
                                 .from(0)
                                 .to(correct_protocol_params[:candidates_closing_protocols_attributes].size)

          CandidatesClosingProtocol.order(:candidate_id).each_with_index do |cp, index|
            expect(cp.closing_protocol).to eq result.protocol
            expect(cp.votes).to eq correct_protocol_params[:candidates_closing_protocols_attributes][index][:votes]
          end
        end

        it 'creates association with the observer' do
          expect(result.protocol.user).to eq user
        end

        it 'provides correct comparable values' do
          expect(result.protocol.station).to eq user_station
          expect(result.protocol.election).to eq election

          ClosingProtocol::COMPARABLE_ATTRIBUTES.each do |attr|
            expect(result.protocol.send(attr)).to eq correct_protocol_params[attr]
          end
        end

        it 'provides unverified status' do
          expect(result.protocol.status).to eq 'unverified'
        end

        it 'provides no conflict' do
          expect(result.protocol.conflict).to eq false
        end

        it 'provides images' do
          expect(result.protocol.images.first.identifier).to eq 'test.jpg'
        end
      end

      context 'given existing protocols with conflicted params' do
        subject(:correct_result) { ::ClosingProtocols::Create.call(params: correct_protocol_params, current_user: user) }

        before do
          ::ClosingProtocols::Create.call(
            params: correct_protocol_params.merge(correct_protocol_params2),
            current_user: user2
          )
        end

        it 'creates a new protocol' do
          expect { correct_result }.to change(ClosingProtocol, :count).from(1).to(2)
        end

        it 'provides unverified status' do
          expect(correct_result.protocol.reload.status).to eq 'unverified'
        end

        it 'provides conflict' do
          expect(correct_result.protocol.reload.conflict).to eq true
        end
      end

      context 'given existing protocols with identical params' do
        subject(:identical_result) {
          ::ClosingProtocols::Create.call(params: correct_protocol_params, current_user: user)
        }

        before do
          result = ::ClosingProtocols::Create.call(params: correct_protocol_params, current_user: user2)
          result.protocol.correct!
        end

        it 'creates identical closing protocol with correct status' do
          expect(identical_result.protocol.correct?).to eq true
        end

        it 'creates identical closing protocol with no conflicts' do
          expect(identical_result.protocol.conflict).to eq false
        end
      end
    end

    context 'with incorrect params' do
      context 'given incorrect params' do
        subject(:incorrect_result) {
          ::ClosingProtocols::Create.call(params: incorrect_protocol_params, current_user: user)
        }

        it 'creates a new closing protocol' do
          expect { incorrect_result }.to change(ClosingProtocol, :count).from(0).to(1)
        end

        it 'creates multiple candidates closing protocols objects' do
          expect { incorrect_result }.to change(CandidatesClosingProtocol, :count)
                                           .from(0)
                                           .to(incorrect_protocol_params[:candidates_closing_protocols_attributes].size)

          CandidatesClosingProtocol.order(:candidate_id).each_with_index do |cp, index|
            expect(cp.closing_protocol).to eq incorrect_result.protocol
            expect(cp.votes).to eq incorrect_protocol_params[:candidates_closing_protocols_attributes][index][:votes]
          end
        end

        it 'creates association with the observer' do
          expect(incorrect_result.protocol.user).to eq user
        end

        it 'provides correct comparable values' do
          expect(incorrect_result.protocol.station).to eq user_station
          expect(incorrect_result.protocol.election).to eq election

          ClosingProtocol::COMPARABLE_ATTRIBUTES.each do |attr|
            expect(incorrect_result.protocol.send(attr)).to eq incorrect_protocol_params[attr]
          end
        end

        it 'provides incorrect status' do
          expect(incorrect_result.protocol.status).to eq 'incorrect'
        end

        it 'provides no conflict' do
          expect(incorrect_result.protocol.conflict).to eq false
        end
      end

      context 'given existing protocols with incorrect params' do
        subject(:incorrect_result) { ::ClosingProtocols::Create.call(params: incorrect_protocol_params, current_user: user) }

        before do
          ::ClosingProtocols::Create.call(
            params: incorrect_protocol_params.merge(correct_protocol_params2),
            current_user: user2
          )
        end

        it 'creates a new protocol' do
          expect { incorrect_result }.to change(ClosingProtocol, :count).from(1).to(2)
        end

        it 'provides unverified status' do
          expect(incorrect_result.protocol.reload.status).to eq 'incorrect'
        end

        it 'provides conflict' do
          expect(incorrect_result.protocol.reload.conflict).to eq false
        end
      end

      context 'given existing protocols with conflicted params' do
        subject(:conflicted_result) {
          ::ClosingProtocols::Create.call(params: correct_protocol_params, current_user: user)
        }

        before do
          result = ::ClosingProtocols::Create.call(params: incorrect_protocol_params, current_user: user2)
          result.protocol.unverified!
        end

        it 'creates identical closing protocol with correct status' do
          expect(conflicted_result.protocol.reload.status).to eq 'unverified'
        end

        it 'creates identical closing protocol with no conflicts' do
          expect(conflicted_result.protocol.reload.conflict).to eq true
        end
      end
    end
  end
end
