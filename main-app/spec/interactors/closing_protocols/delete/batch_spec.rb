describe ClosingProtocols::Delete::Batch do
  subject(:result) { ::ClosingProtocols::Delete::Batch.call(protocol_ids: protocol_ids) }

  describe '.call' do
    let!(:election_group) { create(:election_group) }
    let!(:election) { create(:election, election_group: election_group) }
    let!(:candidates) { create_list(:candidate, 3, election: election) }
    let!(:station) { create(:station, election_group: election_group) }
    let!(:user) { create(:user, :observer) }
    let!(:closing_protocols) { create_list(:closing_protocol, 3, election: election, station: station, user: user) }
    let(:protocol_ids) { closing_protocols.map(&:id) }

    context 'delete multiple records' do
      it 'sets deleted: true to all given protocols' do
        closing_protocols.each do |protocol|
          expect(protocol.deleted).to eq false
        end

        result

        closing_protocols.each do |protocol|
          expect(protocol.reload.deleted).to eq true
        end
      end
    end
  end
end
