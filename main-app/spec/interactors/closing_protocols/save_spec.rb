describe ClosingProtocols::Save do
  include ActionDispatch::TestProcess::FixtureFile

  describe '.call' do
    let!(:election_group) { create(:election_group) }
    let!(:election) { create(:election, election_group: election_group) }
    let!(:candidates) { create_list(:candidate, 3, election: election) }
    let!(:stations) { create_list(:station, 2, election_group: election_group) }
    let!(:user) { create(:user, :observer) }
    let(:image_file) { fixture_file_upload(Rails.root.join('spec/fixtures/test.jpg')) }
    let(:correct_protocol_params) do
      {
        station_id: user_station.id,
        election_id: election.id,
        candidates_closing_protocols_attributes:
          [
            {candidate_id: candidates[0].id, votes: 20},
            {candidate_id: candidates[1].id, votes: 60},
            {candidate_id: candidates[2].id, votes: 70}
          ],
        total_voters: 300,
        active_voters: 200,
        absentee_certificate_voters: 50,
        outdoors_voters: 50,
        total_bulletins: 150,
        voters_bulletins: 75,
        invalid_bulletins: 50,
        canceled_bulletins: 75,
        images: [image_file]
      }
    end

    let(:user_station) { stations.first }

    context 'given valid params' do
      subject(:result) { ::ClosingProtocols::Save.call(params: correct_protocol_params, current_user: user) }

      it 'succeeds' do
        expect(result.success?).to be_truthy
      end

      it 'creates closing protocol' do
        expect { result }.to change(ClosingProtocol, :count).from(0).to(1)
      end

      it 'creates corresponding candidates closing protocols' do
        expect { result }.to change(CandidatesClosingProtocol, :count)
                               .from(0)
                               .to(correct_protocol_params[:candidates_closing_protocols_attributes].size)
      end
    end

    context 'given invalid params' do
      context 'no candidates' do
        let(:no_candidates_params) do
          correct_protocol_params[:candidates_closing_protocols_attributes] = []
          correct_protocol_params
        end

        subject(:invalid_result) { ::ClosingProtocols::Save.call(params: no_candidates_params, current_user: user) }

        it 'does not create new closing protocol' do
          expect { invalid_result }.to raise_error(ActiveRecord::RecordInvalid)
          expect(ClosingProtocol.count).to eq 0
        end

        it 'does not create candidates closing protocols' do
          expect { invalid_result }.to raise_error(ActiveRecord::RecordInvalid)
          expect(CandidatesClosingProtocol.count).to eq 0
        end
      end

      context 'invalid images amount' do
        let(:invalid_images_params) do
          correct_protocol_params[:images] = Array.new(ClosingProtocol::MAX_IMAGES_AMOUNT + 1) { image_file }
          correct_protocol_params
        end

        subject(:invalid_result) { ::ClosingProtocols::Save.call(params: invalid_images_params, current_user: user) }

        it 'does not create create closing protocol' do
          expect { invalid_result }.to raise_error(ActiveRecord::RecordInvalid)
        end
      end
    end
  end
end
