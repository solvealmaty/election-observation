require 'rails_helper'

feature 'manage election groups' do
  let!(:org)            { create(:public_organization) }
  let!(:representative) { org.users.representative.first }
  let!(:election_group) { create(:election_group) }
  let!(:elections)      { create_list(:election, 2, :with_stations, election_group: election_group) }
  let!(:users)          { create_list(:user, 2, :observer, public_organization: org) }
  let(:coordinator)           { create(:user, :coordinator, public_organization: org) }
  let(:certificate_path)      { Rails.root.join('spec', 'fixtures', 'test.jpg') }

  before do
    org.election_groups << election_group
    users.each.with_index do |user, index|
      elections[index].stations.first.observers << user
    end
  end

  scenario 'show all election group region observers' do
    as_user(representative) do
      visit cabinet_election_groups_path

      expect(page).to have_content election_group.name

      region = election_group.stations.map(&:region).first

      expect(page).to have_content region.name

      click_on "btn-group-#{election_group.id}-region-observers-#{region.id}"
      expect(page).to have_current_path cabinet_election_group_observers_stations_path(election_group, region_id: region.id)

      observers_stations = election_group.stations
                             .where(local_area_id: region.local_area_ids)
                             .map(&:observers_stations).flatten

      observers_stations.each do |observer_station|
        certificate_existence = observer_station.certificate.present? ? 'has_certificate' : 'no_certificate'
        [
          observer_station.user.username,
          observer_station.station.name,
          I18n.t("cabinet.observers_stations.certification.#{certificate_existence}"),
          I18n.t("activerecord.attributes.observers_station.certification_status.#{observer_station.certification}"),
          I18n.l(observer_station.created_at, format: :ddmmyy, default: '')
        ].each do |observer_station_info|
          expect(page).to have_content observer_station_info
        end

        expect(page).to have_link(href: "/cabinet/election_groups/#{election_group.id}/observers_stations/#{observer_station.id}?region_id=#{region.id}")
      end
    end
  end

  scenario 'show observers_station details' do
    as_user(coordinator) do
      observers_station = users.first.observers_stations.first
      observers_station.update(certificate: File.open(certificate_path))
      observers_station.reload
      region = observers_station.station.region

      visit cabinet_election_group_observers_stations_path(election_group, region_id: region.id)

      click_on "show-observers-station-#{observers_station.id}"
      expect(page).to have_current_path cabinet_election_group_observers_station_path(election_group, observers_station, region_id: region.id)

      expect(page).to have_content I18n.t('cabinet.pages.observers_stations.show.title')
      expect(page).to have_css("img[src*='#{observers_station.certificate.url}']")

      [
        observers_station.user.identifier,
        observers_station.station.name,
        I18n.t("activerecord.attributes.observers_station.certification_status.unapproved")
      ].each do |observers_station_info|
        expect(page).to have_content observers_station_info
      end

      expect(page).to have_link(href: "/cabinet/election_groups/#{election_group.id}/observers_stations/#{observers_station.id}/approve?region_id=#{region.id}")
    end
  end

  scenario 'approve observers_station certificate' do
    as_user(coordinator) do
      observers_station = users.first.observers_stations.first
      observers_station.update(certificate: File.open(certificate_path))
      observers_station.reload
      region = observers_station.station.region

      visit cabinet_election_group_observers_station_path(election_group, observers_station, region_id: region.id)

      expect(observers_station.approved?).to eq false

      click_on "btn-approve-certificate-#{observers_station.id}"

      expect(page).to have_current_path cabinet_election_group_observers_station_path(election_group, observers_station, region_id: region.id)

      expect(observers_station.reload.approved?).to eq true

      [
        I18n.t("activerecord.attributes.observers_station.certification_status.approved"),
        observers_station.accreditor.full_name || observers_station.accreditor.email
      ].each { |approving_detail| expect(page).to have_content approving_detail }
    end
  end

  scenario 'reject observers_station certificate' do
    as_user(coordinator) do
      observers_station = users.first.observers_stations.first
      observers_station.update(certificate: File.open(certificate_path))
      observers_station.reload
      region = observers_station.station.region

      visit cabinet_election_group_observers_station_path(election_group, observers_station, region_id: region.id)

      expect(observers_station.rejected?).to eq false

      within "form#reject_certificate_#{observers_station.id}" do
        fill_in 'observers_station[rejection_reason]', with: 'Some reason'
        click_on "btn-reject-certificate-#{observers_station.id}"
      end

      expect(page).to have_current_path cabinet_election_group_observers_station_path(election_group, observers_station, region_id: region.id)

      expect(observers_station.reload.rejected?).to eq true

      [
        I18n.t("activerecord.attributes.observers_station.certification_status.rejected"),
        observers_station.accreditor.full_name || observers_station.accreditor.email,
        'Some reason'
      ].each { |rejecting_detail| expect(page).to have_content rejecting_detail }
    end
  end
end
