require 'rails_helper'

feature 'manage election groups' do
  let!(:org)            { create(:public_organization) }
  let!(:representative) { org.users.representative.first }
  let!(:election_group) { create(:election_group) }
  let!(:elections)      { create_list(:election, 2, :with_stations, election_group: election_group) }
  let!(:users)          { create_list(:user, 2, :observer, public_organization: org) }

  before do
    org.election_groups << election_group
    users.each.with_index do |user, index|
      elections[index].stations.first.observers << user
    end
  end

  scenario 'all election groups', js: true do
    as_user(representative) do
      visit cabinet_election_groups_path

      expect(page).to have_content election_group.name
      expect(page).to have_content I18n.l(election_group.date, format: :ddmmyy, default: '')
      expect(page).to have_content election_group.description
    end
  end
end
