require 'rails_helper'

feature 'manage public organization users' do
  let!(:org)            { create(:public_organization) }
  let!(:representative) { org.users.representative.first }
  let!(:election_group) { create(:election_group) }
  let!(:elections)      { create_list(:election, 2, :with_stations, election_group: election_group) }
  let!(:observers)      { create_list(:user, 2, :observer, public_organization: org) }
  let!(:coordinators)   { create_list(:user, 2, :coordinator, public_organization: org) }

  scenario 'show all organization users' do
    as_user(representative) do
      visit cabinet_users_path

      expect(page).to have_content representative.full_name
      expect(page).to have_content representative.email
      expect(page).to have_content I18n.t("activerecord.attributes.user.role_types.representative")

      coordinators.each do |coord|
        expect(page).to have_content coord.full_name
        expect(page).to have_content coord.email
        expect(page).to have_content I18n.t("activerecord.attributes.user.role_types.coordinator")
      end

      observers.each do |observer|
        expect(page).to have_content observer.full_name
        expect(page).to have_content observer.email
        expect(page).to have_content I18n.t("activerecord.attributes.user.role_types.observer")
      end
    end
  end

  scenario 'create organization users' do
    as_user(representative) do
      visit cabinet_users_path

      expect(page).to_not have_content 'New Rep'
      expect(page).to_not have_content 'newrep@example.com'

      click_on 'new_org_user'

      expect(page).to have_current_path new_cabinet_user_path

      within "form#new_observer" do
        fill_in 'user[first_name]', with: 'New'
        fill_in 'user[last_name]', with: 'Rep'
        fill_in 'user[email]', with: 'newrep@example.com'

        click_on I18n.t('cabinet.buttons.save')
      end

      expect(page).to have_current_path cabinet_users_path

      within "tr#tr-org-user-#{org.users.last.id}" do
        expect(page).to have_content 'New Rep'
        expect(page).to have_content 'newrep@example.com'
        expect(page).to have_content I18n.t("activerecord.attributes.user.role_types.coordinator")
      end
    end
  end

  scenario 'edit organization users' do
    as_user(representative) do
      visit cabinet_users_path

      coordinator = coordinators.first

      expect(page).to_not have_content 'Edited Repname'

      click_on "btn-edit-user-#{coordinator.id}"

      expect(page).to have_current_path edit_cabinet_user_path(coordinator)

      within "form#edit_org_user_#{coordinator.id}" do
        fill_in 'user[first_name]', with: 'Edited'
        fill_in 'user[last_name]', with: 'Repname'

        click_on I18n.t('cabinet.buttons.save')
      end

      expect(page).to have_current_path cabinet_users_path

      within "tr#tr-org-user-#{coordinator.id}" do
        expect(page).to have_content 'Edited Repname'
      end
    end
  end

  scenario 'destroy organization users' do
    as_user(representative) do
      visit cabinet_users_path

      coordinator = coordinators.first

      expect(page).to have_content coordinator.full_name
      expect(page).to have_content coordinator.email

      click_on "btn-remove-user-#{coordinator.id}"

      expect(page).to_not have_content coordinator.full_name
      expect(page).to_not have_content coordinator.email
    end
  end
end
