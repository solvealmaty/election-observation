def translate_attribute(attr, value)
  I18n.t("activerecord.attributes.closing_protocol.#{attr}.#{value}")
end

feature 'cabinet' do
  feature 'manage closing protocols' do
    let!(:election_group) { create(:election_group) }
    let!(:org) { create(:public_organization) }
    let!(:elections) { create_list(:election, 2, election_group: election_group) }
    let!(:election1_candidates) { create_list(:candidate, 3, election: elections.first) }
    let!(:election2_candidates) { create_list(:candidate, 3, election: elections.last) }
    let!(:stations) { create_list(:station, 2, election_group: election_group) }
    let!(:observers) { create_list(:user, 2, :observer) }
    let!(:coordinator) { create(:user, :coordinator, public_organization: org) }
    let!(:closing_protocol1) {
      create(:closing_protocol, station: stations.first, election: elections.first, user: observers.first)
    }
    let!(:closing_protocol2) {
      create(:closing_protocol, station: stations.last, election: elections.last, user: observers.last)
    }

    let(:election) { elections.first }
    let(:observer) { observers.first }

    scenario 'coordinator can see the list of closing protocols' do
      as_user(coordinator) do
        visit cabinet_closing_protocols_path

        expect(page).to have_css('#base_filter_form_wrapper', visible: true)
        expect(page).to have_css('#search_form_wrapper', visible: true)
        expect(page).to have_css('#closing_protocols', visible: true)

        within '#closing_protocols' do
          elections.each { |election| expect(page).to have_content election.name }
          stations.each do |station|
            expect(page).to have_content station.name
            expect(page).to have_content station.local_area.region.name
            expect(page).to have_content station.local_area.name
          end

          expect(page).to have_content translate_attribute('statuses', 'unverified')
          expect(page).to have_content translate_attribute('conflict', 'false')
          expect(page).to_not have_content translate_attribute('statuses', 'correct')
          expect(page).to_not have_content translate_attribute('statuses', 'incorrect')
          expect(page).to_not have_content translate_attribute('conflict', 'true')
        end
      end
    end

    scenario 'coordinator can see the single closing protocol' do
      as_user(coordinator) do
        visit cabinet_closing_protocols_path

        find("a[href='/cabinet/closing_protocols/#{closing_protocol1.id}']").click

        within '#closing_protocol_image_wrapper' do
          expect(find('img')['src']).to have_content closing_protocol1.images.first.identifier
        end

        within '#closing_protocol_base_info' do
          expect(page).to have_content election.name
          expect(page).to have_content observer.identifier
          expect(page).to have_content observer.full_name
          expect(page).to have_content translate_attribute('statuses', closing_protocol1.status.to_s)
          expect(page).to have_content translate_attribute('conflict', closing_protocol1.conflict.to_s)
        end

        within '#closing_protocol_candidates_details' do
          election.candidates.each do |c|
            expect(page).to have_content c.name
            expect(page).to have_content c.candidates_closing_protocols.first.votes
          end
        end

        within '#closing_protocol_bulletins_details' do
          %w[total_bulletins voters_bulletins invalid_bulletins canceled_bulletins].each do |attr|
            expect(page).to have_content I18n.t("activerecord.attributes.closing_protocol.#{attr}")
            expect(page).to have_content closing_protocol1.send(attr)
          end
        end

        within '#closing_protocol_voters_details' do
          %w[total_voters active_voters absentee_certificate_voters outdoors_voters].each do |attr|
            expect(page).to have_content I18n.t("activerecord.attributes.closing_protocol.#{attr}")
            expect(page).to have_content closing_protocol1.send(attr)
          end
        end
      end
    end

    scenario 'coordinator can approve closing protocol' do
      as_user(coordinator) do
        visit cabinet_closing_protocol_path(closing_protocol1)

        within '.closing-protocol-status-info' do
          expect(page).to have_content translate_attribute('statuses', 'unverified')
        end

        click_on "approve_closing_protocol_#{closing_protocol1.id}"

        within '.closing-protocol-status-info' do
          expect(page).to_not have_content translate_attribute('statuses', 'unverified')
          expect(page).to have_content translate_attribute('statuses', 'correct')
        end
      end
    end

    scenario 'coordinator can reject closing protocol' do
      as_user(coordinator) do
        visit cabinet_closing_protocol_path(closing_protocol1)

        within '.closing-protocol-status-info' do
          expect(page).to have_content translate_attribute('statuses', 'unverified')
        end

        click_on "reject_closing_protocol_#{closing_protocol1.id}"

        within '.closing-protocol-status-info' do
          expect(page).to_not have_content translate_attribute('statuses', 'unverified')
          expect(page).to have_content translate_attribute('statuses', 'incorrect')
        end
      end
    end

    scenario 'coordinator can batch reject closing protocol by user' do
      as_user(coordinator) do
        visit cabinet_closing_protocol_path(closing_protocol1)

        within '.closing-protocol-status-info' do
          expect(page).to have_content translate_attribute('statuses', 'unverified')
        end

        click_on "batch_reject_closing_protocol_#{closing_protocol1.id}"

        expect(page).to have_current_path cabinet_closing_protocols_path

        within "tr#closing-protocol-info-#{closing_protocol1.id}" do
          expect(page).to have_content translate_attribute('statuses', 'incorrect')
        end
      end
    end

    scenario 'coordinator can mark closing protocol as deleted' do
      as_user(coordinator) do
        visit cabinet_closing_protocol_path(closing_protocol1)

        click_on "delete_closing_protocol_#{closing_protocol1.id}"

        expect(page).to have_current_path cabinet_closing_protocols_path
        expect(page).to_not have_css "#closing-protocol-info-#{closing_protocol1.id}"
      end
    end
  end
end
