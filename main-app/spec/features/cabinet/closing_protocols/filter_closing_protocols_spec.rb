def clear_selectors(selectors)
  page.execute_script("$('#{selectors}').val(null).trigger('change');")
end

feature 'cabinet' do
  feature 'manage closing protocols' do
    let!(:country) { create(:country) }
    let!(:region1) { create(:region, country: country) }
    let!(:region2) { create(:region, country: country) }
    let!(:local_area1) { create(:local_area, region: region1) }
    let!(:local_area2) { create(:local_area, region: region2) }
    let!(:election_group) { create(:election_group) }
    let!(:org1) { create(:public_organization) }
    let!(:org2) { create(:public_organization) }
    let!(:elections) { create_list(:election, 2, election_group: election_group) }
    let!(:election1_candidates) { create_list(:candidate, 3, election: elections.first) }
    let!(:election2_candidates) { create_list(:candidate, 3, election: elections.last) }
    let!(:stations1) { create_list(:station, 10, election_group: election_group, local_area: local_area1) }
    let!(:stations2) { create_list(:station, 10, election_group: election_group, local_area: local_area2) }
    let!(:observer1) { create(:user, :observer, public_organization: org1) }
    let!(:observer2) { create(:user, :observer, public_organization: org2) }
    let!(:coordinator) { create(:user, :coordinator, public_organization: org1) }
    let!(:closing_protocols1) do
      stations1[0..5].map do |station|
        create(:closing_protocol, station: station, election: elections.first, user: observer1)
      end
    end
    let!(:closing_protocols2) do
      stations1[5..(stations1.size - 1)].map do |station|
        create(:closing_protocol, station: station, election: elections.last, user: observer2)
      end
    end
    let!(:closing_protocols3) do
      stations2.map do |station|
        create(:closing_protocol, station: station, election: elections.first, user: observer1)
      end
    end

    let(:election1) { elections.first }
    let(:election2) { elections.last }
    let(:all_protocols) { closing_protocols1 + closing_protocols2 + closing_protocols3 }

    scenario 'filter by election', js: true do
      as_user(coordinator) do
        visit cabinet_closing_protocols_path
        select election1.name, from: 'election_id'

        click_on 'submit_closing_protocols_filter'
        wait_for_ajax

        protocol_rows = all('#closing_protocols tr')

        expect(protocol_rows.size).to eq (closing_protocols1 + closing_protocols3).size + 1
        protocol_rows.each_with_index do |row, index|
          next if index.zero?
          expect(row.text).to include(election1.name)
        end
      end
    end

    scenario 'filter by election + public org', js: true do
      # updates half of the protocols with the new user from org2
      closing_protocols1.each_with_index do |p, index|
        next if index <= 2
        p.update!(user: observer2)
      end

      as_user(coordinator) do
        visit cabinet_closing_protocols_path
        select election1.name, from: 'election_id'
        select org1.name, from: 'public_organization_id'

        expect(all('#closing_protocols tr').size).to eq all_protocols.size + 1

        click_on 'submit_closing_protocols_filter'
        wait_for_ajax

        protocol_rows = all('#closing_protocols tr')

        expect(protocol_rows.size).to eq (closing_protocols1[0..2] + closing_protocols3).size + 1
        protocol_rows.each_with_index do |row, index|
          next if index.zero?
          expect(row.text).to include(election1.name)
          expect(row.text).to include(org1.name)
          expect(row.text).to_not include(org2.name)
        end
      end
    end

    scenario 'filter by election + region', js: true do
      as_user(coordinator) do
        visit cabinet_closing_protocols_path

        expect(all('#closing_protocols tr').size).to eq all_protocols.size + 1

        click_on 'reset_closing_protocols_filter'
        wait_for_ajax

        select election1.name, from: 'election_id'
        select region2.name, from: 'protocol_select_region'
        wait_for_ajax

        clear_selectors('#protocol_select_area, #protocol_select_station')

        click_on 'submit_closing_protocols_filter'
        wait_for_ajax

        protocol_rows = all('#closing_protocols tr')

        expect(protocol_rows.size).to eq closing_protocols3.size + 1
        protocol_rows.each_with_index do |row, index|
          next if index.zero?
          expect(row.text).to include(election1.name)
          expect(row.text).to include(org1.name)
          expect(row.text).to include(region2.name)
          expect(row.text).to_not include(org2.name)
          expect(row.text).to_not include(region1.name)
        end
      end
    end

    scenario 'filter by election + region + org', js: true do
      as_user(coordinator) do
        visit cabinet_closing_protocols_path

        expect(all('#closing_protocols tr').size).to eq all_protocols.size + 1

        click_on 'reset_closing_protocols_filter'
        wait_for_ajax

        select election1.name, from: 'election_id'
        select region1.name, from: 'protocol_select_region'
        select org1.name, from: 'public_organization_id'
        wait_for_ajax

        clear_selectors('#protocol_select_area, #protocol_select_station')

        click_on 'submit_closing_protocols_filter'
        wait_for_ajax

        protocol_rows = all('#closing_protocols tr')

        expect(protocol_rows.size).to eq closing_protocols1.size + 1
        protocol_rows.each_with_index do |row, index|
          next if index.zero?
          expect(row.text).to include(election1.name)
          expect(row.text).to include(org1.name)
          expect(row.text).to include(region1.name)
        end
      end
    end

    scenario 'filter by election + region + local_area', js: true do
      as_user(coordinator) do
        visit cabinet_closing_protocols_path

        expect(all('#closing_protocols tr').size).to eq all_protocols.size + 1

        click_on 'reset_closing_protocols_filter'
        wait_for_ajax

        select election1.name, from: 'election_id'
        select region2.name, from: 'protocol_select_region'
        wait_for_ajax
        select local_area2.name, from: 'protocol_select_area'
        wait_for_ajax

        clear_selectors('#protocol_select_station')

        click_on 'submit_closing_protocols_filter'
        wait_for_ajax

        protocol_rows = all('#closing_protocols tr')

        expect(protocol_rows.size).to eq closing_protocols3.size + 1
        protocol_rows.each_with_index do |row, index|
          next if index.zero?
          expect(row.text).to include(election1.name)
          expect(row.text).to include(region2.name)
          expect(row.text).to include(local_area2.name)
          expect(row.text).to_not include(region1.name)
          expect(row.text).to_not include(local_area1.name)
        end
      end
    end

    scenario 'filter by election + region + local_area + org', js: true do
      as_user(coordinator) do
        visit cabinet_closing_protocols_path

        expect(all('#closing_protocols tr').size).to eq all_protocols.size + 1

        click_on 'reset_closing_protocols_filter'
        wait_for_ajax

        select election2.name, from: 'election_id'
        select region1.name, from: 'protocol_select_region'
        wait_for_ajax
        select local_area1.name, from: 'protocol_select_area'
        wait_for_ajax
        select org2.name, from: 'public_organization_id'

        clear_selectors('#protocol_select_station')

        click_on 'submit_closing_protocols_filter'
        wait_for_ajax

        protocol_rows = all('#closing_protocols tr')

        expect(protocol_rows.size).to eq closing_protocols2.size + 1
        protocol_rows.each_with_index do |row, index|
          next if index.zero?

          expect(row.text).to include(election2.name)
          expect(row.text).to include(region1.name)
          expect(row.text).to include(local_area1.name)
          expect(row.text).to include(org2.name)

          expect(row.text).to_not include(election1.name)
          expect(row.text).to_not include(region2.name)
          expect(row.text).to_not include(local_area2.name)
          expect(row.text).to_not include(org1.name)
        end
      end
    end

    scenario 'filter by election + region + local_area + station', js: true do
      as_user(coordinator) do
        visit cabinet_closing_protocols_path

        expect(all('#closing_protocols tr').size).to eq all_protocols.size + 1

        click_on 'reset_closing_protocols_filter'
        wait_for_ajax

        select election1.name, from: 'election_id'
        select region1.name, from: 'protocol_select_region'
        wait_for_ajax
        select local_area1.name, from: 'protocol_select_area'
        wait_for_ajax
        select stations1.first.name, from: 'protocol_select_station'

        click_on 'submit_closing_protocols_filter'
        wait_for_ajax

        protocol_rows = all('#closing_protocols tr')
        row_data = protocol_rows[1].text

        expect(protocol_rows.size).to eq 2
        expect(row_data).to include(stations1.first.name)
        expect(row_data).to include(election1.name)
        expect(row_data).to include(region1.name)
        expect(row_data).to include(local_area1.name)
      end
    end

    scenario 'filter by election + region + local_area + station + org', js: true do
      station = stations1.first
      [observer1, observer2].each do |user|
        create(:closing_protocol, station: station, election: election1, user: user)
      end

      as_user(coordinator) do
        visit cabinet_closing_protocols_path

        expect(all('#closing_protocols tr').size).to eq (all_protocols.size + 2) + 1

        click_on 'reset_closing_protocols_filter'
        wait_for_ajax

        select election1.name, from: 'election_id'
        select region1.name, from: 'protocol_select_region'
        wait_for_ajax
        select local_area1.name, from: 'protocol_select_area'
        wait_for_ajax
        select station.name, from: 'protocol_select_station'
        select org1.name, from: 'public_organization_id'

        click_on 'submit_closing_protocols_filter'
        wait_for_ajax

        protocol_rows = all('#closing_protocols tr')
        row_data = protocol_rows[1].text

        expect(protocol_rows.size).to eq 3
        expect(row_data).to include(stations1.first.name)
        expect(row_data).to include(election1.name)
        expect(row_data).to include(region1.name)
        expect(row_data).to include(local_area1.name)
        expect(row_data).to include(org1.name)

        select org2.name, from: 'public_organization_id'
        click_on 'submit_closing_protocols_filter'
        wait_for_ajax

        protocol_rows = all('#closing_protocols tr')
        row_data = protocol_rows[1].text

        expect(protocol_rows.size).to eq 2
        expect(row_data).to include(org2.name)
      end
    end
  end
end
