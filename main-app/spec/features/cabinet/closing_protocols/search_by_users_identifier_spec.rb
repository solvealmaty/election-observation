feature "cabinet" do
  feature "search closing protocols by user identifier" do
    let!(:country) { create(:country) }
    let!(:region) { create(:region, country: country) }
    let!(:local_area) { create(:local_area, region: region) }
    let!(:election_group) { create(:election_group) }
    let!(:org) { create(:public_organization) }
    let!(:election) { create(:election, :with_candidates, election_group: election_group) }
    let!(:stations) { create_list(:station, 7, election_group: election_group, local_area: local_area) }
    let!(:observer1) { create(:user, public_organization: org) }
    let!(:observer2) { create(:user) }
    let!(:coordinator) { create(:user, :coordinator, public_organization: org) }
    let!(:closing_protocols1) do
      stations[0..5].map do |station|
        create(:closing_protocol, station: station, election: election, user: observer1)
      end
    end
    let!(:closing_protocols2) do
      stations[5..(stations.size - 1)].map do |station|
        create(:closing_protocol, station: station, election: election, user: observer2)
      end
    end
    let!(:closing_protocols) { closing_protocols1 + closing_protocols2 }
    let!(:observers) { [observer1, observer2] }

    scenario "search by users identifiers", js: true  do
      as_user(coordinator) do
        visit cabinet_closing_protocols_path
        
        expect(closing_protocols.count + 1).to eq all("tr").count
        
        observers.each do |observer|
          within "#search_form" do
            fill_in "identifier", with: observer.identifier
            click_on "Поиск"  
          end
          wait_for_ajax

          expect(observer.closing_protocols.count + 1).to eq all("tr").count
        end
      end
    end
  end
end
