CORRECT_BULLETINS_VALUES = {
  total_bulletins: 150,
  voters_bulletins: 75,
  invalid_bulletins: 50,
  canceled_bulletins: 75
}.freeze

INCORRECT_BULLETINS_VALUES = {
  total_bulletins: 50,
  voters_bulletins: 100,
  invalid_bulletins: 50,
  canceled_bulletins: 100
}.freeze

CORRECT_VOTERS_VALUES = {
  total_voters: 300,
  active_voters: 200,
  absentee_certificate_voters: 50,
  outdoors_voters: 50
}.freeze

INCORRECT_VOTERS_VALUES = {
  total_voters: 100,
  active_voters: 50,
  absentee_certificate_voters: 50,
  outdoors_voters: 50
}.freeze

CORRECT_CANDIDATES_VOTES = [20, 60, 70].freeze
INCORRECT_CANDIDATES_VOTES = [100, 200, 300].freeze

def fill_candidates(values)
  within '#closing_protocol_candidates_form' do
    election.candidates.each_with_index do |c, index|
      expect(page).to have_content c.name
      fill_in "closing_protocol_candidates_closing_protocols_attributes__votes_#{c.id}", with: values[index]
    end
  end
end

def fill_voters(values)
  within '#closing_protocol_voters_form' do
    %w[total_voters active_voters absentee_certificate_voters outdoors_voters].each do |field|
      fill_in "closing_protocol_#{field}", with: values[field.to_sym]
    end
  end
end

def fill_bulletins(values)
  within '#closing_protocol_bulletins_form' do
    %w[total_bulletins voters_bulletins invalid_bulletins canceled_bulletins].each do |field|
      fill_in "closing_protocol_#{field}", with: values[field.to_sym]
    end
  end
end

feature 'observer' do
  feature 'manage closing protocols' do
    let!(:country) { create(:country) }
    let!(:region) { create(:region, country: country) }
    let!(:local_area) { create(:local_area, region: region) }
    let!(:election_group) { create(:election_group) }
    let!(:elections) { create_list(:election, 2, election_group: election_group) }
    let!(:election1_candidates) { create_list(:candidate, 3, election: elections.first) }
    let!(:election2_candidates) { create_list(:candidate, 3, election: elections.last) }
    let!(:stations) { create_list(:station, 2, local_area: local_area, election_group: election_group) }
    let!(:user) { create(:user, :observer) }

    let(:election) { elections.first }
    let(:user_station) { stations.first }

    context 'proceed to create a closing protocol' do
      context 'without attached station' do
        scenario 'user can pick region, local area and station' do
          as_user(user) do
            visit root_path
            click_on 'upload_closing_protocol'

            expect(page).to have_current_path new_closing_protocol_path
            expect(page).to have_css('#protocol_upload')
            expect(page).to have_css('#protocol_select_region')
            expect(page).to have_css('#protocol_select_area')
            expect(page).to have_css('#protocol_select_station')
          end
        end
      end

      context 'with attached station' do
        before do
          user.stations << user_station
          user.reload
        end

        scenario 'user can see attached station info' do
          as_user(user) do
            visit root_path
            click_on 'upload_closing_protocol'

            expect(page).to have_current_path new_closing_protocol_path
            expect(page).to have_css('#protocol_upload')
            expect(page).to_not have_css('#protocol_select_region')
            expect(find_field('closing_protocol[station_id]').value.to_i).to eq(user_station.id)

            expect(page).to have_content(user_station.local_area.region.name)
            expect(page).to have_content(user_station.local_area.name)
            expect(page).to have_content(user_station.name)
          end
        end

        context 'with valid protocol params' do
          before do
            district = region.create_electoral_district(level: 2)
            district.candidates << election.candidates
            district.elections << election
            district.save
            district.reload
          end

          scenario 'creates correct protocol without warning', js: true do
            as_user(user) do
              visit new_closing_protocol_path

              select local_area.name, from: 'protocol_select_area'
              select stations.first.name, from: 'protocol_select_station'
              find("a[href='#next']").click

              expect(page).to have_select('protocol_select_election', selected: election.name)
              find("a[href='#next']").click

              fill_voters(CORRECT_VOTERS_VALUES)
              find("a[href='#next']").click

              fill_candidates(CORRECT_CANDIDATES_VOTES)
              find("a[href='#next']").click

              fill_bulletins(CORRECT_BULLETINS_VALUES)
              find("a[href='#next']").click

              # visible: false because of the Bootstrap wrapper around the file input
              attach_file 'images[]',
                          [Rails.root.join('spec/fixtures/test.jpg')],
                          multiple: true,
                          visible: false

              find("a[href='#next']").click
              expect(page).to have_content 'Протокол отправлен'
            end
          end
        end

        context 'with invalid params' do
          scenario 'creates incorrect protocol with warning about voters and bulletins', js: true do
            as_user(user) do
              visit new_closing_protocol_path
              find("a[href='#next']").click

              expect(page).to have_select('protocol_select_election', selected: election.name)
              find("a[href='#next']").click

              fill_voters(INCORRECT_VOTERS_VALUES)
              find("a[href='#next']").click

              fill_candidates(INCORRECT_CANDIDATES_VOTES)
              find("a[href='#next']").click

              fill_bulletins(INCORRECT_BULLETINS_VALUES)
              find("a[href='#next']").click

              attach_file 'images[]',
                          [Rails.root.join('spec/fixtures/test.jpg')],
                          multiple: true,
                          visible: false

              find("a[href='#next']").click

              expect(page).to_not have_content 'Протокол отправлен'
              expect(page).to have_css('#closing_protocol_form_errors', visible: true)

              within '#closing_protocol_errors_modal', visible: true do
                expect(page).to have_content I18n.t('errors.closing_protocol.invalid_bulletins_message')
                expect(page).to have_content I18n.t('errors.closing_protocol.invalid_voters_message')
              end
            end
          end

          scenario 'creates incorrect protocol with warning about voters', js: true do
            as_user(user) do
              visit new_closing_protocol_path
              find("a[href='#next']").click

              expect(page).to have_select('protocol_select_election', selected: election.name)
              find("a[href='#next']").click

              fill_voters(INCORRECT_VOTERS_VALUES)
              find("a[href='#next']").click

              fill_candidates(CORRECT_CANDIDATES_VOTES)
              find("a[href='#next']").click

              fill_bulletins(CORRECT_BULLETINS_VALUES)
              find("a[href='#next']").click

              attach_file 'images[]',
                          [Rails.root.join('spec/fixtures/test.jpg')],
                          multiple: true,
                          visible: false

              find("a[href='#next']").click

              expect(page).to_not have_content 'Протокол отправлен'
              expect(page).to have_css('#closing_protocol_form_errors', visible: true)

              within '#closing_protocol_errors_modal', visible: true do
                expect(page).to have_content I18n.t('errors.closing_protocol.invalid_voters_message')
                expect(page).to_not have_content I18n.t('errors.closing_protocol.invalid_bulletins_message')
              end
            end
          end
        end
      end
    end
  end
end
