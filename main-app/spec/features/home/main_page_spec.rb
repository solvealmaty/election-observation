require 'rails_helper'

feature 'it works' do
  xscenario 'check home page' do
    as_user do
      visit root_path

      expect(page).to have_current_path(root_path)
      expect(page).to have_content 'Adil Saylau'
    end
  end
end
