require 'rails_helper'

feature 'manage election group orgs' do
  let!(:election_group) { create(:election_group) }
  let!(:orgs)           { create_list(:public_organization, 3) }
  let!(:elections)      { create_list(:election, 3, :with_candidates, election_group: election_group) }
  let!(:candidate)      { elections.first.candidates.first }

  before do
    election_group.public_organizations << orgs

    org = orgs.first
    org.update(candidate_ids: [candidate.id])
    org.reload
  end

  scenario 'index public orgs' do
    as_admin do
      visit admins_election_group_public_organizations_path(election_group)

      orgs.each do |org|
        expect(page).to have_content org.name
        expect(page).to have_content org.social_network
        expect(page).to have_content I18n.t('admin.pages.election_groups.public_organizations.index.not_affiliated')
      end

      org = orgs.first

      within "tr#tr-election-group-org-#{org.id}" do
        expect(page).to have_content org.candidates.map { |c| "#{c.name}, #{c.info}" }.join('; ')
      end
    end
  end

  xscenario 'mark org as affiliated with candidates', js: true do
    as_admin do
      visit admins_election_group_public_organizations_path(election_group)

      org = orgs.second

      expect(org.candidates.any?).to eq false

      click_on "btn-org-affiliation-#{org.id}"

      # within "form#org-affiliation-modal-#{org.id}" do
      #
      # end
    end
  end
end
