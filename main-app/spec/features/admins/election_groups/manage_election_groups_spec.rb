require 'rails_helper'

feature 'manage election groups' do
  let!(:election_group) { create(:election_group) }
  let(:election)              { create(:election, :ready, election_group: election_group) }

  scenario 'create election group' do
    as_admin do
      visit admins_election_groups_path

      expect(page).to_not have_content 'new election group name'
      expect(page).to_not have_content '26.09.2055'
      expect(page).to_not have_content 'new election group description'
      expect(page).to have_content I18n.t("admin.pages.election_groups.index.checkboxes.draft")

      click_on 'btn-add-election-group'
      expect(page).to have_current_path new_admins_election_group_path

      within 'form.new_election_group' do
        fill_in 'election_group_name', with: 'new election group name'
        fill_in 'election_group_date', with: '26/09/2055'
        fill_in 'election_group_description', with: 'new election group description'

        click_on 'btn-save'
      end

      expect(page).to have_content 'new election group name'
      expect(page).to have_content '26.09.2055'
      expect(page).to have_content 'new election group description'
      expect(page).to have_content I18n.t("admin.pages.election_groups.index.checkboxes.draft")
    end
  end

  scenario 'show election groups' do
    as_admin do
      visit admins_election_groups_path

      expect(page).to have_content election_group.name
      expect(page).to have_content election_group.description
      expect(page).to have_content I18n.l(election_group.date, format: :ddmmyy)
      expect(page).to have_content I18n.t("admin.pages.election_groups.index.checkboxes.draft")

      election.reload
      election_group.ready!

      visit admins_election_groups_path
      expect(page).to have_content I18n.t("admin.pages.election_groups.index.checkboxes.ready")
    end
  end

  scenario 'edit election group' do
    as_admin do
      visit admins_election_groups_path
      expect(page).to_not have_content 'new election group name'
      expect(page).to_not have_content '13.03.2021'
      expect(page).to_not have_content 'new election group description'

      click_on "btn-edit-election-group-#{election_group.id}"
      expect(page).to have_current_path edit_admins_election_group_path(election_group)

      within 'form.edit_election_group' do
        fill_in 'election_group_name', with: 'new election group name'
        fill_in 'election_group_date', with: '13/03/2021'
        fill_in 'election_group_description', with: 'new election group description'

        click_on 'btn-save'
      end

      expect(page).to have_content 'new election group name'
      expect(page).to have_content '13.03.2021'
      expect(page).to have_content 'new election group description'
    end
  end

  scenario 'publish election groups' do
    as_admin do
      election.reload
      election_group.ready!
      visit admins_election_groups_path

      expect(page).to have_content election_group.name
      expect(page).to have_content I18n.t("admin.pages.election_groups.index.checkboxes.ready")

      click_on "btn-publish-group-#{election_group.id}"

      expect(page).to have_content I18n.t("admin.pages.election_groups.index.checkboxes.published")
    end
  end

  scenario 'delete election group' do
    as_admin do
      visit admins_election_groups_path
      expect(page).to have_css("#election-group-#{election_group.id}")

      click_on "btn-remove-election-group-#{election_group.id}"
      expect(page).to_not have_css("#election-group-#{election_group.id}")
    end
  end
end
