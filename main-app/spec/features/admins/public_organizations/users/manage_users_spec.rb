require 'rails_helper'

feature 'manage org users' do
  given!(:org) { create(:public_organization) }
  given(:user) { org.users.first}

  scenario 'orgs list' do
    as_admin do
      visit admins_public_organizations_path

      click_on 'Состав ОО'

      expect(page).to have_content user.first_name
      expect(page).to have_content user.last_name
      expect(page).to have_content I18n.t('activerecord.attributes.user.role_types.representative')
    end
  end

  scenario 'create user' do
    as_admin do
      visit admins_public_organization_users_path(org)

      click_on 'Добавить'

      fill_in 'user[first_name]', with: 'New user name'
      fill_in 'user[last_name]', with: 'New user surname'
      select 'coordinator', from: 'user[role]'
      fill_in 'user[email]', with: 'new@example.com'

      click_on I18n.t('admin.buttons.save')

      expect(page).to have_content 'New user name'
      expect(page).to have_content 'New user surname'
      expect(page).to have_content I18n.t('activerecord.attributes.user.role_types.coordinator')
      expect(page).to have_content 'new@example.com'
    end
  end

  scenario 'edit user' do
    as_admin do
      visit admins_public_organization_users_path(org)

      expect(page).to have_content user.first_name
      expect(page).to have_content user.last_name
      expect(page).to have_content I18n.t('activerecord.attributes.user.role_types.representative')
      expect(page).to have_no_content('Edited Name')
      expect(page).to have_no_content('Edited Surname')

      click_on 'Редактировать'

      fill_in 'user[first_name]', with: 'Edited Name'
      fill_in 'user[last_name]', with: 'Edited Surname'
      select 'coordinator', from: 'user[role]'

      click_on I18n.t('admin.buttons.save')

      expect(page).to have_no_content user.first_name
      expect(page).to have_no_content user.last_name
      expect(page).to have_content 'Edited Name'
      expect(page).to have_content 'Edited Surname'
      expect(page).to have_content I18n.t('activerecord.attributes.user.role_types.coordinator')
    end
  end

  scenario 'destroy user' do
    as_admin do
      visit admins_public_organization_users_path(org)

      expect(page).to have_content user.first_name
      expect(page).to have_content user.last_name

      click_on 'Удалить'
      expect(page).to have_no_content user.first_name
      expect(page).to have_no_content user.last_name
    end
  end

  xscenario 'edit email' do
    # check change email instructions delivered
  end
end
