require 'rails_helper'

feature 'manage public organizations' do
  given!(:org) { create(:public_organization) }
  given!(:new_country) { create(:country) }

  scenario 'orgs list' do
    as_admin do
      visit admins_public_organizations_path

      expect(page).to have_content org.name
      expect(page).to have_content org.address
      expect(page).to have_content org.phone
      expect(page).to have_content org.iin_bin
      expect(page).to have_content org.social_network
    end
  end

  scenario 'create org' do
    as_admin do
      visit new_admins_public_organization_path

      within '#new-public-org-form' do
        fill_in 'public_organization[name]', with: 'New org'
        fill_in 'public_organization[address]', with: 'New org address'
        fill_in 'public_organization[phone]', with: '71234567890'
        fill_in 'public_organization[iin_bin]', with: '123456789012'
        fill_in 'public_organization[social_network]', with: 'www.example.com'
        select new_country.name, from: 'public_organization[country_id]'
        select 'representative', from: 'public_organization[users_attributes][0][role]'
        fill_in 'public_organization[users_attributes][0][first_name]', with: 'Alex'
        fill_in 'public_organization[users_attributes][0][last_name]', with: 'Smith'
        fill_in 'public_organization[users_attributes][0][email]', with: 'alex@example.com'
        click_on I18n.t('admin.buttons.save')
      end

      expect(page).to have_content('New org')
      expect(page).to have_content('New org address')
      expect(page).to have_content('71234567890')
      expect(page).to have_content('123456789012')
      expect(page).to have_content('www.example.com')

      visit admins_public_organization_users_path(PublicOrganization.last)
      expect(page).to have_content 'Alex'
      expect(page).to have_content 'Smith'
      expect(page).to have_content 'alex@example.com'
      expect(page).to have_content I18n.t('activerecord.attributes.user.role_types.representative')
    end
  end

  scenario 'edit org' do
    as_admin do
      visit admins_public_organizations_path

      click_on 'Редактировать'

      fill_in 'public_organization[name]', with: 'Edited org'
      fill_in 'public_organization[address]', with: 'Edited org address'
      fill_in 'public_organization[phone]', with: '71234567890'
      fill_in 'public_organization[iin_bin]', with: '123456789012'
      fill_in 'public_organization[social_network]', with: 'www.example.com'
      select new_country.name, from: 'public_organization[country_id]'

      click_on I18n.t('admin.buttons.save')
    end

    expect(page).to have_content 'Edited org'
    expect(page).to have_content 'Edited org address'
    expect(page).to have_content '71234567890'
    expect(page).to have_content '123456789012'
    expect(page).to have_content 'www.example.com'
  end

  scenario 'destroy org`' do
    as_admin do
      visit admins_public_organizations_path

      expect(page).to have_content org.name

      click_on 'Удалить'

      expect(page).to have_no_content org.name
    end
  end
end
