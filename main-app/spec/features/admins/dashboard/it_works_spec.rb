require 'rails_helper'

feature 'admin dashboard page' do
  xscenario 'works for authenticated user' do
    as_admin do
      visit admins_root_path

      expect(page).to have_content 'Welcome in INSPINIA Static SeedProject'
    end
  end

  xscenario 'requires sign in for anonymous' do
    visit admins_root_path

    expect(page).to have_current_path(new_admin_session_path)
    expect(page).to have_content 'Log in'
    expect(page).to have_field 'Email'
    expect(page).to have_field 'Password'
  end
end
