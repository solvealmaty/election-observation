require 'rails_helper'

feature 'edit elections' do
  given!(:election) { create(:election) }
  given!(:new_election_group ) { create(:election_group) }

  scenario 'base attributes' do
    as_admin do
      visit admins_elections_path

      within "#election-row-#{election.id}" do
        expect(page).to have_no_content 'Edited name'
        expect(page).to have_no_content new_election_group.name
        expect(find("#election-pinned-#{election.id}")).not_to be_checked
      end

      within "#edit-election-#{election.id}" do
        fill_in 'election[name]', with: 'Edited name'
        select new_election_group.name, from: 'election[election_group_id]'
        check 'election[pinned]'
        click_on 'Сохранить'
      end

      within "#election-row-#{election.id}" do
        expect(page).to have_content 'Edited name'
        expect(page).to have_content I18n.l(election.date, format: :full_month)
        expect(page).to have_content new_election_group.name
        expect(find("#election-pinned-#{election.id}")).to be_checked
      end
    end
  end
end
