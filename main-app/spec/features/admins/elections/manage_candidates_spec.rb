require 'rails_helper'

feature 'show elections' do
  given!(:election) { create(:election, :with_candidates) }

  scenario 'show election candidates' do
    as_admin do
      visit admins_election_candidates_path(election)

      expect(page).to have_content election.name

      election.candidates.each do |candidate|
        expect(page).to have_content candidate.name
        expect(page).to have_content candidate.info
        expect(page).to have_content full_district_name(candidate.electoral_district)
      end
    end
  end

  scenario 'delete election candidate' do
    as_admin do
      visit admins_election_candidates_path(election)

      candidate = election.candidates.last

      expect(page).to have_content election.name
      expect(page).to have_content candidate.name
      expect(page).to have_content candidate.info

      click_on "btn-remove-election-candidate-#{candidate.id}"

      expect(page).to_not have_content candidate.name
      expect(page).to_not have_content candidate.info
    end
  end

  def full_district_name(district)
    return 'Не указан' if district.nil?

    if district.level == 1 || district.level == 2
      district.scopeable.name
    elsif district.level == 3
      "#{district.scopeable.region.name} - #{district.scopeable.name}"
    end
  end
end
