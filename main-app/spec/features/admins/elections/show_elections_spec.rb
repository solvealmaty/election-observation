require 'rails_helper'

feature 'show elections' do
  let!(:election)       { create(:election) }
  let!(:election_ready) { create(:election, :with_stations, :with_candidates, :ready) }

  describe 'election attributes' do
    scenario 'without candidates and stations' do
      as_admin do
        visit admins_elections_path

        expect(page).to have_content(election.election_group.name)
        expect(page).to have_content election.name
        expect(page).to have_content I18n.l(election.date, format: :full_month)
        expect(page).to have_content I18n.t('admin.pages.elections.index.no_candidates')
        expect(page).to have_content I18n.t('admin.pages.elections.index.no_stations')
        expect(find(id: "election-pinned-#{election.id}")).not_to be_checked
      end
    end

    scenario 'with candidates and stations' do
      as_admin do
        visit admins_elections_path

        within "tr#election-row-#{election_ready.id}" do
          expect(page).to have_content(election_ready.election_group.name)
          expect(page).to have_content election_ready.name
          expect(page).to have_content I18n.l(election_ready.date, format: :full_month)
          expect(page).to have_css('.fa.fa-check-square-o.checkmark-icon')
          expect(find(id: "election-pinned-#{election_ready.id}")).not_to be_checked
        end
      end
    end

    scenario 'pinned' do
      election.pinned!

      as_admin do
        visit admins_elections_path

        checkbox = find("#election-pinned-#{election.id}")
        expect(checkbox).to be_checked
      end
    end
  end

  describe 'status check marks' do
    scenario 'draft' do
      as_admin do
        visit admins_elections_path

        td = find("#td-election-ready-#{election.id}")
        expect(td).to have_no_css('.fa.fa-square-o')
        expect(td).to have_no_css('.fa.fa-check-square-o')
      end
    end

    scenario 'ready' do
      election.ready!

      as_admin do
        visit admins_elections_path

        td = find("#td-election-ready-#{election.id}")
        expect(td).to have_css('.fa.fa-check-square-o')
      end
    end
  end
end
