require 'rails_helper'

feature 'create elections' do
  given!(:election_group) { create(:election_group) }

  scenario 'election with candidates and stations' do
    as_admin do
      visit admins_elections_path
      click_on 'Добавить выборы'

      within '#new-election' do
            fill_in 'election[name]', with: 'Новые выборы'
            attach_file('election[districts]', Rails.root.join('spec', 'fixtures', 'csv', 'districts.csv'))
            attach_file('election[candidates]', Rails.root.join('spec', 'fixtures', 'csv', 'candidates-region.csv'))
            select election_group.name, from: 'election[election_group_id]'
            check 'election[pinned]'
            fill_in 'election[date]', with: DateTime.now.strftime('%d-%m-%Y')
            click_on 'Добавить'
      end

      expect(page).to have_content 'Новые выборы'
      expect(page).to have_content election_group.name
      expect(page).to have_content I18n.l(DateTime.now, format: :full_month)
      expect(page.has_checked_field?("election-pinned-#{Election.last.id}")).to be true
      expect(page).to have_css('.fa.fa-check-square-o.checkmark-icon')
      expect(page).to have_no_content I18n.t('admin.pages.elections.index.no_stations')
      expect(page).to have_no_content I18n.t('admin.pages.elections.index.no_candidates')
    end
  end

  scenario 'election with stations' do
    as_admin do
      visit admins_elections_path
      click_on 'Добавить выборы'

      within '#new-election' do
            fill_in 'election[name]', with: 'Новые выборы'
            attach_file('election[districts]', Rails.root.join('spec', 'fixtures', 'csv', 'districts.csv'))
            select election_group.name, from: 'election[election_group_id]'
            check 'election[pinned]'
            fill_in 'election[date]', with: DateTime.now.strftime('%d-%m-%Y')
            click_on 'Добавить'
      end

      expect(page).to have_content 'Новые выборы'
      expect(page).to have_content election_group.name
      expect(page).to have_content I18n.l(DateTime.now, format: :full_month)
      expect(page.has_checked_field?("election-pinned-#{Election.last.id}")).to be true
      expect(page).to have_no_content I18n.t('admin.pages.elections.index.no_stations')
      expect(page).to have_content I18n.t('admin.pages.elections.index.no_candidates')
    end
  end

  scenario 'election with candidates' do
    as_admin do
      visit admins_elections_path
      click_on 'Добавить выборы'

      within '#new-election' do
            fill_in 'election[name]', with: 'Новые выборы'
            attach_file('election[candidates]', Rails.root.join('spec', 'fixtures', 'csv', 'candidates-region.csv'))
            select election_group.name, from: 'election[election_group_id]'
            check 'election[pinned]'
            fill_in 'election[date]', with: DateTime.now.strftime('%d-%m-%Y')
            click_on 'Добавить'
      end

      expect(page).to have_content 'Новые выборы'
      expect(page).to have_content election_group.name
      expect(page).to have_content I18n.l(DateTime.now, format: :full_month)
      expect(page.has_checked_field?("election-pinned-#{Election.last.id}")).to be true
      expect(page).to have_content I18n.t('admin.pages.elections.index.no_stations')
      expect(page).to have_no_content I18n.t('admin.pages.elections.index.no_candidates')
    end
  end
end
