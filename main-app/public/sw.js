const staticCacheName = 'static-cache-v0'
const dynamicCacheName = 'dynamic-cache'

const staticAssets = [
  '/icons/icon-192x192.png',
  'sw-registration.js',
]

const allowedHosts = [
  'mc.yandex.ru'
]

self.addEventListener('install', async event => {
  const cache = await caches.open(staticCacheName)
  await cache.addAll(staticAssets)
  console.log('Service worker has been installed')
})

self.addEventListener('activate', async event => {
  const cachesKeys = await caches.keys()
  checkKeys = cachesKeys.map(async key => {
    if (staticCacheName !== key) {
      await caches.delete(key)
    }
  })
  await Promise.all(checkKeys)
  console.log('Service worker has been activated')
})

self.addEventListener('fetch', async event => {
  if (allowedHosts.some(host => event.request.url.includes(host))) {
    return event.stopPropagation();
  } else {
    event.respondWith(cachedResponse(event.request))
  }
})

async function cachedResponse(req) {
   const cache = await caches.open(dynamicCacheName)

  try {
    const res = await fetch(req)
    await cache.put(req, res.clone())
    return res
  } catch (error) {
    return await cache.match(req)
  }
}
