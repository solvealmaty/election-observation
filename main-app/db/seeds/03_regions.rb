country = Country.find_by!(name: 'Казахстан')

%w[
  Алматы
  Нур-Султан
].each do |name|
  Region.create!(
    name: name,
    country: country
  )
end
