admin_email = 'admin@example.com'
admin_password = '123456'

if admin_email && admin_password
  Admin.create!(
    email: admin_email,
    password: admin_password,
    password_confirmation: admin_password
  )
end
