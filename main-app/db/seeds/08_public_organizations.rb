election_group = ElectionGroup.find_by!(name: 'Выборы Казахстан 2036')
country = Country.find_by!(name: 'Казахстан')

3.times do |n|
  org = country.public_organizations.find_or_create_by!(
    name: "Org #{n}",
    address: "Гоголя, #{n}",
    iin_bin: "123456",
    social_network: "twitter.com"
  )
  election_group.public_organizations << org
  election_group.save!
end
