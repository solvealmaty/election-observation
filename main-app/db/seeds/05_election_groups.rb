require 'faker'

ElectionGroup.create!(
  name: 'Выборы Казахстан 2036',
  description: Faker::Lorem.sentence,
  date: Time.zone.now.since(60.days)
)
