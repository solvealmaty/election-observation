require 'faker'

PublicOrganization.all.each.with_index(1) do |org, index|
  User.create!(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: "representative#{index}@example.com",
    password: '123456',
    public_organization: org,
    role: 'representative'
  )

  3.times do |n|
    count = "#{n}#{index}"
    User.create!(
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name,
      email: "coordinator#{count}@example.com",
      password: '123456',
      public_organization: org,
      role: 'coordinator'
    )
  end
end
