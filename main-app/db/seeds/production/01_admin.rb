admin_email = ENV.fetch('ADMIN_PRODUCTION_EMAIL')
admin_password = ENV.fetch('ADMIN_PRODUCTION_PASSWORD')

if admin_email && admin_password
  Admin.create!(
    email: admin_email,
    password: admin_password,
    password_confirmation: admin_password
  )
end
