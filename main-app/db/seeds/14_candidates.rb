election1 = Election.find_by!(name: 'Выборы президента')
election2 = Election.find_by!(name: 'Выборы в Мажилис')

%w[Trump Baiden Obama].each do |candidate_name|
  candidate = Candidate.create(name: candidate_name, election: election1)

  election1.electoral_districts.first.candidates << candidate
end

%w[Нур-Отан ЕдРос Республиканцы].each do |candidate_name|
  candidate = Candidate.create(name: candidate_name, election: election2)

  election2.electoral_districts.first.candidates << candidate
end

election3 = Election.find_by!(name: 'Выборы в региональные Маслихаты')
election3.electoral_districts.each do |district|
  3.times do |n|
    district.candidates.create(name: "Candidate #{district.scopeable.name} #{n}", election: election3)
  end
end

election4 = Election.find_by!(name: 'Выборы в районные Маслихаты')
election4.electoral_districts.each do |district|
  2.times do |n|
    district.candidates.create(name: "Candidate #{district.scopeable.name} #{n}", election: election4)
  end
end
