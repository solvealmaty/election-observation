region1 = Region.find_by!(name: 'Алматы')
region2 = Region.find_by!(name: 'Нур-Султан')

addresses1 = [
  'Бостандыкский район',
  'Наурызбайский район',
  'Медеуский район'
]

addresses2 = [
  'Байконурский район',
  'Есильский район',
]

addresses1.each do |name|
  LocalArea.find_or_create_by!(
    name: name,
    region_id: region1.id
  )
end

addresses2.each do |name|
  LocalArea.find_or_create_by!(
    name: name,
    region_id: region2.id
  )
end
