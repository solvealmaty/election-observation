require 'faker'

User.create!(
  first_name: Faker::Name.first_name,
  last_name: Faker::Name.last_name,
  email: "observer00@example.com",
  password: '123456',
  role: 'observer'
)

PublicOrganization.all.each.with_index(1) do |org, index|
  10.times do |n|
    count = "#{n}#{index}"
    User.create!(
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name,
      email: "observer#{count}@example.com",
      password: '123456',
      public_organization: org,
      role: 'observer'
    )
  end
end

User.observer.each.with_index do |observer, index|
  station = Station.order(:created_at).all[index]
  observer.stations << station if station.present?
end
