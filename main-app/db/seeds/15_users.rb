user = User.find_by(email: 'user@example.com')

unless user
  user = User.create!(
    phone: '71231234568',
    first_name: 'simple',
    last_name: 'user',
    email: 'user@example.com',
    password: '123456'
  )
end
