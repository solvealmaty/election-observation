country = Country.find_by!(name: 'Казахстан')

# Выборы президента
election1 = Election.find_by!(name: 'Выборы президента')
election1.electoral_districts.create!(
  level: 1,
  scopeable_id: country.id,
  scopeable_type: 'Country'
)

# Выборы в Мажилис
election2 = Election.find_by!(name: 'Выборы в Мажилис')
election2.electoral_districts.create!(
  level: 1,
  scopeable_id: country.id,
  scopeable_type: 'Country'
)

# Выборы в региональные Маслихаты
election3 = Election.find_by!(name: 'Выборы в региональные Маслихаты')
['Алматы', 'Нур-Султан'].each do |region_name|
  region = Region.find_by!(name: region_name)

  election3.electoral_districts.create!(
    level: 2,
    scopeable_id: region.id,
    scopeable_type: 'Region'
  )
end

# Выборы в районные Маслихаты
election4 = Election.find_by!(name: 'Выборы в районные Маслихаты')
[
  'Бостандыкский район',
  'Наурызбайский район',
  'Медеуский район',
  'Байконурский район',
  'Есильский район'
].each do |location_name|
  location = LocalArea.find_by!(name: location_name)

  election4.electoral_districts.create!(
    level: 3,
    scopeable_id: location.id,
    scopeable_type: 'LocalArea'
  )
end
