election_group = ElectionGroup.find_by!(name: 'Выборы Казахстан 2036')

['Выборы президента',
 'Выборы в Мажилис',
 'Выборы в региональные Маслихаты',
 'Выборы в районные Маслихаты'
].each do |election|
  election_group.elections.find_or_create_by!(
    name: election,
    date: election_group.date
  )
end
