election_group = ElectionGroup.find_by!(name: 'Выборы Казахстан 2036')

regions_data = {
  'Алматы' => [
    {
      local_area_name: 'Бостандыкский район',
      stations: [
        { address: 'коктем-1' },
        { address: 'Тимирязева' },
        { address:  'Алмалы' },
      ]
    },
    {
      local_area_name: 'Медеуский район',
      stations: [
        { address: 'микрорайон Самал-3' },
        { address:  'улица Луганского' },
        { address:  'проспект Достык' },
        { address: 'микрорайон Самал-2'},
        { coordinates: '43.2083, 76.9704' },
        { coordinates: '43.2122, 76.9686' }
      ]
    },
    {
      local_area_name: 'Наурызбайский район',
      stations: [
        { address: 'улица Таутаган' }
      ]
    }
  ],
  'Нур-Султан' => [
    { local_area_name: 'Байконурский район', stations: [{ address: 'улица Аксай'} ] },
    { local_area_name: 'Есильский район', stations: [{ address: 'проспект Мангилик Ел' }, { coordinates: '51.0908, 71.4106' }] },
  ]
}

# Создание участков для election_group
regions_data.each do |region_name, stations_data|
  region = Region.find_by!(name: region_name)

  stations_data.each do |data|
    local_area = region.local_areas.find_by!(name: data[:local_area_name])
    data[:stations].each_with_index do |station_info, idx|
      10.times do |iter|
        station_num = "#{iter}#{idx}"

        local_area.stations.create!(
          name: "#{local_area.name}, УИК №#{station_num}",
          address: station_info[:address],
          coordinates: station_info[:coordinates],
          election_group: election_group
        )
      end
    end
  end
end

# Добавление участков в выборы
election1 = Election.find_by!(name: 'Выборы президента')
election2 = Election.find_by!(name: 'Выборы в Мажилис')
Station.all.each do |station|
  election1.stations << station
  election2.stations << station
end

election3 = Election.find_by!(name: 'Выборы в региональные Маслихаты')
election3.electoral_districts.each do |district|
  district.scopeable.local_areas.flat_map(&:stations).each do |station|
    election3.stations << station
  end
end

election4 = Election.find_by!(name: 'Выборы в районные Маслихаты')
election4.electoral_districts.each do |district|
  district.scopeable.stations.each do |station|
    election4.stations << station
  end
end
