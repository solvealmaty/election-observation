PublicOrganization.all.each do |org|
  representative = User.where(
    email: "rep#{org.id}@example.com",
    public_organization_id: org.id
  ).first

  unless representative
    representative = User.create!(
      email: "rep#{org.id}@example.com",
      phone: "777812345#{org.id}",
      password: "123456",
      public_organization: org,
      role: 'representative'
    )
  end

  coordinator = User.where(
    email: "coordinator#{org.id}@example.com",
    public_organization_id: org.id
  ).first

  unless coordinator
    coordinator = User.create!(
      email: "coordinator#{org.id}@example.com",
      phone: "777712345#{org.id}",
      password: "123456",
      public_organization: org,
      role: 'coordinator'
    )
  end
end
