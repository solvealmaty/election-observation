election1 = Election.find_by!(name: 'Выборы президента')
election2 = Election.find_by!(name: 'Выборы в Мажилис')
election3 = Election.find_by!(name: 'Выборы в региональные Маслихаты')
election4 = Election.find_by!(name: 'Выборы в районные Маслихаты')

voted_range = 100..500
max_voters_range = 500..1000
invalid_bulletins_range = 1..50
protocols_created = 0

[election1, election2].each do |election|
  election.electoral_districts.each do |district|
    district.scopeable.regions.each do |region|
      region.local_areas.each do |area|
        area.stations.each do |station|
          protocol_attributes = {
            status: 'correct',
            total_voters: rand(max_voters_range),
            absentee_certificate_voters: rand(max_voters_range),
            outdoors_voters: rand(max_voters_range),
            active_voters: rand(max_voters_range),
            total_bulletins: rand(max_voters_range),
            voters_bulletins: rand(max_voters_range),
            invalid_bulletins: rand(invalid_bulletins_range),
            canceled_bulletins: rand(max_voters_range),
            station: station,
            election: election,
            candidates_closing_protocols_attributes: []
          }

          district.candidates.each do |candidate|
            voted = rand(voted_range)
            protocol_attributes[:candidates_closing_protocols_attributes] << {candidate_id: candidate.id, votes: voted}
          end

          User.observer.order('RANDOM()').take(2).each do |observer|
            protocol = station.closing_protocols.new(protocol_attributes.merge(user: observer))
            protocol.images = [
              Pathname.new(Rails.root.join('public', 'fixtures', 'test.jpg')).open,
              Pathname.new(Rails.root.join('public', 'fixtures', 'test.jpg')).open,
            ]

            protocol.save!

            protocols_created += 1
            puts "Протоколов создано: #{protocols_created}"
          end
        end
      end
    end
  end
end

election3.electoral_districts.each do |district|
  region = district.scopeable
  region.local_areas.each do |area|
    area.stations.each do |station|
      protocol_attributes = {
        status: 'correct',
        total_voters: rand(max_voters_range),
        absentee_certificate_voters: rand(max_voters_range),
        outdoors_voters: rand(max_voters_range),
        active_voters: rand(max_voters_range),
        total_bulletins: rand(max_voters_range),
        voters_bulletins: rand(max_voters_range),
        invalid_bulletins: rand(invalid_bulletins_range),
        canceled_bulletins: rand(max_voters_range),
        station: station,
        election: election3,
        candidates_closing_protocols_attributes: []
      }

      district.candidates.each do |candidate|
        voted = rand(voted_range)
        protocol_attributes[:candidates_closing_protocols_attributes] << {candidate_id: candidate.id, votes: voted}
      end

      User.observer.order('RANDOM()').take(2).each do |observer|
        protocol = station.closing_protocols.new(protocol_attributes.merge(user: observer))
        protocol.images = [
          Pathname.new(Rails.root.join('public', 'fixtures', 'test.jpg')).open,
          Pathname.new(Rails.root.join('public', 'fixtures', 'test.jpg')).open,
        ]

        protocol.save!

        protocols_created += 1
        puts "Протоколов создано: #{protocols_created}"
      end
    end
  end
end

election4.electoral_districts.each do |district|
  local_area = district.scopeable
  local_area.stations.each do |station|
    protocol_attributes = {
      status: 'correct',
      total_voters: rand(max_voters_range),
      absentee_certificate_voters: rand(max_voters_range),
      outdoors_voters: rand(max_voters_range),
      active_voters: rand(max_voters_range),
      total_bulletins: rand(max_voters_range),
      voters_bulletins: rand(max_voters_range),
      invalid_bulletins: rand(invalid_bulletins_range),
      canceled_bulletins: rand(max_voters_range),
      station: station,
      election: election4,
      candidates_closing_protocols_attributes: []
    }

    district.candidates.each do |candidate|
      voted = rand(voted_range)
      protocol_attributes[:candidates_closing_protocols_attributes] << {candidate_id: candidate.id, votes: voted}
    end

    User.observer.order('RANDOM()').take(2).each do |observer|
      protocol = station.closing_protocols.new(protocol_attributes.merge(user: observer))
      protocol.images = [
        Pathname.new(Rails.root.join('public', 'fixtures', 'test.jpg')).open,
        Pathname.new(Rails.root.join('public', 'fixtures', 'test.jpg')).open,
      ]

      protocol.save!

      protocols_created += 1
      puts "Протоколов создано: #{protocols_created}"
    end
  end
end
