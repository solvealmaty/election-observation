# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_01_22_071640) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "candidates", force: :cascade do |t|
    t.string "name"
    t.text "info"
    t.bigint "election_id"
    t.bigint "electoral_district_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "public_organization_id"
    t.index ["election_id"], name: "index_candidates_on_election_id"
    t.index ["electoral_district_id"], name: "index_candidates_on_electoral_district_id"
    t.index ["public_organization_id"], name: "index_candidates_on_public_organization_id"
  end

  create_table "candidates_closing_protocols", id: false, force: :cascade do |t|
    t.bigint "closing_protocol_id", null: false
    t.bigint "candidate_id", null: false
    t.integer "votes", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["candidate_id"], name: "index_candidates_closing_protocols_on_candidate_id"
    t.index ["closing_protocol_id"], name: "index_candidates_closing_protocols_on_closing_protocol_id"
  end

  create_table "closing_protocols", force: :cascade do |t|
    t.bigint "station_id"
    t.bigint "election_id"
    t.bigint "user_id"
    t.jsonb "images"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "status", default: 0
    t.integer "total_voters", default: 0, null: false
    t.integer "active_voters", default: 0, null: false
    t.integer "absentee_certificate_voters", default: 0, null: false
    t.integer "outdoors_voters", default: 0, null: false
    t.integer "total_bulletins", default: 0, null: false
    t.integer "voters_bulletins", default: 0, null: false
    t.integer "invalid_bulletins", default: 0, null: false
    t.integer "canceled_bulletins", default: 0, null: false
    t.boolean "conflict", default: false
    t.boolean "deleted", default: false
    t.index ["absentee_certificate_voters"], name: "index_closing_protocols_on_absentee_certificate_voters"
    t.index ["active_voters"], name: "index_closing_protocols_on_active_voters"
    t.index ["canceled_bulletins"], name: "index_closing_protocols_on_canceled_bulletins"
    t.index ["conflict"], name: "index_closing_protocols_on_conflict"
    t.index ["deleted"], name: "index_closing_protocols_on_deleted"
    t.index ["election_id"], name: "index_closing_protocols_on_election_id"
    t.index ["invalid_bulletins"], name: "index_closing_protocols_on_invalid_bulletins"
    t.index ["outdoors_voters"], name: "index_closing_protocols_on_outdoors_voters"
    t.index ["station_id"], name: "index_closing_protocols_on_station_id"
    t.index ["status"], name: "index_closing_protocols_on_status"
    t.index ["total_bulletins"], name: "index_closing_protocols_on_total_bulletins"
    t.index ["total_voters"], name: "index_closing_protocols_on_total_voters"
    t.index ["user_id"], name: "index_closing_protocols_on_user_id"
    t.index ["voters_bulletins"], name: "index_closing_protocols_on_voters_bulletins"
  end

  create_table "countries", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "election_groups", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "date"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "status", default: 0
  end

  create_table "election_groups_organizations", force: :cascade do |t|
    t.bigint "election_group_id"
    t.bigint "public_organization_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["election_group_id"], name: "index_election_groups_organizations_on_election_group_id"
    t.index ["public_organization_id"], name: "index_election_groups_organizations_on_public_organization_id"
  end

  create_table "elections", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "date"
    t.integer "status", default: 0
    t.boolean "pinned", default: false
    t.bigint "election_group_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "candidates_count", default: 0
    t.index ["election_group_id"], name: "index_elections_on_election_group_id"
  end

  create_table "elections_electoral_districts", id: false, force: :cascade do |t|
    t.bigint "election_id"
    t.bigint "electoral_district_id"
    t.index ["election_id"], name: "index_elections_electoral_districts_on_election_id"
    t.index ["electoral_district_id"], name: "index_elections_electoral_districts_on_electoral_district_id"
  end

  create_table "elections_stations", id: false, force: :cascade do |t|
    t.bigint "election_id", null: false
    t.bigint "station_id", null: false
    t.bigint "elections_id"
    t.bigint "stations_id"
    t.index ["elections_id"], name: "index_elections_stations_on_elections_id"
    t.index ["stations_id"], name: "index_elections_stations_on_stations_id"
  end

  create_table "electoral_districts", force: :cascade do |t|
    t.integer "level", default: 1
    t.bigint "scopeable_id"
    t.string "scopeable_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["scopeable_type", "scopeable_id"], name: "index_electoral_districts_on_scopeable_type_and_scopeable_id"
  end

  create_table "identities", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "provider"
    t.string "uid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_identities_on_user_id"
  end

  create_table "local_areas", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "region_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["region_id", "name"], name: "index_local_areas_on_region_id_and_name", unique: true
    t.index ["region_id"], name: "index_local_areas_on_region_id"
  end

  create_table "observers_stations", force: :cascade do |t|
    t.bigint "station_id"
    t.bigint "user_id"
    t.integer "certification", default: 0
    t.string "certificate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "certification_request", default: false
    t.integer "election_group_id", null: false
    t.integer "accreditor_id"
    t.string "rejection_reason"
    t.index ["station_id"], name: "index_observers_stations_on_station_id"
    t.index ["user_id", "election_group_id"], name: "index_observers_stations_on_user_id_and_election_group_id"
    t.index ["user_id"], name: "index_observers_stations_on_user_id"
  end

  create_table "public_organizations", force: :cascade do |t|
    t.string "name", null: false
    t.string "address"
    t.string "phone"
    t.string "iin_bin"
    t.string "social_network"
    t.integer "state", default: 0, null: false
    t.bigint "country_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["country_id"], name: "index_public_organizations_on_country_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "country_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["country_id", "name"], name: "index_regions_on_country_id_and_name", unique: true
    t.index ["country_id"], name: "index_regions_on_country_id"
  end

  create_table "stations", force: :cascade do |t|
    t.string "name", null: false
    t.string "address"
    t.string "coordinates"
    t.bigint "election_group_id"
    t.bigint "local_area_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["election_group_id"], name: "index_stations_on_election_group_id"
    t.index ["local_area_id"], name: "index_stations_on_local_area_id"
    t.index ["name", "local_area_id"], name: "index_stations_on_name_and_local_area_id", unique: true
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "phone"
    t.string "username"
    t.string "email"
    t.string "encrypted_password", default: "", null: false
    t.integer "role", default: 0, null: false
    t.string "first_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.string "access_token"
    t.string "verification_code"
    t.string "social_network"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "public_organization_id"
    t.string "identifier"
    t.index ["access_token"], name: "index_users_on_access_token", unique: true
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["identifier"], name: "index_users_on_identifier"
    t.index ["public_organization_id"], name: "index_users_on_public_organization_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "candidates", "elections"
  add_foreign_key "candidates", "electoral_districts"
  add_foreign_key "candidates", "public_organizations"
  add_foreign_key "closing_protocols", "elections"
  add_foreign_key "closing_protocols", "stations"
  add_foreign_key "closing_protocols", "users"
  add_foreign_key "elections", "election_groups"
  add_foreign_key "elections_stations", "elections", column: "elections_id"
  add_foreign_key "elections_stations", "stations", column: "stations_id"
  add_foreign_key "identities", "users"
  add_foreign_key "local_areas", "regions"
  add_foreign_key "observers_stations", "users", column: "accreditor_id"
  add_foreign_key "public_organizations", "countries"
  add_foreign_key "regions", "countries"
  add_foreign_key "stations", "election_groups"
  add_foreign_key "stations", "local_areas"
  add_foreign_key "users", "public_organizations"
end
