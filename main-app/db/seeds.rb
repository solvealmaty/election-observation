Dir["db/seeds/*.rb"].sort.each do |seed|
  begin
    load seed
  rescue ActiveRecord::RecordInvalid => e
    puts e
    puts "Seed: #{seed}"
  end
end
