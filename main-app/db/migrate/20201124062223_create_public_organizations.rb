class CreatePublicOrganizations < ActiveRecord::Migration[6.0]
  def change
    create_table :public_organizations do |t|
      t.string :name, null: false
      t.string :address
      t.string :phone
      t.string :iin_bin
      t.string :social_network
      t.integer :state, null: false, default: 0

      t.references :country, foreign_key: true

      t.timestamps
    end
  end
end
