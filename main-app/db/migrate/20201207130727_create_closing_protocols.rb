class CreateClosingProtocols < ActiveRecord::Migration[6.0]
  def change
    create_table :closing_protocols do |t|
      t.references :station, foreign_key: true
      t.references :election, foreign_key: true
      t.references :user, foreign_key: true
      t.jsonb :images

      t.timestamps
    end
  end
end
