class AddPublicOrganizationsToCandidates < ActiveRecord::Migration[6.0]
  def change
    add_reference :candidates, :public_organization, foreign_key: true, null: true
  end
end
