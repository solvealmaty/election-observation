class CreateLocalAreas < ActiveRecord::Migration[6.0]
  def change
    create_table :local_areas do |t|
      t.string :name, null: false
      t.references :region, foreign_key: true

      t.timestamps
    end
  end
end
