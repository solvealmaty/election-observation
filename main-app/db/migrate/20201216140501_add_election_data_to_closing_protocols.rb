class AddElectionDataToClosingProtocols < ActiveRecord::Migration[6.0]
  def change
    add_column :closing_protocols, :total_voters, :integer, default: 0, null: false
    add_column :closing_protocols, :active_voters, :integer, default: 0, null: false
    add_column :closing_protocols, :absentee_certificate_voters, :integer, default: 0, null: false
    add_column :closing_protocols, :outdoors_voters, :integer, default: 0, null: false
    add_column :closing_protocols, :total_bulletins, :integer, default: 0, null: false
    add_column :closing_protocols, :voters_bulletins, :integer, default: 0, null: false
    add_column :closing_protocols, :invalid_bulletins, :integer, default: 0, null: false
    add_column :closing_protocols, :canceled_bulletins, :integer, default: 0, null: false

    add_index :closing_protocols, :total_voters
    add_index :closing_protocols, :active_voters
    add_index :closing_protocols, :absentee_certificate_voters
    add_index :closing_protocols, :outdoors_voters
    add_index :closing_protocols, :total_bulletins
    add_index :closing_protocols, :voters_bulletins
    add_index :closing_protocols, :invalid_bulletins
    add_index :closing_protocols, :canceled_bulletins
  end
end
