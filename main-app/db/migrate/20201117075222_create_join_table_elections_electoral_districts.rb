class CreateJoinTableElectionsElectoralDistricts < ActiveRecord::Migration[6.0]
  def change
    create_table :elections_electoral_districts, id: false do |t|
      t.belongs_to :election
      t.belongs_to :electoral_district
    end
  end
end
