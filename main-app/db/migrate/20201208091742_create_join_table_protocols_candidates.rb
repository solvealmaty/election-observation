class CreateJoinTableProtocolsCandidates < ActiveRecord::Migration[6.0]
  def change
    create_join_table :closing_protocols, :candidates do |t|
      t.integer :votes, default: 0, null: false

      t.index :closing_protocol_id
      t.index :candidate_id

      t.timestamps
    end
  end
end
