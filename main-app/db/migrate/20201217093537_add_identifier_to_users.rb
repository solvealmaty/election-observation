class AddIdentifierToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :identifier, :string
    add_index :users, :identifier
  end
end
