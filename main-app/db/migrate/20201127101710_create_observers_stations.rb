class CreateObserversStations < ActiveRecord::Migration[6.0]
  def change
    create_table :observers_stations do |t|
      t.belongs_to :station
      t.belongs_to :user

      t.integer :certification, default: 0
      t.string :certificate

      t.timestamps
    end
  end
end
