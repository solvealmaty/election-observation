class CreateJoinTableElectionsStations < ActiveRecord::Migration[6.0]
  def change
    create_join_table :elections, :stations do |t|
      t.references :elections, foreign_key: true
      t.references :stations, foreign_key: true
    end
  end
end
