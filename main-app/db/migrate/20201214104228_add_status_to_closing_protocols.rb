class AddStatusToClosingProtocols < ActiveRecord::Migration[6.0]
  def change
    add_column :closing_protocols, :status, :integer, default: 0
    add_index :closing_protocols, :status
  end
end
