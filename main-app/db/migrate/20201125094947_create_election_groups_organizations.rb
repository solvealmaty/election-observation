class CreateElectionGroupsOrganizations < ActiveRecord::Migration[6.0]
  def change
    create_table :election_groups_organizations do |t|
      t.belongs_to :election_group
      t.belongs_to :public_organization

      t.timestamps
    end
  end
end
