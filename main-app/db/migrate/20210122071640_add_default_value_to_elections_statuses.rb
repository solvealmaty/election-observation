class AddDefaultValueToElectionsStatuses < ActiveRecord::Migration[6.0]
  def change
    change_column_default :elections, :status, from: nil, to: 0
  end
end
