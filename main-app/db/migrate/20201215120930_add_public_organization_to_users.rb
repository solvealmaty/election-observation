class AddPublicOrganizationToUsers < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :public_organization, foreign_key: true, null: true
  end
end
