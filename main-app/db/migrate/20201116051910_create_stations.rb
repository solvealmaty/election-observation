class CreateStations < ActiveRecord::Migration[6.0]
  def change
    create_table :stations do |t|
      t.string :name, null: false
      t.string :address
      t.string :coordinates
      t.references :election_group, foreign_key: true
      t.references :local_area, foreign_key: true

      t.timestamps
    end

    add_index :stations, [:name, :local_area_id], unique: true
  end
end
