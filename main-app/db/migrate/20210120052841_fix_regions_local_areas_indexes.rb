class FixRegionsLocalAreasIndexes < ActiveRecord::Migration[6.0]
  def change
    remove_index :regions, name: 'index_regions_on_name'

    add_index :regions, [:country_id, :name], unique: true
    add_index :local_areas, [:region_id, :name], unique: true
  end
end
