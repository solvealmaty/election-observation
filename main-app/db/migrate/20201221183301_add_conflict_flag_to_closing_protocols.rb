class AddConflictFlagToClosingProtocols < ActiveRecord::Migration[6.0]
  def change
    add_column :closing_protocols, :conflict, :boolean, default: false
    add_index :closing_protocols, :conflict
  end
end
