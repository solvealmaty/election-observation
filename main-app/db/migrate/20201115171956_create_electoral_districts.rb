class CreateElectoralDistricts < ActiveRecord::Migration[6.0]
  def change
    create_table :electoral_districts do |t|
      t.integer :level, default: 1
      t.bigint :scopeable_id
      t.string :scopeable_type

      t.timestamps
    end

    add_index :electoral_districts, [:scopeable_type, :scopeable_id]
  end
end
