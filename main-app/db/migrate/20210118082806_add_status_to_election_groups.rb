class AddStatusToElectionGroups < ActiveRecord::Migration[6.0]
  def change
    add_column :election_groups, :status, :integer, default: 0
  end
end
