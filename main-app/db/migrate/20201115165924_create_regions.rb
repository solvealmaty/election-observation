class CreateRegions < ActiveRecord::Migration[6.0]
  def change
    create_table :regions do |t|
      t.string :name, null: false
      t.references :country, foreign_key: true

      t.timestamps
    end

    add_index :regions, :name, unique: true
  end
end
