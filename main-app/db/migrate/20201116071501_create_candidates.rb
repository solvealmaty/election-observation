class CreateCandidates < ActiveRecord::Migration[6.0]
  def change
    create_table :candidates do |t|
      t.string :name
      t.text :info

      t.references :election, foreign_key: true
      t.references :electoral_district, foreign_key: true

      t.timestamps
    end
  end
end
