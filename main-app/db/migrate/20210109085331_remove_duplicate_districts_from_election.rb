class RemoveDuplicateDistrictsFromElection < ActiveRecord::Migration[6.0]
  def up
    ActiveRecord::Base.transaction do
      Election.all.each do |election|
        districts = election.electoral_districts.uniq
        election.electoral_districts.delete(districts)
        election.electoral_districts << districts
      end
    end
  end

  def down
  end
end
