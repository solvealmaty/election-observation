class AddElectionGroupIdToObserversStations < ActiveRecord::Migration[6.0]
  def change
    add_column :observers_stations, :election_group_id, :integer, null: false
    add_column :observers_stations, :accreditor_id, :integer, null: true
    add_column :observers_stations, :rejection_reason, :string

    add_index :observers_stations, [:user_id, :election_group_id]
    add_foreign_key :observers_stations, :users, column: :accreditor_id, primary_key: :id
  end
end
