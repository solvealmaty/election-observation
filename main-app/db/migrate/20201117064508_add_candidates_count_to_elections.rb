class AddCandidatesCountToElections < ActiveRecord::Migration[6.0]
  def change
    add_column :elections, :candidates_count, :integer, default: 0
  end
end
