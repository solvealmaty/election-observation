class AddDeletedFlagToClosingProtocols < ActiveRecord::Migration[6.0]
  def change
    add_column :closing_protocols, :deleted, :boolean, default: false
    add_index :closing_protocols, :deleted
  end
end
