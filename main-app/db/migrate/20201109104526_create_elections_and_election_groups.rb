class CreateElectionsAndElectionGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :election_groups do |t|
      t.string :name, null: false
      t.datetime :date
      t.text :description

      t.timestamps
    end

    create_table :elections do |t|
      t.string :name, null: false
      t.datetime :date
      t.integer :status, default: 0
      t.boolean :pinned, default: false

      t.references :election_group, foreign_key: true

      t.timestamps
    end
  end
end
