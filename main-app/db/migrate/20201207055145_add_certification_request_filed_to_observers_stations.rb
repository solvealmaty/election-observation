class AddCertificationRequestFiledToObserversStations < ActiveRecord::Migration[6.0]
  def change
    add_column :observers_stations, :certification_request, :boolean, default: false
  end
end
