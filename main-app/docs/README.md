# Open Elections Documentation

Documentation directory structure:
```
docs/
├── DEPLOYMENT.md
├── plantuml                          < Diagrams PlantUML sources 
│   ├── c4-component-diagram.puml     < C4 means 'C4 Model', see below
│   └── c4-system-context.puml
├── README.md                         < This file
├── README.ru.md                      < This file on other language
└── svg                               < SVG files for publication
    ├── c4-component-diagram.svg
    └── c4-system-context.svg
```

## How to document a Project

**TODO** Move this chapter to a separate document.

### Project Diagrams

We use Ricardo Niepel's [C4-PlantUML](https://github.com/RicardoNiepel/C4-PlantUML) which "is a lean graphical notation technique for modelling the architecture of software systems".

You can try the interactive [PlantUML Editor](https://plantuml-editor.kkeisuke.com/) to try "diagrams as a code".

## Architecture Overview

### System Context (level 1)

![System Context Diagram](svg/c4-system-context.svg)


