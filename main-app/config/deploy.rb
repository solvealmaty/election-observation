set :application, 'election-observation'
set :repo_url, "git@gitlab.com:solvealmaty/election-observation.git"

set :file_permissions_paths, %w(app/logs app/cache)
set :file_permissions_users, %w(www-data)

set :rvm_ruby_version, '2.5.5@election-observation'
set :bundle_bins, %w{gem rake rails}
set :bundle_env_variables, { nokogiri_use_system_libraries: 1 }
set :rvm1_map_bins, %w(rake gem bundle ruby honeybadger)
set :assets_dependencies, %w(app/assets lib/assets vendor/assets Gemfile.lock config/routes.rb)

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml", ".ruby-version", ".ruby-gemset"

# Default value for linked_dirs is []
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system", "public/uploads", ".bundle"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

before 'deploy:updated', 'bundler:install'

after 'deploy:published', 'deploy:assets:precompile'
after 'deploy:published', 'passenger:restart'
