Rails.application.routes.draw do
  devise_for :admins, controllers: {
    sessions: 'admins/sessions'
  }

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions',
    omniauth_callbacks: 'users/omniauth_callbacks'
  }

  devise_scope :user do
    get  'users/:id/request_code'   => 'users/sms_verifications#request_code',   as: 'request_code'
    post 'users/:id/verify_account' => 'users/sms_verifications#verify_account', as: 'verify_account'
  end

  mount GrapeSwaggerRails::Engine => '/swagger'
  mount ElectionObservation::Api, at: '/'

  root  to: 'home#index'
  get 'filter_stations' => 'home#filter_stations'

  resources :closing_protocols do
    get :candidates, on: :collection
  end

  resources :stations, only: %i[index show]

  namespace :results do
    resources :elections, only: %i[index show], controller: 'elections' do
      resources :districts, only: %i[index show], controller: 'elections/districts' do
        get :local_areas, on: :member

        resources :stations, controller: 'elections/stations'
      end
    end
  end

  scope module: 'users' do
    resource :profile
    resources :observers_stations, only: %i[create update destroy] do
      delete :delete_certificate, on: :member
    end
  end

  namespace :admins do
    root 'dashboard#index'

    resources :election_groups do
      patch :publish, on: :member

      resources :public_organizations, only: %i[index update], controller: 'election_groups/public_organizations'
    end
    resources :public_organizations do
      resources :users, controller: 'public_organizations/users'
    end
    resources :elections, only: %i[index create update destroy show] do
      resources :candidates, controller: 'elections/candidates'
      resources :electoral_districts, only: %i[index destroy], controller: 'elections/electoral_districts'
      resources :stations, only: %i[index update destroy], controller: 'elections/stations' do
        get :filtered, on: :collection
      end
    end
    resources :local_areas, only: :update
  end

  namespace :cabinet do
    root 'dashboard#index'

    devise_scope :user do
      get 'sign_in' => 'sessions#new'
    end

    resources :closing_protocols do
      collection do
        get :filter
        get :search
        get :reset_filter
        patch :batch_delete
      end
      member do
        patch :approve
        patch :reject
        patch :delete
        patch :batch_reject
      end
    end
    resources :users
    resources :election_groups do
      resources :observers_stations, only: %i[index show], controller: 'election_groups/observers_stations' do
        member do
          patch :approve
          patch :reject
        end
      end
    end
  end

  namespace :api do
    namespace :v1, default: { format: :json } do
      resources :regions
      resources :local_areas
      resources :stations
      resources :elections
      resources :candidates

      namespace :mobile do
        post 'user_token' => 'user_token#create'
        get 'token_login' => 'users#me'
      end
    end
  end
end
