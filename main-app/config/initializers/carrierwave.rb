CarrierWave.configure do |config|
  config.storage = :aws
  config.aws_bucket = ENV.fetch('STORAGE_BUCKET')
  config.aws_attributes = {
    cache_control: "max-age=#{365.day.to_i}"
  }
  config.aws_credentials = {
    access_key_id: ENV.fetch('STORAGE_ACCESS_KEY_ID'),
    secret_access_key: ENV.fetch('STORAGE_SECRET_ACCESS_KEY'),
    region: ENV.fetch('STORAGE_REGION'),
    stub_responses: Rails.env.test?,
    endpoint: "https://#{ENV.fetch('STORAGE_ENDPOINT')}"
  }

  config.aws_acl = 'public-read'
end
