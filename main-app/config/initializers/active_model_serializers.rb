require_relative '../../lib/core_ext/big_decimal'

ActiveModelSerializers.config.adapter = :json
ActiveModelSerializers.config.json_include_toplevel_object = true
ActiveModelSerializers.config.default_includes = "**"
