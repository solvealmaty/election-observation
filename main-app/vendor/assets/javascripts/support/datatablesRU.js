function datatablesLanguageSettingsRu () {
  return {
    search: 'Поиск',
    lengthMenu: 'Показывать _MENU_ записей',
    info: 'Страница _PAGE_ из _PAGES_',
    paginate: {
      first: 'Первая',
      previous: 'Предыдущая',
      next: 'Следующая',
      last: 'Последняя'
    }
  }
}
