$(function () {
  const stationFilterSelector = $('#station_filter_selector');
  
  stationFilterSelector.select2({
    multiple: true,
    placeholder: 'Все',
    allowClear: true
  });
  stationFilterSelector.select2('val', 0);
  
  $(document).on('click', '.btn-filter-stations-coverage', function () {
    loadingIndicator(true);
    
    let regionId = document.getElementById('dashboard_region_id').value;
    
    $.ajax({
      method: 'GET',
      url: `/filter_stations.json`,
      data: {
        region_id: regionId
      },
      success: function (response) {
        loadingIndicator(false);
        reInitYMaps(response);
      }
    });
  });
  
  $(document).on('click', '.btn-filter-stations', function () {
    loadingIndicator(true);
    
    let orgsCheckboxes = document.getElementsByClassName('org-checkbox');
    let orgsIds = [];
    let orgsNames = [];
    
    for(let i = 0; i < orgsCheckboxes.length; i++){
      let orgCheckbox = orgsCheckboxes[i];
      
      if (orgCheckbox.checked) {
        orgsIds.push(orgCheckbox.value);
        orgsNames.push(orgCheckbox.dataset['name']);
      }
    }
    
    let orgsDropdownInput = document.getElementById('public_organizations');
    orgsDropdownInput.value = orgsNames.join(', ');
    
    let orgType      = document.getElementById('public_organization_type').value;
    let observerType = document.getElementById('observer_type').value;
    let regionId     = document.getElementById('region_id').value;
    let localAreaId  = document.getElementById('local_area_id').value;
    let stationIds   = $('#station_filter_selector').val();
    
    $.ajax({
      method: 'GET',
      url: `/stations.json`,
      data: {
        station: {
          public_organization_type: orgType,
          observer_type: observerType,
          public_organization_ids: orgsIds,
          region_id: regionId,
          local_area_id: localAreaId,
          station_ids: stationIds
        }
      },
      success: function (response) {
        loadingIndicator(false);
        reInitYMaps(response);
      }
    });
  });
})
