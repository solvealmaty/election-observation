function validateNumberFields (fields) {
  let valid = true;
  fields.toArray().forEach(n => {
    if (n.value < 0 || n.value === '') {
      $(n.parentElement).append(`<p class='text-danger error-message'>Должно быть больше или равно 0</p>`);
      valid = false;
    }
  });
  return valid;
}
