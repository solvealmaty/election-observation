let loadingIndicatorTimeout;

function showIndicator (visible) {
  document.getElementById('loading-overlay').style.visibility = visible ? 'visible' : 'hidden';
}

function loadingIndicator (visible) {
  if (visible) {
    loadingIndicatorTimeout = setTimeout(() => showIndicator(visible), LOADING_INDICATOR_TIMEOUT);
  } else {
    clearTimeout(loadingIndicatorTimeout);
    showIndicator(visible);
  }
}
