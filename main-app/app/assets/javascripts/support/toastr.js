//= require inspinia/plugins/toastr/toastr.min

function toastErrors (status, message = '') {
  if (status === 408) {
    return toastr.error('Проблемы с подключением. Попробуйте снова позже');
  }
  
  if (status === 422) {
    return toastr.error(message);
  }
  
  toastr.error('Не удалось загрузить данные');
}
