//= require jquery2
//= require jquery_ujs
//= require bootstrap-sprockets
//= require cable.js
//= require plugins/select2.min

//= require inspinia/plugins/metisMenu/jquery.metisMenu
//= require inspinia/plugins/slimscroll/jquery.slimscroll.min
//= require inspinia/plugins/pace/pace.min
//= require inspinia/plugins/toastr/toastr.min
//= require inspinia/style

toastr.options = {
  closeButton: true,
  progressBar: true,
  showMethod: 'slideDown',
  timeOut: 4000
};

$(function () {
  $('.select2-org-candidates').select2();

  $('.election-pinned-checkbox').on('change', function () {
    const isChecked = this.checked;
    const id = $(this).data('id');

    $.ajax({
      method: 'PATCH',
      url: `/admins/elections/${id}`,
      data: {
        election: {
          pinned: isChecked
        }
      }
    });
  });

  $("[name='election[election_group_id]']").on('change', function () {
    const electionGroupId = this.value;
    const electionDate = gon.election_groups[electionGroupId];
    const electionId = this.id.replace('election-group-', '');
    const dateField = document.getElementById(`election-date-${electionId}`);

    if (electionDate) {
      dateField.value = electionDate;
      dateField.readOnly = true;
    } else {
      dateField.value = "";
      dateField.readOnly = false;
    }
  });

  $('.election-stations').on('click', function () {
    const localAreaId = $(this).data('localAreaId');
    const electionId = $(this).data('electionId');
    const levels = $(this).data('levels').split(';');

    document.getElementById('content-country').innerHTML = levels[0];
    document.getElementById('content-region').innerHTML = levels[1];
    document.getElementById('content-local-area').innerHTML = levels[2];

    $.ajax({
    method: 'GET',
    url: `/admins/elections/${electionId}/stations/filtered?local_area_id=${localAreaId}`,
    dataType: 'json',
    success: function (response) {
      const tableRef = document.getElementById('local-area-stations');

      $("#local-area-stations").find("tr:gt(0)").remove();

      $.each(response, function (key, station) {
        let row = tableRef.insertRow();

        let cellId = row.insertCell(0);
        let cellName = row.insertCell(1);
        let cellAddress = row.insertCell(2);
        let cellCoordinates = row.insertCell(3);

        cellId.innerHTML = station['id'];
        cellName.innerHTML = station['name'];
        cellAddress.innerHTML = station['address'];
        cellCoordinates.innerHTML = station['coordinates'];
      });
    }
    });
  });
});
