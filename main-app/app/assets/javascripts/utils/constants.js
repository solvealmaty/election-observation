const LOADING_INDICATOR_TIMEOUT = 250;
const BULLETINS_THRESHOLD_PERCENT = 101;
const IMAGE_QUALITY = 0.2;
const MAX_IMAGES_AMOUNT = 5;
const FILTER_SELECTORS_IDS = [
  '#election_id',
  '#public_organization_id',
  '#protocol_select_status',
  '#protocol_select_conflict'
]
