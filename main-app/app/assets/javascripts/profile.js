$(function () {
  $(document).on('click', '#observer-info', function () {
    $(this).addClass('active');
    $('#personal-info').removeClass('active');
    
    $('#personal-section')[0].style.display = 'none';
    $('#observer-section')[0].style.display = 'flex';
  })
  
  $(document).on('click', '#personal-info', function () {
    $(this).addClass('active');
    $('#observer-info').removeClass('active');
    
    $('#observer-section')[0].style.display = 'none';
    $('#personal-section')[0].style.display = 'flex';
  })
  
  document.querySelectorAll('#user_profile_form input').forEach(f => {
    f.addEventListener('focus', function () {
      $('#submit_user_profile').removeClass('button-disabled');
    })
  });
  
  $('#user_profile_form').submit(function (e) {
    e.preventDefault();
    
    $.ajax({
      method: 'PATCH',
      url: '/profile',
      data: new FormData(this),
      dataType: 'script',
      processData: false,
      contentType: false,
      success: function (data, status, xhr) {
        toastr.success(xhr.responseText);
      },
      error: function (xhr, status, error) {
        toastr.error(error);
      }
    })
  })
});
