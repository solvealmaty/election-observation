$(function () {
  const actionsWrapperId = '#closing_protocols_actions';
  const closingProtocolCheckClass = '.closing-protocol-check';
  
  $('#protocol_select_station, #protocol_select_area, #protocol_select_region').select2({
    allowClear: true,
    placeholder: 'Поиск'
  });
  
  $('#closing_protocols').DataTable({
    language: datatablesLanguageSettingsRu(),
    pageLength: 50,
    paging: false
  });
  
  $(document).on('change', closingProtocolCheckClass, function () {
    if ($(`${closingProtocolCheckClass}:checked`).length > 0) {
      $(actionsWrapperId).html(
        '<a class="btn btn-sm btn-danger m-1 closing-protocol-batch-delete" data-confirm="Удалить выбранные?" href="">Удалить</a>'
      );
    } else {
      $(actionsWrapperId).empty();
    }
  });
  
  $(document).on('click', '.closing-protocol-batch-delete', function (e) {
    e.preventDefault();
    
    const ids = $(`${closingProtocolCheckClass}:checked`).toArray().map(c => {
      return c.dataset['closingProtocolId'];
    });
    
    $.ajax({
      method: 'PATCH',
      url: '/cabinet/closing_protocols/batch_delete',
      dataType: "script",
      data: {
        ids
      },
      success: function () {
        $(actionsWrapperId).empty();
        ids.forEach(id => {
          $(`tr#closing-protocol-info-${id}`).remove();
        });
      }
    })
  });
});
