//= require './templates/candidateFields'
//= require '../support/loadingIndicator'
//= require '../utils/constants'

$(function () {
  function isCabinetPath () {
    return window.location.href.includes('cabinet');
  }
  
  function getCandidates () {
    if (isCabinetPath()) return;
    
    const stationId = $('#protocol_select_station')[0].value;
    const electionId = $('#protocol_select_election')[0].value;
    
    if (!stationId || !electionId) return;
    
    loadingIndicator(true);
  
    $.ajax({
      method: 'GET',
      url: '/api/v1/candidates',
      data: {
        election_id: electionId,
        station_id: stationId
      },
      success: function (data, status, xhr) {
        if (xhr.status !== 200) {
          return toastErrors(xhr.status);
        }
      
        const candidatesForm = $('#closing_protocol_candidates_form');
        candidatesForm.empty();
      
        data.candidates.forEach(c => {
          candidatesForm.append(candidateFields(c));
        });
  
        loadingIndicator(false);
      }
    })
  }
  
  $(document).on('change', '#protocol_select_region', function () {
    if (!this.value) return;
  
    loadingIndicator(true);
    
    $.ajax({
      method: 'GET',
      url: `/api/v1/regions/${this.value}`,
      success: function (data, status, xhr) {
        if (xhr.status !== 200) {
          return toastErrors(xhr.status);
        }
        
        const localAreasSelector = $('#protocol_select_area');
        const stationsSelector = $('#protocol_select_station');
        
        localAreasSelector.empty();
        stationsSelector.empty();
        
        data.region.local_areas.forEach(l => {
          localAreasSelector.append(
            `<option value="${l.id}">${l.name}</option>`
          );
        });
        
        data.region.local_areas[0].stations.forEach(station => {
          stationsSelector.append(
            `<option value="${station.id}">${station.name}</option>`
          );
        })
  
        loadingIndicator(false);
        getCandidates();
      }
    })
  });
  
  $(document).on('change', '#protocol_select_area', function () {
    if (!this.value) return;
  
    loadingIndicator(true);
    
    $.ajax({
      method: 'GET',
      url: `/api/v1/local_areas/${this.value}`,
      success: function (data, status, xhr) {
        if (xhr.status !== 200) {
          return toastErrors(xhr.status);
        }
        
        const stationsSelector = $('#protocol_select_station');
        stationsSelector.empty();
        
        data.local_area.stations.forEach(s => {
          stationsSelector.append(
            `<option value="${s.id}">${s.name}</option>`
          );
        });
  
        loadingIndicator(false);
        getCandidates();
      }
    })
  });
  
  $(document).on('change', '#protocol_select_station', function () {
    getCandidates();
  });
  
  $(document).on('change', '#protocol_select_election', function () {
    getCandidates();
  })
  
  $(document).on('click', '#reset_closing_protocols_filter', function (e) {
    e.preventDefault();
    
    $.ajax({
      method: 'GET',
      url: '/cabinet/closing_protocols/reset_filter',
      dataType: 'script',
      success: function (data, status, xhr) {
        if (xhr.status !== 204) {
          return toastErrors(xhr.status);
        }
  
        FILTER_SELECTORS_IDS.forEach(field => {
          $(field)[0].selectedIndex = 0;
        });
  
        $('#protocol_select_region, #protocol_select_area, #protocol_select_station').val(null).trigger('change');
      }
    })
  });
})
