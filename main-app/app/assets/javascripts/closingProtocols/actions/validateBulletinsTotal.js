//= require '../../utils/constants'

function validateBulletinsTotal () {
  const result = { valid: true, message: null };
  const totalVoters = parseInt($('input[name="closing_protocol[total_voters]"]')[0].value);
  const totalBulletins = parseInt($('input[name="closing_protocol[total_bulletins]"]')[0].value);
  const limit = (totalVoters * BULLETINS_THRESHOLD_PERCENT) / 100;
  
  if (totalBulletins > parseInt(limit)) {
    result.message = I18n.errors.closing_protocol.invalid_total_bulletins_message;
    result.valid = false;
  }
  
  return result;
}
