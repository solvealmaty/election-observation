function validateBulletinFields () {
  const result = { valid: true, message: null };
  const totalFieldValue = parseInt($('input[name="closing_protocol[total_bulletins]"]')[0].value);
  const votersFieldValue = parseInt($('input[name="closing_protocol[voters_bulletins]"]')[0].value);
  const canceledFieldValue = parseInt($('input[name="closing_protocol[canceled_bulletins]"]')[0].value);
  
  if (totalFieldValue !== (votersFieldValue + canceledFieldValue)) {
    result.message = I18n.errors.closing_protocol.invalid_bulletins_message;
    result.valid = false;
  }
  
  return result;
}
