function validateVoterFields () {
  const result = { valid: true, message: null };
  const activeVotersValue = parseInt($('input[name="closing_protocol[active_voters]"]')[0].value);
  const invalidBulletins = parseInt($('input[name="closing_protocol[invalid_bulletins]"]')[0].value);
  const votesFields = $('input[name="closing_protocol[candidates_closing_protocols_attributes][][votes]"]');
  const votesValue = votesFields.toArray().reduce((a, b) => a + parseInt(b.value), 0);
  
  if (activeVotersValue !== (invalidBulletins + votesValue)) {
    result.message = I18n.errors.closing_protocol.invalid_voters_message;
    result.valid = false;
  }
  
  return result;
}
