function candidateFields(candidate) {
  return `
      <h5>${candidate.name}</h5>
      <input id="closing_protocol_candidates_closing_protocols_attributes__candidate_id_${candidate.id}" name="closing_protocol[candidates_closing_protocols_attributes][][candidate_id]" type="hidden" value="${candidate.id}">
      <div class="form-group">
        <input class="form-control input valid" id="closing_protocol_candidates_closing_protocols_attributes__votes_${candidate.id}" name="closing_protocol[candidates_closing_protocols_attributes][][votes]" type="number" value="0" aria-invalid="false">
      </div>
    `
}
