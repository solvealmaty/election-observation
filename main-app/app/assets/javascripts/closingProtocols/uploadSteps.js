//= require 'utils/constants'
//= require 'support/validateNumberFields'
//= require 'plugins/compressor.min'

//= require './actions/validateBulletinFields'
//= require './actions/validateBulletinsTotal'
//= require './actions/validateVoterFields'

$(function () {
  const form = $("#protocol_upload");
  const errorsModal = $('#closing_protocol_errors_modal');
  let formErrors = {};
  let formValid = true;
  
  $(document).ready(function () {
    const nextButton = $('a[href="#next"]');
    const prevButton = $('a[href="#previous"]');
    
    $('ul[role="menu"]').addClass('actions-wrapper');
    nextButton.addClass('button-md button-primary width-100 float-right');
    prevButton.addClass('button-md button-secondary width-100');
    prevButton.hide();
  });
  
  function submitClosingProtocolForm () {
    $('#protocol_wizard .actions').remove();
    form.submit();
  }
  
  function changeSubmitButtonText (text) {
    $('a[href="#next"]')[0].innerHTML = text;
  }
  
  function compressImages (files) {
    const images = [];
    return new Promise(async function(resolve) {
      let processedImages = 0
      let imagesToProcess = files.length;
      for (let i = 0; i < imagesToProcess; i++) {
        const file = files[i];
        await new Promise(resolve => {
          new Compressor(file, {
            quality: IMAGE_QUALITY,
            success(result) {
              images.push({result, name: result.name});
              resolve();
            }
          })
        })
        processedImages += 1
      }
      if (processedImages === imagesToProcess) {
        resolve(images);
      }
    })
  }
  
  async function sendFormData (target) {
    loadingIndicator(true);
    
    const formData = new FormData(target);
    const imageFiles = document.getElementById('closing_protocol_images').files;
    const images = await compressImages(imageFiles, formData);
  
    formData.delete('images[]');

    images.forEach(image => {
      formData.append('images[]', image.result, image.name);
    })
  
    $.ajax({
      method: 'POST',
      url: '/closing_protocols',
      data: formData,
      dataType: 'script',
      processData: false,
      contentType: false,
      complete: function (_xhr, _status) {
        loadingIndicator(false);
      },
      success: function (data, status, xhr) {
        if (xhr.status !== 200) toastErrors(xhr.status);
      },
      error: function (xhr, _status, _error) {
        toastErrors(xhr.status, xhr.responseText);
      }
    });
  }
  
  $(document).on('change', '#protocol_upload .custom-file-input', function () {
    const nextLink = $('a[href="#next"]');
    const count = this.files.length;
    const wrapper = this.parentElement;
    
    $(wrapper).append(`<p class="text-info">Выбрано файлов: ${count}</p>`)
    
    if (count > MAX_IMAGES_AMOUNT) {
      $(wrapper).append(`<p class="text-danger error-message">Можно загрузить не более 5 фотографий</p>`);
      nextLink.addClass('disabled-link');
    } else {
      $('.error-message').remove();
      nextLink.removeClass('disabled-link');
    }
  })
  
  $(document).on('click', '.close-invalid-form-modal', function (e) {
    e.preventDefault();
    errorsModal.modal('hide');
  });
  
  $(document).on('click', '.submit-invalid-form-modal', function (e) {
    e.preventDefault();
    
    formErrors = {};
    errorsModal.modal('hide');
    
    $('a[href="#next"]').click();
  });
  
  $(document).on('submit', '#protocol_upload', function (e) {
    e.preventDefault();
    sendFormData(this);
  });

  form.steps({
    headerTag: "h5",
    bodyTag: "fieldset",
    labels: {
      finish: "На главную",
      next: "Далее",
      previous: "Назад",
    },
    onStepChanging: function (event, currentIndex, newIndex) {
      changeSubmitButtonText(newIndex >= 5 ? 'Отправить' : 'Далее');
      
      $('a[href="#previous"]').show();
      
      if (currentIndex > newIndex) return true;
      if (currentIndex < newIndex) {
        form.find(".body:eq(" + newIndex + ") label.error").remove();
        form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
      }

      form.validate().settings.ignore = ":disabled,:hidden";
  
      // Candidates step
      if (currentIndex === 2) {
        const numberFields = $('input[name="closing_protocol[candidates_closing_protocols_attributes][][votes]"]');
        
        $('.error-message').remove();
        formValid = validateNumberFields(numberFields);
      }
      
      if (currentIndex === 3) {
        const numberFields = $('input.voters-field');
  
        $('.error-message').remove();
        formValid = validateNumberFields(numberFields);
      }
  
      if (currentIndex === 4) {
        const numberFields = $('input.bulletins-field');
        let votersCheck = validateVoterFields();
        let bulletinsCheck = validateBulletinFields();
        let bulletinsTotalCheck = validateBulletinsTotal();
  
        if (!bulletinsCheck.valid) {
          formErrors.bulletins = bulletinsCheck.message;
        } else {
          delete formErrors.bulletins;
        }
  
        if (!votersCheck.valid) {
          formErrors.voters = votersCheck.message;
        } else {
          delete formErrors.voters;
        }
        
        if (!bulletinsTotalCheck.valid) {
          formErrors.bulletinsTotal = bulletinsTotalCheck.message;
        } else {
          delete formErrors.bulletinsTotal;
        }
    
        $('.error-message').remove();
        formValid = validateNumberFields(numberFields);
      }
      
      // Last step
      if (currentIndex === 5 && form.valid()) {
        const errorFields = Object.keys(formErrors);
        
        if (errorFields.length > 0) {
          formValid = false;
          errorFields.forEach(f => {
            $(`*[data-message="${f}"]`).removeClass('d-none');
          });
          errorsModal.modal('show');
        } else {
          formValid = true;
          submitClosingProtocolForm();
        }
      }

      return formValid && form.valid();
    },
    onFinishing: function (event, currentIndex) {
      form.validate().settings.ignore = ":disabled";
      return form.valid();
    }
  });
})
