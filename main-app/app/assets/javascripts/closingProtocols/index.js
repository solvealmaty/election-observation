$(function () {
  $(document).on('click', '.show-protocol-images', function () {
    let protocolId = this.dataset['id'];
    $(`#protocol-images-${protocolId}`).toggleClass('display-none', 'display-block');
  });
})
