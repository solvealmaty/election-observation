//= require jquery3
//= require jquery_ujs
//= require popper
//= require bootstrap-sprockets
//= require cable.js
//= require plugins/datatables.min
//= require plugins/select2.min
//= require support/datatablesRU
//= require support/toastr

//= require closingProtocols/selectors
//= require closingProtocols/filters
