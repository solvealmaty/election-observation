let districtStationsMap;

$(function () {
  if (document.getElementById('results-district-stations-map')) {
    ymaps.ready(initDistrictResultsMaps);
  } else {
    return false;
  }
  
  function initDistrictResultsMaps() {
    let stationsGeoObjects = new ymaps.GeoObjectCollection();
    let redStationsPromises;
    let grayStationsPromises;
    let coordinates;
  
    districtStationsMap = new ymaps.Map("results-district-stations-map", {
      center: [43.23, 76.94], // Almaty
      zoom: 12,
      controls: ['smallMapDefaultSet']
    });
    
    function createPlacemarkByCoordsPromise(station, preset) {
      coordinates = station.coordinates.split(',').map(coord => parseFloat(coord));
    
      if (station.fullAddress)
        return addPlacemarkToCollection(station, {
          coordinates: coordinates,
          hintContent: station.name + ', ' + station.id + ', ' + station.fullAddress,
          preset: preset
        });
    
      let getAddressPromise = new Promise((resolve, reject) => {
        resolve(ymaps.geocode(coordinates));
      });
    
      getAddressPromise.then(function (result) {
        let firstGeoObject = result.geoObjects.get(0);
      
        if (firstGeoObject) {
          let locationData = firstGeoObject.properties.get('metaDataProperty');
          let locationComponents = locationData.GeocoderMetaData.Address.Components;
          let locationStreet = locationComponents[3].name;
          let locationHouseNumber = locationComponents[4].name;
        
          let address = `${locationStreet}, ${locationHouseNumber}`;
        
          addPlacemarkToCollection(station, {
            coordinates: coordinates,
            hintContent: station.name + ', ' + station.id + ', ' + address,
            preset: preset
          });
        }
      });
    
      return getAddressPromise;
    }
  
    function createPlacemarkByAddressPromise(station, preset) {
      let address = station.fullAddress;
      let getCoordsPromise = new Promise((resolve, reject) => {
        resolve(ymaps.geocode(address, {results: 1, boundedBy: districtStationsMap.getBounds()}));
      });
    
      getCoordsPromise.then(function (res) {
        if (res.geoObjects.get(0)) {
          let coordinates = res.geoObjects.get(0).geometry.getCoordinates();
        
          addPlacemarkToCollection(station, {
            coordinates: coordinates,
            hintContent: station.id + ', ' + station.fullAddress,
            preset: preset
          });
        }
      });
    
      return getCoordsPromise;
    }
  
    function addPlacemarkToCollection(station, properties) {
      let stationPlacemark = new ymaps.Placemark(
        properties.coordinates,
        {
          hintContent: properties.hintContent,
          balloonContent: `<div>${properties.hintContent}</div>
            <div><a href=${gon.errorLink} class="station-point-error-link" target="_blank">Сообщить об ошибке</a></div>`,
          id: station.id
        },
        {preset: properties.preset, draggable: true}
      );
    
      stationsGeoObjects.add(stationPlacemark);
    }
    
    $('#district-results-show-stations').on('click', function () {
      if (gon.correct_protocols_stations.length === 0 && gon.non_correct_protocols_stations.length === 0) return;
  
      loadingIndicator(true);
      
      redStationsPromises = gon.correct_protocols_stations.map(station => {
        let presetColor = 'islands#redDotIcon'
        if (station.coordinates) {
          return createPlacemarkByCoordsPromise(station, presetColor);
        } else if (station.fullAddress) {
          return createPlacemarkByAddressPromise(station, presetColor);
        }
      });
      
      grayStationsPromises = gon.non_correct_protocols_stations.map(station => {
        let presetColor = 'islands#grayDotIcon';
        
        if (station.coordinates) {
          return createPlacemarkByCoordsPromise(station, presetColor);
        } else if (station.fullAddress) {
          return createPlacemarkByAddressPromise(station, presetColor);
        }
      });
  
      Promise.all(redStationsPromises.concat(grayStationsPromises).filter(promise => !!promise))
      .then(() => {
        districtStationsMap.geoObjects.add(stationsGeoObjects);
    
        let myMapBounds = districtStationsMap.geoObjects.getBounds();
    
        if (myMapBounds) {
          // Масштабируем и выравниваем карту так, чтобы были видны все метки
          districtStationsMap.setBounds(myMapBounds, {checkZoomRange: true}).then(function () {
            if (districtStationsMap.getZoom() > 12) districtStationsMap.setZoom(12);
          });
        }

        loadingIndicator(false);
      });
    })
  }
})
