//= require '../support/loadingIndicator'

let myMap;

$(function () {
  if (document.getElementById('stations_info_map')) {
    ymaps.ready(initYMaps);
  } else {
    return false;
  }

  function initYMaps() {
    myMap = new ymaps.Map("stations_info_map", {
      center: [43.23, 76.94], // Almaty
      // center: [55.72532368326033, 37.748675112058876], // Moscow
      // Уровень масштабирования. Допустимые значения: от 0 (весь мир) до 19.
      zoom: 12,
      controls: ['smallMapDefaultSet']
    });
  }
});

function reInitYMaps(stations) {
  loadingIndicator(true);

  // удаляем существующую коллекцию меток
  myMap.geoObjects.removeAll();

  if (stations.regionStationsInfo.length === 0 && stations.filteredStationsInfo.length === 0) return;

  // Создаём коллекцию для добавления меток
  let objects = new ymaps.GeoObjectCollection();

  let regionStationsObjectsPromises = stations.regionStationsInfo.map(station => {
    let presetColor = station.isObserverStation ? 'islands#darkGreenDotIcon' : 'islands#grayDotIcon';

    if (station.coordinates) {
      return createPlacemarkByCoordsPromise(station, presetColor);
    } else if (station.address) {
      return createPlacemarkByAddressPromise(station, presetColor);
    }
  });

  let filteredStationsObjectsPromises = stations.filteredStationsInfo.map(station => {
    let presetColor = station.isObserverStation ? 'islands#darkGreenDotIcon' : 'islands#redDotIcon';

    if (station.coordinates) {
      return createPlacemarkByCoordsPromise(station, presetColor);
    } else if (station.address) {
      return createPlacemarkByAddressPromise(station, presetColor);
    }
  });

  function createPlacemarkByCoordsPromise(station, preset) {
    let coordinates = station.coordinates.split(',').map(coord => parseFloat(coord));

    if (station.address)
      return addPlacemarkToCollection(station,{
        coordinates: coordinates,
        hintContent: station.name + ', ' + station.address,
        preset: preset
      });

    let getAddressPromise = new Promise((resolve, reject) => {
      resolve(ymaps.geocode(coordinates));
    });

    getAddressPromise.then(function(result) {
      let firstGeoObject = result.geoObjects.get(0);

      if (firstGeoObject) {
        let locationData       = firstGeoObject.properties.get('metaDataProperty');
        let locationComponents = locationData.GeocoderMetaData.Address.Components;
        let locationStreet     = locationComponents[3].name;
        let locationHouseNumber = locationComponents[4].name;

        let address = `${locationStreet}, ${locationHouseNumber}`;

        addPlacemarkToCollection(station,{
          coordinates: coordinates,
          hintContent: station.name + ', ' + address,
          preset: preset
        });
      }
    });

    return getAddressPromise;
  }

  function createPlacemarkByAddressPromise(station, preset) {
    let address = station.fullAddress;
    let getCoordsPromise = new Promise((resolve, reject) => {
      resolve(ymaps.geocode(address, { results: 1, boundedBy: myMap.getBounds() }));
    });

    getCoordsPromise.then(function (res) {
      if (res.geoObjects.get(0)) {
        let coordinates = res.geoObjects.get(0).geometry.getCoordinates();

        addPlacemarkToCollection(station,{
          coordinates: coordinates,
          hintContent: station.name + ', ' + station.address,
          preset: preset
        });
      }
    });

    return getCoordsPromise;
  }

  function addPlacemarkToCollection(station, properties) {
    let placemarkInfo = station.isObserverStation ? `Вы - наблюдатель данного участка` : `<a href="/stations/${station.id}">Стать наблюдателем</a>`;
    let stationPlacemark = new ymaps.Placemark(
      properties.coordinates,
      {
        hintContent: properties.hintContent,
        balloonContent: `<div>${properties.hintContent}</div>
                         <div>Организации: <span>${station.orgs}</span></div>
                         <div>Тип ОО: <span>${station.orgType}</span></div>
                         <div>Тип наблюдателей: <span>${station.observerType}</span></div>
                         <div><a href=${gon.errorLink} class="station-point-error-link" target="_blank">Сообщить об ошибке</a></div>
                         <div>${placemarkInfo}</div>`,
        id: station.id
      },
      { preset: properties.preset, draggable: true }
    );

    objects.add(stationPlacemark);
  }

  Promise.all(regionStationsObjectsPromises.concat(filteredStationsObjectsPromises).filter(promise => !!promise))
    .then(() => {
      myMap.geoObjects.add(objects);

      let myMapBounds = myMap.geoObjects.getBounds();

      if (myMapBounds) {
        // Масштабируем и выравниваем карту так, чтобы были видны все метки
        myMap.setBounds(myMapBounds, { checkZoomRange:true }).then(function() {
          if(myMap.getZoom() > 12) myMap.setZoom(12);
        });
      }

      loadingIndicator(false);
  });
}
