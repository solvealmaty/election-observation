$(function () {
  $("#results-select-region, #results-select-local-area").select2();
  $("#select-district").select2({
    placeholder: "Выберите избирательный округ ",
    allowClear: true
  });
  $("#select-district").on('select2:select', function(e) {
    window.location = e.target.value
  });
  
  $(document).on('change', '#region_id', function () {
    loadingIndicator(true);
    
    let regionId  = document.getElementById('region_id').value;
    
    $.ajax({
      method: 'GET',
      url: `/api/v1/regions/${regionId}`,
      success: function (data, status, xhr) {
        if (xhr.status !== 200) {
          return toastErrors(xhr.status);
        }
        
        const localAreasSelector = $('#local_area_id');
        const stationsSelector = $('#station_filter_selector');
        
        localAreasSelector.empty();
        stationsSelector.empty();
        
        data.region.local_areas.forEach(l => {
          localAreasSelector.append(
            `<option value="${l.id}">${l.name}</option>`
          );
        });
        
        data.region.stations.forEach(s => {
          stationsSelector.append(
            `<option value="${s.id}">${s.name}</option>`
          );
        });
        
        loadingIndicator(false);
      }
    })
  });
  
  $(document).on('change', '#local_area_id', function () {
    loadingIndicator(true);
    
    let localAreaId  = document.getElementById('local_area_id').value;
    
    $.ajax({
      method: 'GET',
      url: `/api/v1/stations/${localAreaId}`,
      success: function (data, status, xhr) {
        if (xhr.status !== 200) {
          return toastErrors(xhr.status);
        }
        
        const stationsSelector = $('#station_filter_selector');
        
        stationsSelector.empty();
        
        data.stations.forEach(s => {
          stationsSelector.append(
            `<option value="${s.id}">${s.name}</option>`
          );
        });
        
        loadingIndicator(false);
      }
    });
  });
  
  $('.orgs-dropdown').on('click', function () {
    $('.orgs-checkboxes').toggleClass('display-none', 'display-block');
  });
  
  $("#results-select-region").on("change", function(e) {
    loadingIndicator(true);

    let electionId = document.getElementById('election-id').value;
    let districtId = document.getElementById('district-id').value;
    let regionId = document.getElementById('results-select-region').value;

    $.ajax({
      method: 'GET',
      url: `/results/elections/${electionId}/districts/${districtId}/local_areas.json`,
      data: {
        region_id: regionId
      },
      success: function (response) {
        const localAreasSelector = $('#results-select-local-area');

        localAreasSelector.empty();
        localAreasSelector.append(
          `<option value="">Не выбрано</option>`
        );

        response.forEach(l => {
          localAreasSelector.append(
            `<option value="${l.id}">${l.name}</option>`
          );
        });

        loadingIndicator(false);
      }
    });
  });
})
