wb = xlsx_package.workbook

@elections_info.each do |election_info|
  election_id = election_info[:id]
  closing_protocols = election_info[:closing_protocols]
  date = closing_protocols.any? ? l(closing_protocols.last.created_at, format: :long) : ''

  closing_protocol_info = [
    { title: 'Общее число избирателей', value: closing_protocols.pluck(:total_voters).reduce(:+) },
    { title: 'Общее число избирателей, принявших участие', value: closing_protocols.pluck(:active_voters).reduce(:+) },
    { title: 'Общее число избирательных бюллетеней', value: closing_protocols.pluck(:total_bulletins).reduce(:+) },
    { title: 'Число избирателей, получивших избирательные бюллетени', value: closing_protocols.pluck(:voters_bulletins).reduce(:+) },
    { title: 'Число избирательных бюллетеней, признанных недействительными', value: closing_protocols.pluck(:invalid_bulletins).reduce(:+) },
    { title: 'Число погашенных избирательных бюллетеней', value: closing_protocols.pluck(:canceled_bulletins).reduce(:+) },
    { title: 'Дата', value: date }
  ]

  election_info[:candidates_by_district].each.with_index(1) do |details, index|
    district = details.first
    candidates = details.second

    wb.add_worksheet(name: "#{election_id}-#{index}") do |sheet|
      sheet.add_row ["#{election_info[:name]}".upcase]
      sheet.add_row ['']

      # Create the header for total info
      sheet.add_row ['Название', 'Значение'].map(&:upcase)

      # Create entries for total info
      closing_protocol_info.each do |info|
        sheet.add_row [info[:title], info[:value]]
      end
      sheet.add_row ['Избирательный Округ', district]

      2.times { sheet.add_row [''] }
      sheet.add_row ['Результаты по избирательным участкам'.upcase]

      # Create the header row for stations info
      headers = [
        'Номер',
        'Наименование избирательного участка',
        'Местонахождение избирательного участка',
        'Общее число избирателей',
        'Общее число избирателей, принявших участие',
        'Общее число избирательных бюллетеней',
        'Число избирателей, получивших избирательные бюллетени',
        'Число избирательных бюллетеней, признанных недействительными',
        'Число погашенных избирательных бюллетеней',
        'Число кандидатов'
      ]
      candidates = candidates.sort_by(&:id)
      candidates.each.with_index(1) { |candidate, index| headers << "Кандидат #{index}: #{candidate.name} (число голосов)" }
      sheet.add_row headers

      # Create entries for stations info
      closing_protocols.each.with_index(1) do |protocol, index|
        station = protocol.station
        station_protocol_info = [
          index,
          station.name,
          station.full_address,
          protocol.total_voters,
          protocol.active_voters,
          protocol.total_bulletins,
          protocol.voters_bulletins,
          protocol.invalid_bulletins,
          protocol.canceled_bulletins,
          candidates.count
        ]

        # add votes counts for candidates
        candidates_protocols = protocol.candidates_closing_protocols.includes(:candidate)
        candidates.each do |candidate|
          candidate_protocol = candidates_protocols.where(candidates: { id: candidate.id }).first
          candidate_votes = candidate_protocol.present? ? candidate_protocol.votes : 0
          station_protocol_info << candidate_votes
        end

        sheet.add_row station_protocol_info
      end
    end
  end
end
