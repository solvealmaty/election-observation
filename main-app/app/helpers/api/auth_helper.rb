module Api
  module AuthHelper
    def current_user
      @current_user ||= user_from_token || user_from_session
    end

    private

    def user_from_token
      User.find_by(access_token: access_token)
    end

    def user_from_session
      return unless session
      User.find_by(id: session[:user_id])
    end

    def access_token
      # break if rails controller
      return if request.headers.is_a? ActionDispatch::Http::Headers

      auth_header = request.headers.slice('Authorization')
      auth_header['Authorization'].to_s.split.last
    end
  end
end
