module ApplicationHelper
  def district_options(election_id, electoral_districts)
    options_for_select(
      electoral_districts.includes(:scopeable).map do |district|
        [district.scopeable.full_name, results_election_district_url(election_id, district.id)]
      end
    )
  end

  def current_translations
    @translations ||= I18n.backend.translations
    @translations[I18n.locale].with_indifferent_access
  end
end
