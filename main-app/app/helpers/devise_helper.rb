module DeviseHelper
  def devise_error_messages!
    return '' if resource.errors.empty?

    messages = safe_join(resource.errors.full_messages.map { |msg| content_tag(:li, msg) })

    content_tag :div, class: 'alert alert-error alert-danger' do
      content_tag :button, 'x', type: :button, class: :close, 'data-dismiss': :alert
      messages
    end
  end
end
