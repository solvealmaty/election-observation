module Cabinet::ApplicationHelper
  def certificate_exists?(certificate)
    I18n.t("cabinet.observers_stations.certification.#{certificate.present? ? 'has_certificate' : 'no_certificate'}")
  end

  def closing_protocol_statuses
    ClosingProtocol.statuses.to_a.map do |status|
      [I18n.t("activerecord.attributes.closing_protocol.statuses.#{status[0]}"), status[1]]
    end
  end

  def closing_protocol_conflicts
    %w[true false].map do |conflict|
      [I18n.t("activerecord.attributes.closing_protocol.conflict.#{conflict}"), conflict]
    end
  end
end
