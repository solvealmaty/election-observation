module Admin::ApplicationHelper
  def toastr_flash
    flash_messages = []
    flash.each do |type, message|
      next unless message

      type = 'success' if type == 'notice'
      type = 'error' if type == 'alert'
      flash_messages << javascript_tag("toastr.#{type}('#{message}');")
    end

    safe_join(flash_messages)
  end

  def active_controller?(controller_name, class_name = nil)
    class_name || 'active' if params[:controller] == controller_name
  end

  def active_action?(action_name)
    params[:action] == action_name ? 'active' : nil
  end

  def checkmark(value)
    value ? 'fa-check-square-o' : 'fa-square-o'
  end

  def election_ready?(election)
    candidates_status = election.candidates_exist? ? '' : t('admin.pages.elections.index.no_candidates')
    stations_status = election.stations.present? ? '' : t('admin.pages.elections.index.no_stations')

    if election.draft?
      content_tag :div do
        content = content_tag(:p, "#{candidates_status}", class: 'readiness-check')
        content << content_tag(:p, "#{stations_status}", class: 'readiness-check')
      end
    else
      content_tag(:i, nil, class: "fa #{checkmark(!election.draft?)} checkmark-icon")
    end
  end

  def local_area_district(local_area)
    local_area.electoral_district || local_area.region.electoral_district || local_area.country.electoral_district
  end

  def profile_link(link)
    return if link.blank?

    link
  end

  def affiliated?(org, election_group)
    affiliated_candidates = org.candidates.includes(election: [:election_group])
                             .where(election_groups: { id: election_group.id })

    if affiliated_candidates.present?
      affiliated_candidates.map { |c| "#{c.name}, #{c.info}" }.join('; ')
    else
      I18n.t('admin.pages.election_groups.public_organizations.index.not_affiliated')
    end
  end

  def full_district_name(district)
    return 'Не указан' if district.nil?

    if district.level == 1 || district.level == 2
      district.scopeable.name
    elsif district.level == 3
      "#{district.scopeable.region.name} - #{district.scopeable.name}"
    end
  end
end
