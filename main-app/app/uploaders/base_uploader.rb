class BaseUploader < CarrierWave::Uploader::Base
  storage Rails.env.production? ? :aws : :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def extension_whitelist
    %w[jpg jpeg png]
  end

  def size_range
    return unless Rails.env.production?

    10.kilobytes..10.megabytes
  end
end
