class ClosingProtocolUploader < BaseUploader
  include CarrierWave::MiniMagick

  def initialize(*)
    super

    return unless Rails.env.production?

    self.aws_bucket = ENV.fetch('STORAGE_CLOSING_PROTOCOLS_BUCKET_NAME')
    self.aws_acl = 'public-read'
  end
end
