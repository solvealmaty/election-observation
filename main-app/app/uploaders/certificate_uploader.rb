class CertificateUploader < BaseUploader
  include CarrierWave::MiniMagick

  def initialize(*)
    super

    return unless Rails.env.production?

    self.aws_bucket = ENV.fetch('STORAGE_CERTIFICATES_BUCKET_NAME')
    self.aws_acl = 'private'
  end

  version :thumbnail do
    process resize_to_fit: [100, 100]
  end
end
