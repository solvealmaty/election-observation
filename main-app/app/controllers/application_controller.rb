require 'application_responder'

class ApplicationController < ActionController::Base
  layout 'application'

  protect_from_forgery prepend: true

  before_action :clear_stale_session,
                :save_current_user,
                :save_error_link,
                :detect_page_speed_insights

  self.responder = ApplicationResponder
  respond_to :html

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, alert: exception.message
  end

  def election_group
    @election_group ||= ElectionGroup.order(:date).last
  end

  private

  def clear_stale_session
    return if current_user

    session[:user_id] = nil
  end

  def save_current_user
    gon.current_user = current_user if current_user
  end

  def save_error_link
    gon.errorLink = ENV.fetch('LINK_TO_REPORT_ERROR')
  end

  def detect_page_speed_insights
    @page_speed_insights = request.user_agent&.include?('Chrome-Lighthouse')
  end

  def after_sign_in_path_for(resource)
    stored_location_for(resource) ||
      if resource.coordinator? || resource.representative?
        cabinet_root_path
      else
        super
      end
  end
end
