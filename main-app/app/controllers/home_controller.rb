class HomeController < ApplicationController
  before_action :set_regions, only: %i[index filter_stations]

  respond_to :js

  def index
    if Time.zone.now > results_publishing_time
      redirect_to results_elections_path
    else
      set_observers_info
    end
  end

  def filter_stations
    stations_info = ::Stations::Filter.call(
      election_group: election_group,
      public_organization_ids: nil,
      public_organization_type: '',
      observer_type: 'any',
      region_id: params[:region_id],
      local_area_id: nil,
      station_ids: nil,
      observer_id: current_user&.id
    ).stations_info

    render json: stations_info.to_json
  end

  private

  def set_regions
    @regions = Region.order(:name)
  end

  def set_observers_info
    observers = User.group_observers(election_group.id)
    @observers_count = observers.count
    @certified_observers_count = observers.certified_observers.count
    @uncovered_stations_count = election_group.stations.without_observers.count
  end

  def results_publishing_time
    election_group.date.beginning_of_day.since((24 + ENV.fetch('SHOW_RESULTS_OFFSET').to_i).hours)
  end
end
