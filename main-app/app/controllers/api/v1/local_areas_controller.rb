class Api::V1::LocalAreasController < Api::V1::BaseController
  before_action :set_local_area

  def show
    render json: @local_area, serializer: LocalAreaSerializer, each_serializer: StationSerializer
  end

  private

  def set_local_area
    @local_area = LocalArea.find_by(id: params[:id])
  end
end
