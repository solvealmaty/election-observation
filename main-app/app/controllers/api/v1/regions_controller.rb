class Api::V1::RegionsController < Api::V1::BaseController
  before_action :set_region

  def show
    render json: @region, serializer: RegionSerializer, each_serializer: LocalAreaSerializer
  end

  private

  def set_region
    @region = Region.find_by(id: params[:id])
  end
end
