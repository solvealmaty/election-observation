class Api::V1::BaseController < ApplicationController
  before_action :authenticate_user!

  respond_to :json
  protect_from_forgery with: :null_session
end
