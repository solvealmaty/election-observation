class Api::V1::CandidatesController < Api::V1::BaseController
  before_action :set_election, :set_station, :set_candidates, only: :index

  def index
    render json: @candidates, each_serializer: CandidateSerializer
  end

  private

  def set_election
    @election = Election.find_by(id: params[:election_id])
  end

  def set_station
    @station = Station.find(params[:station_id])
  end

  def set_candidates
    district = @election.district_by_station(@station)
    @candidates = district.present? ? district.candidates.where(election_id: @election.id) : nil
    gon.watch.current_protocol_candidates = @candidates
  end
end
