class Api::V1::ElectionsController < Api::V1::BaseController
  before_action :set_election

  def show
    render json: @election, serializer: ElectionSerializer, each_serializer: CandidateSerializer
  end

  private

  def set_election
    @election = Election.find_by(id: params[:id])
  end
end
