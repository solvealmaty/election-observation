class Api::V1::Mobile::UsersController < Api::V1::BaseController
  def me
    respond_with(current_user, serializer: ::Mobile::UserSerializer)
  end
end
