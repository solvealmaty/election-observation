class ClosingProtocolsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_candidates, :set_locations, only: :new

  respond_to :js

  def index
  end

  def new
    @protocol = ClosingProtocol.new
  end

  def show
  end

  def create
    @result = ::ClosingProtocols::Create.call(
      params: protocol_params.merge(images: params[:images]),
      current_user: current_user
    )

    respond_to do |format|
      format.js
      format.html {
        if @result.success?
          flash[:notice] = I18n.t('messages.actions.create.success')
        else
          flash[:error] = @result.message
        end

        redirect_to root_path
      }
    end
  end

  private

  def protocol_params
    params.require(:closing_protocol).permit(
      :station_id,
      :election_id,
      :total_voters,
      :absentee_certificate_voters,
      :outdoors_voters,
      :total_bulletins,
      :voters_bulletins,
      :active_voters,
      :invalid_bulletins,
      :canceled_bulletins,
      images: [],
      candidates_closing_protocols_attributes: [
        :candidate_id,
        :votes
      ]
    )
  end

  def set_statuses
    @statuses = ClosingProtocol.statuses.to_a
  end

  def set_candidates
    @candidates = Election.first.candidates
  end

  def set_locations
    @regions = Region.order(:id)
    @current_station = current_user.station
    @current_local_area = @current_station&.local_area
    @local_areas = if @current_local_area.present?
                     @current_local_area.region.local_areas.order(:name)
                   else
                     @regions.first.local_areas.order(:name)
                   end
    @stations = @local_areas.first.stations
  end
end
