class Cabinet::ElectionGroupsController < Cabinet::ApplicationController
  before_action :set_regions, only: :index

  def index
    @election_groups = current_org.election_groups.order(date: :desc)
  end

  private

  def set_regions
    @regions = Region.includes(local_areas: {stations: {observers_stations: [:user]}})
      .where(
        local_areas: {
          stations: {
            observers_stations: {
              users: {
                public_organization_id: current_org.id
              }
            }
          }
        }).order(:name)
  end
end
