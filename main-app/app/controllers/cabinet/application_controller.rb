class Cabinet::ApplicationController < ApplicationController
  layout 'cabinet'

  before_action :authenticate!
  before_action :require_organization_user
  before_action :current_org

  def current_org
    @public_organization ||= current_user.public_organization
  end

  private

  def authenticate!
    redirect_to cabinet_sign_in_path unless user_signed_in?
  end

  def require_organization_user
    return if current_user.coordinator? || current_user.representative?

    redirect_to root_path
  end
end
