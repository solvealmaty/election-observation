class Cabinet::UsersController < Cabinet::ApplicationController
  before_action :check_representative_rights
  before_action :set_user, only: %i[edit update destroy]

  def index
    @users = current_org.users.order(role: :desc)
  end

  def new
    @coordinator = current_org.users.new(role: 'coordinator')
  end

  def create
    @coordinator = current_org.users.create(user_params.merge(role: 'coordinator'))
    @coordinator.send_reset_password_instructions unless @coordinator.errors.any?

    respond_with(@coordinator, location: cabinet_users_path)
  end

  def edit
  end

  def update
    @user.update(user_params)
    respond_with(@user, location: cabinet_users_path)
  end

  def destroy
    respond_with(@user.destroy, location: cabinet_users_path)
  end

  private

  def user_params
    collection = params
      .require(:user)
      .permit(
        :email,
        :first_name,
        :last_name,
        :social_network
      )

    collection.merge(password: generate_password)
  end

  def generate_password
    SecureRandom.hex(3)
  end

  def check_representative_rights
    return if current_user.representative?

    redirect_to cabinet_root_path
  end

  def set_user
    @user = User.find(params[:id])
  end
end
