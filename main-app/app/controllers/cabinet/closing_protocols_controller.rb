class Cabinet::ClosingProtocolsController < Cabinet::ApplicationController
  before_action :set_closing_protocol, only: %i[show approve reject batch_reject delete]
  before_action :set_candidates_protocols, only: %i[show]
  before_action :save_filters, only: :filter

  respond_to :js

  def index
    @closing_protocols = ClosingProtocol.list.page(params[:page]).without_count

    respond_to do |format|
      format.js
      format.html
    end
  end

  def show
  end

  def filter
    @closing_protocols = ClosingProtocol.base_filter(filter_params).page(params[:page]).without_count

    respond_to do |format|
      format.js
      format.html
    end
  end

  def approve
    @result = ::ClosingProtocols::Approve::Organize.call(protocol: @closing_protocol)
    set_update_message

    redirect_to cabinet_closing_protocol_path(@closing_protocol)
  end

  def reject
    @result = ::ClosingProtocols::Reject::Organize.call(protocol: @closing_protocol)
    set_update_message

    redirect_to cabinet_closing_protocol_path(@closing_protocol)
  end

  def search
    @closing_protocols = ClosingProtocol.search(search_params).page(params[:page]).without_count

    respond_to do |format|
      format.js
      format.html
    end
  end

  def delete
    result = @closing_protocol.update(deleted: true)
    set_update_message(success: result)

    redirect_to cabinet_closing_protocols_path
  end

  def batch_delete
    ::ClosingProtocols::Delete::Batch.call(protocol_ids: params[:ids])
  end

  def batch_reject
    @result = ::ClosingProtocols::Reject::Batch.call(protocol: @closing_protocol)
    set_update_message

    redirect_to cabinet_closing_protocols_path
  end

  def reset_filter
    session[:closing_protocols_filter] = {}
  end

  private

  def set_closing_protocol
    @closing_protocol = ClosingProtocol.find_by(id: params[:id])
  end

  def set_candidates_protocols
    @candidates_protocols = @closing_protocol.candidates_closing_protocols.includes(:candidate)
  end

  def search_params
    params.permit(:identifier)
  end

  def filter_params
    params.permit(
      :election_id,
      :public_organization_id,
      :region_id,
      :local_area_id,
      :station_id,
      :status,
      :conflict
    )
  end

  def set_update_message(success: true)
    if @result&.success? || success
      flash[:notice] = I18n.t('messages.actions.update.success')
    else
      flash[:error] = @result&.message || I18n.t('messages.actions.update.failure')
    end
  end

  def save_filters
    session[:closing_protocols_filter] = filter_params.to_h.symbolize_keys
  end
end
