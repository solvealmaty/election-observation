class Cabinet::ElectionGroups::ObserversStationsController < Cabinet::ApplicationController
  before_action :set_election_group,
                :set_region, only: %i[index show approve reject]
  before_action :set_observers_station, only: %i[show approve reject]

  def index
    @observers_stations = @election_group.stations
                            .includes(observers_stations: { user: [:public_organization] })
                            .where(users: { public_organization_id: current_org.id })
                            .where(local_area_id: @region.local_area_ids)
                            .map(&:observers_stations).flatten.sort_by { |os| [os.certification, os.created_at] }
  end

  def show
  end

  def approve
    result = ::ElectionGroups::ObserversStations::ApproveCertificate.call(
      observers_station: @observers_station,
      accreditor_id: current_user.id
    )

    if result.observers_station.approved?
      flash[:notice] = I18n.t('messages.observers_stations.approve.success')
    else
      flash[:error] = I18n.t('messages.observers_stations.approve.fail')
    end

    redirect_to cabinet_election_group_observers_station_path(@election_group, @observers_station, region_id: @region.id)
  end

  def reject
    result = ::ElectionGroups::ObserversStations::RejectCertificate.call(
      observers_station: @observers_station,
      rejection_reason: observers_station_params[:rejection_reason],
      accreditor_id: current_user.id
    )

    if result.observers_station.rejected?
      flash[:notice] = I18n.t('messages.observers_stations.reject.success')
    else
      flash[:error] = I18n.t('messages.observers_stations.approve.fail')
    end

    redirect_to cabinet_election_group_observers_station_path(@election_group, @observers_station, region_id: @region.id)
  end

  private

  def set_election_group
    @election_group = ElectionGroup.find(params[:election_group_id])
  end

  def set_region
    @region = Region.find(params[:region_id])
  end

  def set_observers_station
    @observers_station = ObserversStation.find(params[:id])
  end

  def observers_station_params
    params.require(:observers_station).permit(:rejection_reason)
  end
end
