class Results::Elections::DistrictsController < ApplicationController
  include ResultsDownloaded

  before_action :set_election, :set_district, only: %i[show local_areas]
  before_action :set_region, only: :local_areas

  respond_to :js

  def show
    result = Results::Districts::New.call(
      region_id: params[:region_id],
      local_area_id: params[:local_area_id],
      page: params[:page],
      district: @district,
      election: @election
    )

    @general_details = result.general_details
    @results = result.district_results
    gon.correct_protocols_stations = result.red_stations
    gon.non_correct_protocols_stations = result.gray_stations

    base_responders_with_xlsx("Результаты_выборов_#{@election.id}-#{@district.id}")
  end

  def local_areas
    local_areas = @region.local_areas.includes(stations: :closing_protocols)
                    .where(stations: { closing_protocols: { status: 'correct' }})

    render json: local_areas.to_json
  end

  private

  def set_district
    @district = ElectoralDistrict.find(params[:id])
  end

  def set_election
    @election = Election.find(params[:election_id])
  end

  def set_region
    @region = Region.find(params[:region_id])
  end
end
