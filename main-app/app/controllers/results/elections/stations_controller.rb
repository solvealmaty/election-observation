class Results::Elections::StationsController < ApplicationController
  include ResultsDownloaded

  before_action :set_station,
                :set_election,
                :set_district,
                :set_candidates, only: :show

  def show
    @station_closing_protocols = @station.closing_protocols
      .includes(candidates_closing_protocols: :candidate)
      .joins(:election)
      .where(election: @election).order(status: :desc)

    base_responders_with_xlsx("Результаты_выборов_#{@election.id}_#{@district.id}_#{@station.id}")
  end

  private

  def set_station
    @station = election_group.stations.find_by(id: params[:id])
  end

  def set_election
    @election = Election.find(params[:election_id])
  end

  def set_district
    @district = ElectoralDistrict.find(params[:district_id])
  end

  def set_candidates
    @candidates = @district.candidates.where(election_id: @election.id)
  end
end
