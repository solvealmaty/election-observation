class Results::ElectionsController < ApplicationController
  include ResultsDownloaded

  before_action :set_election, only: %i[show local_areas]

  def index
    @elections = election_group.elections

    base_responders_with_xlsx("Результаты_выборов")
  end

  def show
    @electoral_districts = @election.electoral_districts.includes(:scopeable)

    redirect_to results_election_district_path(@electoral_districts.first) if @electoral_districts.size == 1

    base_responders_with_xlsx("Результаты_выборов_#{@election.id}")
  end

  private

  def set_election
    @election = Election.find(params[:id])
  end
end
