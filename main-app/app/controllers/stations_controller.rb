class StationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_public_orgs, only: %i[index show]
  before_action :set_regions,
                :set_local_areas,
                :set_stations, only: %i[index]
  before_action :set_station,  only: :show

  respond_to :js

  def index
    if params[:station].present?
      @stations_info = ::Stations::Filter.call(
        election_group: election_group,
        public_organization_ids: station_params[:public_organization_ids],
        public_organization_type: station_params[:public_organization_type],
        observer_type: station_params[:observer_type],
        region_id: station_params[:region_id],
        local_area_id: station_params[:local_area_id],
        station_ids: station_params[:station_ids],
        observer_id: current_user.id
      ).stations_info
    end

    respond_to do |format|
      format.html
      format.json { render json: @stations_info.to_json }
    end
  end

  def show
    @observers_station = current_user.observers_stations.find_by(station: @station) || @station.observers_stations.new
  end

  private

  def station_params
    params.require(:station).permit(
      :region_id,
      :local_area_id,
      :public_organization_type,
      :observer_type,
      public_organization_ids: [],
      station_ids: []
    )
  end

  def set_public_orgs
    @orgs = PublicOrganization.order(:name)
  end

  def set_station
    @station = Station.find(params[:id])
  end

  def set_regions
    @regions = Region.eager_load(local_areas: [:stations]).order(:name)
  end

  def set_local_areas
    @local_areas = @regions.first.local_areas.order(:name)
  end

  def set_stations
    @stations = @local_areas.first.stations.order(:name)
  end
end
