class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  skip_before_action :verify_authenticity_token, only: %i[facebook google_oauth2 vkontakte odnoklassniki]

  def facebook
    base_handler('facebook')
  end

  def google_oauth2
    base_handler('google')
  end

  def vkontakte
    base_handler('vkontakte')
  end

  def odnoklassniki
    base_handler('odnoklassniki')
  end

  def twitter
    base_handler('twitter')
  end

  def failure
    redirect_to root_path
  end

  private

  def base_handler(provider)
    @user = User.from_omniauth(request.env['omniauth.auth'])
    if @user.persisted?
      sign_in_and_redirect @user, event: :authentication
      session[:user_id] = @user.id
      set_flash_message(:notice, :success, kind: provider.capitalize) if is_navigational_format?
    else
      session["devise.#{provider}_data"] = request.env["omniauth.auth"].except(:extra) # Removing extra as it can overflow some session stores
      redirect_to root_path, alert: I18n.t('errors.user.authentication.fail')
    end
  end
end
