class Users::ProfilesController < Users::ApplicationController
  before_action :set_public_orgs, :set_regions, only: :show
  before_action :set_user, only: :update

  respond_to :js

  def show
    @current_station = current_user.stations.find_by(election_group_id: election_group.id)
    @observers_station = current_user.observers_stations.find_by(station: @current_station)
  end

  def update
    success = @user.update(user_profile_params)

    respond_to do |format|
      format.js {
        if success
          render json: I18n.t('messages.actions.update.success')
        else
          render status: :unprocessable_entity
        end
      }
      format.html
    end
  end

  private

  def user_profile_params
    params.permit(:first_name, :last_name, :email)
  end

  def set_public_orgs
    @orgs = PublicOrganization.order(:name)
  end

  def set_regions
    @regions = Region.order(:name)
  end

  def set_user
    @user = User.find_by(id: current_user.id)
  end
end
