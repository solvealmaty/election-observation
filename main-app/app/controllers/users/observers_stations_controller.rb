class Users::ObserversStationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_station, only: %i[create update]
  before_action :set_observers_station, only: %i[update destroy delete_certificate]

  def create
    result = ::Stations::ObserversStations::Add.call(
      station: @station,
      user: current_user,
      params: observers_station_params
    )

    flash[result.success? ? :notice : :error] = result.message
    redirect_to profile_path
  end

  def update
    result = ::Stations::ObserversStations::Update.call(
      observers_station: @observers_station,
      user: current_user,
      params: observers_station_params
    )

    flash[result.success? ? :notice : :error] = result.message
    redirect_to profile_path
  end

  def destroy
    @observers_station.update(station: nil)

    flash[:notice] = I18n.t('cabinet.users.observers_stations.destroy.success')
    redirect_to profile_path
  end

  def delete_certificate
    @observers_station.update(
      certificate: nil,
      certification: 'unapproved',
      accreditor_id: nil
    )

    flash[:notice] = I18n.t('cabinet.users.observers_stations.delete_certificate.success')

    redirect_to profile_path
  end

  private

  def observers_station_params
    params.require(:observers_station).permit(
      :station_id,
      :certificate,
      :certification_request,
      :public_organization_id
    )
  end

  def set_station
    @station = Station.find(observers_station_params[:station_id])
  end

  def set_observers_station
    @observers_station = ObserversStation.find(params[:id])
  end
end
