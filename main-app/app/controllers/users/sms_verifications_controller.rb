class Users::SmsVerificationsController < Devise::RegistrationsController
  before_action :set_user
  before_action :ensure_user_unconfirmed

  def request_code
    Users::Verification::New.call(user: @user)
  end

  def verify_account
    result = Users::Verification::Complete.call(user: @user, code: params[:code])

    if result.success?
      sign_in(:user, @user)
      redirect_to root_path
    else
      flash[:error] = result.message
      redirect_back fallback_location: verify_account_path
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def ensure_user_unconfirmed
    redirect_to new_user_session_path if @user.confirmed?
  end
end
