class Admins::PublicOrganizationsController < Admins::ApplicationController
  before_action :set_org, only: %w[show edit update destroy]

  def index
    respond_with(@orgs = PublicOrganization.all)
  end

  def new
    @org = PublicOrganization.new
    @user = @org.users.new

    respond_with(@org)
  end

  def create
    result = Admin::PublicOrganizations::New.call(org_params: org_params)

    if result.success?
      @org = result.org
      flash[:notice] = I18n.t('messages.actions.create.success')
      redirect_to admins_public_organizations_path
    else
      flash[:error] = result.message
      render action: 'new'
    end
  end

  def edit
  end

  def update
    @org.update(org_params)
    respond_with(@org, location: admins_public_organizations_path)
  end

  def destroy
    result = ::Admin::PublicOrganizations::Destroy.call(org: @org)

    if result.success?
      flash[:notice] = I18n.t('messages.actions.destroy.success')
    else
      flash[:error] = result.message
    end

    redirect_to admins_public_organizations_path
  end

  private

  def org_params
    params
      .require(:public_organization)
      .permit(
        :name,
        :address,
        :iin_bin,
        :phone,
        :social_network,
        :country_id,
        users_attributes: [
          :role,
          :email,
          :first_name,
          :last_name,
          :social_network
        ]
      )
  end

  def set_org
    @org = PublicOrganization.find(params[:id])
  end
end
