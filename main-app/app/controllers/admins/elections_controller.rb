class Admins::ElectionsController < Admins::ApplicationController
  before_action :set_election, only: %i[update show destroy]
  before_action :set_election_group, only: %i[create update destroy]
  before_action :set_election_groups, only: :index

  after_action :update_group_status, only: :destroy

  respond_to :js

  def index
    @elections = Election.includes(:election_group, :stations).order(:date)
  end

  def show
  end

  def create
    result = Election::Create.call(
      election_params: election_params,
      districts:       params[:election][:districts],
      candidates:      params[:election][:candidates]
    )

    if result.success?
      flash[:notice] = I18n.t('messages.actions.create.success')
    else
      flash[:error] = result.message
    end

    redirect_to admins_elections_path
  end

  def update
    result = Election::Update.call(
      election: @election,
      election_group: @election.election_group,
      election_params: election_params,
      districts:       params[:election][:districts],
      candidates:      params[:election][:candidates]
    )

    if result.success?
      flash[:notice] = I18n.t('messages.actions.update.success')
    else
      flash[:error] = result.message
    end

    redirect_to admins_elections_path
  end

  def destroy
    respond_with(@election.destroy, location: admins_elections_path)
  end

  private

  def election_params
    params.require(:election).permit(:name, :date, :pinned, :election_group_id)
  end

  def set_election
    @election = Election.find(params[:id])
  end

  def set_election_group
    id = params[:election].present? ? election_params[:election_group_id] : nil
    @election_group = id ? ElectionGroup.find(id) : @election.election_group
  end

  def set_election_groups
    gon.election_groups = ElectionGroup.all.collect { |eg| [eg.id, eg.date&.strftime('%Y-%m-%d')] }.to_h
  end

  def update_group_status
    ::ElectionGroups::UpdateStatus.call(election_group: @election_group)
  end
end
