class Admins::ElectionGroups::PublicOrganizationsController < Admins::ApplicationController
  before_action :set_election_group, only: %i[index update]
  before_action :set_public_org

  def index
    @orgs = @election_group.public_organizations.order(:name)
  end

  def update
    candidate_ids = params[:public_organization].present? ? public_org_params[:candidate_ids].map(&:to_i) : []
    @public_org.update(candidate_ids: candidate_ids)

    if @public_org.errors.blank?
      flash[:notice] = t('messages.actions.update.success')
    else
      flash[:error] = @public_org.errors.full_messages
    end

    redirect_to admins_election_group_public_organizations_path(@election_group)
  end

  private

  def set_election_group
    @election_group = ElectionGroup.find(params[:election_group_id])
  end

  def set_public_org
    @public_org = @election_group.public_organizations.find_by(id: params[:id])
  end

  def public_org_params
    params.require(:public_organization).permit(candidate_ids: [])
  end
end
