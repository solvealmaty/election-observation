class Admins::PublicOrganizations::UsersController < Admins::ApplicationController
  before_action :set_org
  before_action :set_user, only: %i[edit update destroy]

  def index
    @users = @org.users.order(:role, :email)
  end

  def new
    @user = @org.users.new
  end

  def create
    @user = @org.users.create!(user_params)
    @user.send_reset_password_instructions

    respond_with(@user, location: admins_public_organization_users_path)
  end

  def edit
  end

  def update
    @user.update(user_params)
    respond_with(@user, location: admins_public_organization_users_path)
  end

  def destroy
    respond_with(@user.destroy, location: admins_public_organization_users_path)
  end

  private

  def set_org
    @org = PublicOrganization.find(params[:public_organization_id])
  end

  def set_user
    @user = @org.users.find(params[:id])
  end

  def user_params
    collection = params
      .require(:user)
      .permit(
        :role,
        :first_name,
        :last_name,
        :email
      )

    collection.merge(password: generate_password)
  end

  def generate_password
    SecureRandom.hex(3)
  end
end
