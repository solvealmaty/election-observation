class Admins::ElectionGroupsController < Admins::ApplicationController
  before_action :set_election_group, only: %i[edit update publish destroy]

  def index
    respond_with(@election_groups = ElectionGroup.all)
  end

  def new
    respond_with(@election_group = ElectionGroup.new)
  end

  def edit
  end

  def create
    respond_with(@election_group = ElectionGroup.create(election_group_params), location: admins_election_groups_path)
  end

  def update
    @election_group.update(election_group_params)
    respond_with(@election_group, location: admins_election_groups_path)
  end

  def publish
    if @election_group.draft?
      flash[:error] = I18n.t('messages.election_group.published.failure')
    else
      @election_group.published!
      flash[:notice] = I18n.t('messages.election_group.published.success')
    end

    redirect_to admins_election_groups_path
  end

  def destroy
    respond_with(@election_group.destroy, location: admins_election_groups_path)
  end

  private

  def election_group_params
    params.require(:election_group).permit(:name, :date, :description)
  end

  def set_election_group
    @election_group = ElectionGroup.find(params[:id])
  end
end
