class Admins::LocalAreasController < Admins::ApplicationController
  before_action :set_local_area

  def update
    result = LocalAreas::ParentLevels::Update.call(local_area: @local_area, params: local_area_params)

    if result.success?
      flash[:notice] = I18n.t('messages.actions.update.success')
    else
      flash[:error] = result.message
    end

    redirect_to admins_elections_path
  end

  private

  def set_local_area
    @local_area = LocalArea.find(params[:id])
  end

  def local_area_params
    params.require(:local_area).permit(:name, region: [:id, :name], country: [:id, :name])
  end
end
