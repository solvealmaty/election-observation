class Admins::ApplicationController < ActionController::Base
  layout 'admins'

  protect_from_forgery prepend: true
  before_action :authenticate_admin!

  self.responder = ApplicationResponder
  respond_to :html
end
