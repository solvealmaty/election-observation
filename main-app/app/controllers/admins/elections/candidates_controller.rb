class Admins::Elections::CandidatesController < Admins::ApplicationController
  before_action :set_election, only: %i[index new create edit update destroy]
  before_action :set_candidate, only: %i[edit update destroy]

  def index
    respond_with(@candidates = @election.candidates.includes(electoral_district: [:scopeable]))
  end

  def new
    respond_with(@candidate = @election.candidates.new)
  end

  def create
    respond_with(@candidate = Candidate.create(candidate_params), location: admins_election_candidates_path(@election))
  end

  def edit
  end

  def update
    @candidate.update(candidate_params)
    respond_with(@candidate, location: admins_election_candidates_path(@election))
  end

  def destroy
    @candidate.destroy
    respond_with(@candidate, location: admins_election_candidates_path(@election))
  end

  private

  def candidate_params
    params.require(:candidate).permit(:name, :info, :election_id)
  end

  def set_candidate
    @candidate = Candidate.find(params[:id])
  end

  def set_election
    @election = Election.find(params[:election_id])
  end
end
