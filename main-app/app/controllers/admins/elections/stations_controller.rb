class Admins::Elections::StationsController < Admins::ApplicationController
  before_action :set_election
  before_action :set_local_area, only: :filtered

  respond_to :js

  def index
    @districts = Station.parent_levels(@election.election_group_id)
  end

  def filtered
    @stations = @local_area.stations.where(election_group_id: @election.election_group_id).sort_by(&:name)

    render json: @stations.to_json
  end

  private

  def set_election
    @election = Election.find(params[:election_id])
  end

  def set_local_area
    @local_area = LocalArea.find(params[:local_area_id])
  end
end
