class Admins::Elections::ElectoralDistrictsController < Admins::ApplicationController
  before_action :set_election, only: %i[index destroy]
  before_action :set_local_area, :set_district, only: :destroy

  def index
    @district_local_areas = LocalArea.by_election(@election).order(:region_id)
  end

  def destroy
    @election.electoral_districts.delete(@district)
    redirect_to admins_election_electoral_districts_path(@election)
  end

  private

  def set_election
    @election = Election.find(params[:election_id])
  end

  def set_local_area
    @local_area = LocalArea.find(params[:local_area_id])
  end

  def set_district
    @district =
      @local_area.electoral_district ||
      @local_area.region.electoral_district ||
      @local_area.country.electoral_district
  end
end
