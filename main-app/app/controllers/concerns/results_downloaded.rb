module ResultsDownloaded
  extend ActiveSupport::Concern

  def base_responders_with_xlsx(filename)
    respond_to do |format|
      format.html
      format.js
      format.xlsx do
        set_elections_info
        response.headers[
          'Content-Disposition'
        ] = "attachment; filename=#{filename}.xlsx"
      end
    end
  end

  protected

  def set_elections_info
    if @elections.present?
      @elections_info = @elections.map { |election| election_info(election) }
    else
      @election_info  = election_info(@election, @station&.id, @district&.id)
    end
  end

  def election_info(election, station_id = nil, district_id = nil)
    {
      id: election.id,
      name: election.name,
      candidates_by_district: candidates_info(election.candidates, district_id),
      closing_protocols: election_closing_protocols(election, station_id, district_id)
    }
  end

  def election_closing_protocols(election, station_id = nil, district_id = nil)
    correct_protocols = if district_id.present?
                          election.closing_protocols.includes(candidates_closing_protocols: [:candidate])
                            .where(
                              candidates_closing_protocols: {
                                candidates: { id: election.candidates.where(electoral_district_id: district_id).ids }
                              }
                            )
                        else
                          election.closing_protocols
                        end.correct

    return correct_protocols.where(station_id: station_id).take(1) if station_id.present?

    correct_protocols.group_by(&:station_id).map { |_station_id, protocols| protocols.first }.sort_by(&:created_at)
  end

  def candidates_info(candidates, district_id = nil)
    candidates = district_id.present? ? candidates.where(electoral_district_id: district_id) : candidates

    candidates.group_by do |c|
      if c.electoral_district.present?
        district = c.electoral_district.scopeable
        district.respond_to?(:region) ? "#{district.region.name} - #{district.name}" : district.name
      else
        I18n.t('pages.results.elections.index.no_district')
      end
    end
  end
end
