class SmsGateway::Request < ApplicationService
  BASE_URL = 'https://api.mobizon.kz/service/'.freeze
  ENDPOINTS = {
    send_sms: 'message/sendsmsmessage',
    get_balance: 'user/getownbalance'
  }.freeze

  def call(action, params = {})
    url = "#{BASE_URL}/#{ENDPOINTS[action]}"

    RestClient.post(url, base_params.merge(params))
  end

  private

  def base_params
    {
      apiKey: ENV.fetch('MOBIZON_KEY'),
      output: 'json',
    }
  end
end
