class ClosingProtocols::CollectIdenticalParams < ApplicationService
  include ClosingProtocols::Comparable

  def initialize(protocol:, status: nil, conflict: nil)
    @protocol = protocol
    @status = status
    @conflict = conflict
  end

  def call
    params = comparable_params
    params[:candidates_closing_protocols] = candidates_params
    params[:status] = @status if @status
    params[:conflict] = @conflict unless @conflict.nil?

    params
  end

  private

  def candidates_params
    @protocol.candidates_closing_protocols.reduce({}) do |h, cp|
      h[cp.candidate_id] = cp.votes
      h
    end
  end
end
