module ClosingProtocols
  module Comparable
    private

    def comparable_params
      comparable = ClosingProtocol::COMPARABLE_BASE_ATTRIBUTES + ClosingProtocol::COMPARABLE_ATTRIBUTES

      @protocol.attributes.symbolize_keys.select do |k, _v|
        comparable.include?(k)
      end
    end
  end
end
