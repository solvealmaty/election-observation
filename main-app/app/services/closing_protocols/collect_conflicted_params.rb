class ClosingProtocols::CollectConflictedParams < ApplicationService
  include ClosingProtocols::Comparable

  def initialize(protocol:)
    @protocol = protocol
  end

  def call
    comparable_params.merge(candidates_params)
  end

  private

  def candidates_params
    @protocol.candidates_closing_protocols.reduce({}) do |h, cp|
      h[:candidate_id] ||= []
      h[:votes] ||= []

      h[:candidate_id] << cp.candidate_id
      h[:votes] << cp.votes

      h
    end
  end
end
