class ClosingProtocols::BuildConflictedQuery < ApplicationService
  def call
    "(#{protocol_query}) AND (#{candidates_query})"
  end

  private

  def protocol_query
    ClosingProtocol::COMPARABLE_ATTRIBUTES.map do |attr|
      "#{attr} != :#{attr}"
    end.join(' OR ')
  end

  def candidates_query
    [
      'candidates_closing_protocols.candidate_id IN (:candidate_id)',
      'candidates_closing_protocols.votes <> ANY(ARRAY[:votes])'
    ].join(' AND ')
  end
end
