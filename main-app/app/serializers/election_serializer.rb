class ElectionSerializer < ActiveModel::Serializer
  attributes :id, :name, :candidates

  def candidates
    object.candidates.map do |c|
      CandidateSerializer.new(c).serializable_hash
    end
  end
end
