class CandidateSerializer < ActiveModel::Serializer
  attributes :id, :name, :info
end
