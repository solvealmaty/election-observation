class Mobile::UserSerializer < ActiveModel::Serializer
  attributes :email, :full_name
end
