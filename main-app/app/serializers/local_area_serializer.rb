class LocalAreaSerializer < ActiveModel::Serializer
  attributes :id, :name, :stations

  def stations
    object.stations.map { |s| StationSerializer.new(s).serializable_hash }
  end
end
