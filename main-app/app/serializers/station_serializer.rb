class StationSerializer < ActiveModel::Serializer
  attributes :id, :name, :address
end
