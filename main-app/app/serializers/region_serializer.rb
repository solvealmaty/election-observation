class RegionSerializer < ActiveModel::Serializer
  attributes :id, :name, :local_areas, :stations

  def local_areas
    object.local_areas.includes([:stations]).order(:name).map do |area|
      LocalAreaSerializer.new(area).serializable_hash
    end
  end

  def stations
    object.local_areas.includes([:stations]).order(:name).first.stations.map do |station|
      StationSerializer.new(station).serializable_hash
    end
  end
end
