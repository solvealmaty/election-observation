class ElectionGroup < ApplicationRecord
  enum status: %i[draft ready published completed]

  has_many :elections, dependent: :destroy
  has_many :stations, dependent: :destroy

  has_many :election_groups_organizations, dependent: :destroy
  has_many :public_organizations, through: :election_groups_organizations

  validates :name, presence: true
end
