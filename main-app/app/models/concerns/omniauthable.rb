module Omniauthable
  extend ActiveSupport::Concern

  module ClassMethods
    def from_omniauth(auth)
      Identity.where(provider: auth.provider, uid: auth.uid).first_or_create do |identity|
        if auth.info.email.present?
          email = auth.info.email
          user = User.find_by(email: email)
        else
          email = "#{auth.uid}@#{auth.provider.downcase}"
          user = User.find_by(email: email)
        end

        if user.present?
          identity.user = user
        else
          new_user = identity.build_user(email: email, password: Devise.friendly_token[0,20])
          new_user
        end
      end.user
    end
  end
end
