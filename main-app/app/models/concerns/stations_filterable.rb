module StationsFilterable
  extend ActiveSupport::Concern

  included do
    scope :by_election, ->(attrs) {
      includes(:closing_protocols)
        .where(
          closing_protocols: {
            election_id: attrs[:election_id]
          }.merge(attrs[:protocol_status] ? {status: attrs[:protocol_status]} : {})
        )
    }

    scope :by_region, ->(attrs) {
      includes({local_area: :region}, :closing_protocols)
        .distinct
        .by_election(attrs)
        .where(
          local_areas: {regions: {id: attrs[:scopeable_id]}}
        )
    }
    scope :by_local_area, ->(attrs) {
      includes(:local_area, :closing_protocols)
        .distinct
        .by_election(attrs)
        .where(
          local_areas: {id: attrs[:scopeable_id]}
        )
    }

    scope :by_district_protocols, -> (election_id, district) {
      case district.scopeable_type
      when 'Country'
        includes(:closing_protocols, local_area: {region: [:country]})
          .distinct
          .where(
            closing_protocols: {election_id: election_id},
            local_areas: {regions: {
              countries: {id: district.scopeable_id}}
            }
          )
      when 'Region'
        includes(:closing_protocols, local_area: [:region])
          .distinct
          .where(
            closing_protocols: {election_id: election_id},
            local_areas: {region_id: district.scopeable_id}
          )
      when 'LocalArea'
        includes(:closing_protocols, :local_area)
          .distinct
          .where(
            closing_protocols: {election_id: election_id},
            local_area_id: district.scopeable_id
          )
      end
    }

    scope :by_public_orgs, -> (public_org_ids = []) {
      if public_org_ids.present?
        includes(observers_stations: [:user])
          .where(observers_stations: { users: { public_organization_id: public_org_ids } })
      else
        self
      end
    }

    scope :by_org_type, -> (org_type, election_ids) {
      case org_type
      when 'affiliated'
        self.with_affiliated_orgs_observers(election_ids)
      when 'unaffiliated'
        self.without_affiliated_orgs_observers(election_ids)
      else
        self
      end
    }

    scope :by_observers_type, -> (observer_type, public_org_ids) {
      case observer_type
      when 'no_certificate'
        self.without_certified_observers(public_org_ids)
      when 'certified'
        self.with_certified_observers(public_org_ids)
      when 'uncertified'
        self.with_unapproved_certificate_observers(public_org_ids)
      when 'any'
        self.includes(observers_stations: [:station])
          .where.not(observers_stations: { station_id: nil })
      else
        self
      end
    }

    scope :with_certified_observers, -> (public_org_ids = []) {
      if public_org_ids.present?
        includes(observers_stations: [:user])
          .where(observers_stations:
                   {
                     users: { public_organization_id: public_org_ids },
                     certification: 'approved'
                   }
          )
      else
        includes(:observers_stations).where(observers_stations: { certification: 'approved' })
      end
    }

    scope :without_observers, -> { includes(:observers_stations).where(observers_stations: { station_id: nil }) }

    scope :without_certified_observers, -> (public_org_ids = []) {
      if public_org_ids.present?
        includes(observers_stations: [:user])
          .where(observers_stations:
                   {
                     users: { public_organization_id: public_org_ids },
                     certification: 'unapproved',
                     certificate: nil
                   }
          )
          .where.not(id: self.with_certified_observers.ids)
      else
        includes(:observers_stations)
          .where(observers_stations: { certification: 'unapproved', certificate: nil })
          .where.not(id: self.with_certified_observers.ids)
      end
    }

    scope :with_unapproved_certificate_observers, -> (public_org_ids = []) {
      if public_org_ids.present?
        includes(observers_stations: [:user])
          .where(observers_stations: {
            users: { public_organization_id: public_org_ids },
            certification: ['unapproved', 'rejected'] })
          .where.not(observers_stations: { certificate: nil })
          .where.not(id: self.with_certified_observers.ids)
      else
        includes(:observers_stations)
          .where(observers_stations: { certification: ['unapproved', 'rejected'] })
          .where.not(observers_stations: { certificate: nil })
          .where.not(id: self.with_certified_observers.ids)
      end
    }

    scope :with_affiliated_orgs_observers, -> (election_ids) {
      includes(observers_stations: { user: { public_organization: [:candidates] } })
        .where(candidates: { election_id: election_ids })
    }

    scope :without_affiliated_orgs_observers, -> (election_ids) {
      where.not(id: Station.with_affiliated_orgs_observers(election_ids).ids).joins(:observers_stations).distinct
    }
  end
end
