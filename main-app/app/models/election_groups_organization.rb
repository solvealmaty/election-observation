class ElectionGroupsOrganization < ApplicationRecord
  belongs_to :election_group
  belongs_to :public_organization
end
