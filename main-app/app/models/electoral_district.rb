class ElectoralDistrict < ApplicationRecord
  DISTRICT_LEVELS = {'Country' => 1, 'Region' => 2, 'LocalArea' => 3}.freeze

  belongs_to :scopeable, polymorphic: true

  has_and_belongs_to_many :elections

  has_many :candidates, dependent: :destroy

  before_validation :set_level, on: :create

  validates :level, inclusion: { in: DISTRICT_LEVELS.values }

  def local_area_ids
    case scopeable_type
    when 'Country'
      scopeable.regions.flat_map(&:local_area_ids)
    when 'Region'
      scopeable.local_area_ids
    when 'LocalArea'
      [scopeable_id]
    end
  end

  private

  def set_level
    self.level = DISTRICT_LEVELS[scopeable_type]
  end
end
