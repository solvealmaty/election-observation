class Region < ApplicationRecord
  belongs_to :country

  has_one :electoral_district, as: :scopeable

  has_many :local_areas, dependent: :destroy

  validates :name, presence: true

  def full_name
    "#{country.name} - #{name}"
  end
end
