class Candidate < ApplicationRecord
  belongs_to :election, counter_cache: true
  belongs_to :electoral_district, optional: true
  belongs_to :public_organization, optional: true

  has_many :candidates_closing_protocols, dependent: :destroy
  has_many :closing_protocols, through: :candidates_closing_protocols, dependent: :destroy

  accepts_nested_attributes_for :electoral_district

  validates :name, presence: true
end
