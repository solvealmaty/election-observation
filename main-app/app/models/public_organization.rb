class PublicOrganization < ApplicationRecord
  enum state: { draft: 0, approved: 1 }

  belongs_to :country

  has_many :candidates
  has_many :users, dependent: :destroy
  has_many :election_groups_organizations, dependent: :destroy
  has_many :election_groups, through: :election_groups_organizations

  accepts_nested_attributes_for :users

  validates :name,
            :address,
            :iin_bin,
            :social_network,
            :country,
            presence: true
end
