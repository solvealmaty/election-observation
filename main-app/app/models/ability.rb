class Ability
  include CanCan::Ability

  def initialize(user)
    return unless user

    can :manage, User, id: user.id
    send(user.role)
  end

  private

  def admin
    can :manage, :all
  end

  def manager
  end

  def regular
  end
end
