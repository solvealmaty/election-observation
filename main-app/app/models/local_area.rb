class LocalArea < ApplicationRecord
  belongs_to :region

  has_one :electoral_district, as: :scopeable

  has_many :stations, dependent: :destroy

  validates :name, presence: true

  delegate_missing_to :region

  scope :by_election, -> (election) {
    includes(region: :country, stations: :election_group)
      .where(stations: {
        id: election.stations.ids,
        election_group_id: election.election_group_id
      })
  }

  def full_name
    "#{region.country.name} - #{region.name} - #{name}"
  end
end
