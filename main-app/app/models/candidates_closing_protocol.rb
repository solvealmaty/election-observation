class CandidatesClosingProtocol < ApplicationRecord
  belongs_to :closing_protocol
  belongs_to :candidate

  validates :votes, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  scope :with_station_info, -> {
    includes(
      :candidate,
      closing_protocol: [
        :election,
        station: [
          local_area: [
            :region
          ]
        ]
      ]
    )
  }
end
