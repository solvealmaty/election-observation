class Country < ApplicationRecord
  alias_attribute :full_name, :name

  has_one :electoral_district, as: :scopeable

  has_many :regions, dependent: :destroy
  has_many :public_organizations, dependent: :destroy

  validates :name, presence: true, uniqueness: true
end
