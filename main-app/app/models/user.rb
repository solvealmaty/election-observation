class User < ApplicationRecord
  include Omniauthable

  enum role: {
    observer: 0,
    coordinator: 1,
    representative: 2
  }

  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :omniauthable,
         :validatable,
         authentication_keys: [:email],
         omniauth_providers: %i[facebook google_oauth2 vkontakte odnoklassniki twitter]

  belongs_to :public_organization, optional: true

  has_many :identities, dependent: :destroy

  has_many :closing_protocols, dependent: :destroy
  has_many :observers_stations, dependent: :destroy
  has_many :stations, through: :observers_stations
  has_many :accreditions, class_name: 'User', foreign_key: 'accreditor_id'

  before_validation :ensure_access_token!, :ensure_identifier!

  validates :access_token, presence: true, uniqueness: true
  validates :email, uniqueness: true, allow_blank: true
  validates :phone, uniqueness: true, allow_blank: true
  validate :account_field_presence
  validate :representatives_count_restriction

  alias authenticate valid_password?

  scope :group_observers, -> (election_group_id) {
    self.observer.includes(:observers_stations)
       .where(observers_stations: { election_group_id: election_group_id })
  }
  scope :certified_observers, -> {
    where(observers_stations: { certification: 'approved' })
      .where.not(observers_stations: { station_id: nil })
  }

  def self.responsible_roles
    roles.keys[1..2]
  end

  def full_name
    return unless first_name.present? && last_name.present?

    "#{first_name} #{last_name}"
  end

  def send_confirmation_notification?
    false
  end

  def station
    stations.last
  end

  private

  def account_field_presence
    return if email.present? || phone.present? || username.present?

    errors.add(:account_field, I18n.t('errors.user.account_field.blank'))
  end

  def ensure_identifier!
    self.identifier ||= generate_identifier
  end

  def ensure_access_token!
    self.access_token ||= generate_access_token
  end

  def generate_identifier
    loop do
      seq = SecureRandom.random_number.to_s[2..9]
      break seq unless User.find_by(identifier: seq)
    end
  end

  def generate_access_token
    loop do
      token = Devise.friendly_token(32)
      break token unless User.find_by(access_token: token)
    end
  end

  def representatives_count_restriction
    return if public_organization.blank?
    return if public_organization.users.representative.size < 5

    errors.add(:role, I18n.t('errors.public_organization.representatives_count'))
  end
end
