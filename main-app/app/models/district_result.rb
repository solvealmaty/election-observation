class DistrictResult
  def initialize(district, election)
    @district = district
    @election = election
    @correct_status_id = ClosingProtocol.statuses['correct']

    @stations_candidates_votes  = ActiveRecord::Base.connection.execute(stations_candidates_votes_query).to_a
    @general_info = ActiveRecord::Base.connection.execute(general_info_query).to_a
  end

  def total_voters_count
    count_results('total_voters')
  end

  def active_voters_count
    count_results('active_voters')
  end

  def total_invalid_bulletins
    count_results('invalid_bulletins_total')
  end

  def voted_count
    general_info.reduce(0) { |sum, candidate_result| sum + candidate_result['voted'] }
  end

  def candidates_votes
    stations_candidates_votes.reduce({}) do |result, station_candidate_result|
      name = station_candidate_result['name']

      if result.key?(name)
        result[name] += station_candidate_result['voted']
      else
        result[name] = station_candidate_result['voted']
      end

      result
    end
  end

  private

  attr_reader :district, :election, :correct_status_id, :stations_candidates_votes, :general_info

  def count_results(key)
    general_info.group_by { |info| info["station_id"] }.reduce(0) do |sum, (_station_id, results)|
      sum + results.first[key]
    end
  end

  def collect_stations_ids
    stations_ids = Station.by_district_protocols(@election.id, @district).ids
    stations_ids.empty? ? 'NULL' : stations_ids.join(',')
  end

  def base_query
    <<-SQL
      SELECT stations.id as station_id,
            candidates.name,
            candidates_closing_protocols.votes,
            closing_protocols.total_voters,
            closing_protocols.active_voters,
            closing_protocols.invalid_bulletins
      FROM closing_protocols
          INNER JOIN stations ON closing_protocols.station_id = stations.id 
            AND closing_protocols.status = #{correct_status_id}
            AND closing_protocols.election_id = #{election.id}
            AND stations.id IN (#{collect_stations_ids})
          INNER JOIN candidates_closing_protocols
                      ON closing_protocols.id = candidates_closing_protocols.closing_protocol_id
          INNER JOIN candidates ON candidates_closing_protocols.candidate_id = candidates.id
          INNER JOIN electoral_districts ON electoral_districts.id = candidates.electoral_district_id 
            AND electoral_districts.id = #{district.id}
    SQL
  end

  def general_info_query
    <<-SQL
      SELECT station_id,
             name,
             MAX(total_voters) as total_voters,
             MAX(active_voters) as active_voters,
             MAX(votes) as voted,
             MAX(invalid_bulletins) as invalid_bulletins_total
      FROM (#{base_query})  AS base_query
      GROUP BY station_id,
               name;
    SQL
  end

  def stations_candidates_votes_query
    <<-SQL
      SELECT station_id,
             name,
             MAX(votes) as voted
      FROM (#{base_query})  AS base_query
      GROUP BY station_id,
               name;
    SQL
  end
end
