class Station < ApplicationRecord
  include ActiveModel::Serialization
  include StationsFilterable

  COORDINATES_FORMAT = /\A\d+\.\d+\,\ *\d+\.\d+\z/.freeze

  belongs_to :election_group
  belongs_to :local_area

  has_many :closing_protocols, dependent: :destroy
  has_many :observers_stations, dependent: :destroy
  has_many :observers, through: :observers_stations, source: :user

  has_and_belongs_to_many :elections

  validates :name, presence: true, uniqueness: {scope: :local_area_id}

  before_save :check_coordinates

  delegate_missing_to :local_area

  class << self
    def parent_levels(election_group_id)
      query = <<-SQL
      SELECT
        c.id AS country_id, c.name AS country_name,
        r.id AS region_id, r.name AS region_name,
        l.id AS local_area_id, l.name AS local_area_name
      FROM countries c
      LEFT JOIN regions r ON r.country_id = c.id
      LEFT JOIN local_areas l ON l.region_id = r.id
      LEFT JOIN stations s ON s.local_area_id = l.id
      WHERE s.election_group_id = ?
      GROUP BY c.id, c.name, r.id, r.name, l.id, l.name
      ORDER BY c.name, r.name, l.name;
      SQL

      self.connection.execute(
        sanitize_sql([query, election_group_id])
      ).to_a
    end
  end

  def full_address
    "#{region.name}, #{local_area.name}, #{address}"
  end

  private

  def check_coordinates
    return if self.coordinates.nil?
    return if self.coordinates.match?(COORDINATES_FORMAT)

    self.coordinates = nil
  end
end
