class ClosingProtocol < ApplicationRecord
  mount_uploaders :images, ClosingProtocolUploader

  BULLETINS_THRESHOLD_PERCENT = 101
  MAX_IMAGES_AMOUNT = 5
  PAGINATE_COUNT = 50
  COMPARABLE_BASE_ATTRIBUTES = %i[
    election_id
    station_id
  ]
  COMPARABLE_ATTRIBUTES = %i[
    total_voters
    absentee_certificate_voters
    outdoors_voters
    active_voters
    total_bulletins
    voters_bulletins
    invalid_bulletins
    canceled_bulletins
  ]

  paginates_per PAGINATE_COUNT

  enum status: {
    unverified: 0,
    incorrect: 1,
    correct: 2
  }

  belongs_to :station
  belongs_to :election
  belongs_to :user

  has_many :candidates_closing_protocols
  has_many :candidates, through: :candidates_closing_protocols, dependent: :destroy

  accepts_nested_attributes_for :candidates_closing_protocols

  validates :total_voters,
            :absentee_certificate_voters,
            :outdoors_voters,
            :active_voters,
            :total_bulletins,
            :voters_bulletins,
            :invalid_bulletins,
            :canceled_bulletins,
            presence: true,
            numericality: {only_integer: true, greater_than_or_equal_to: 0}

  validates_presence_of :candidates_closing_protocols

  validate :images_amount, on: :create

  scope :live, -> { where(deleted: false) }
  scope :by_public_org, ->(org_id) {
    return unless org_id
    joins(:user)
      .where(users: {public_organization_id: org_id})
      .live
  }
  scope :by_region, ->(region_id) {
    return unless region_id
    includes(station: [local_area: :region])
      .where(stations: {local_areas: {region_id: region_id}})
      .live
  }
  scope :by_local_area, ->(area_id) {
    return unless area_id
    includes(station: :local_area)
      .where(stations: {local_area_id: area_id})
      .live
  }
  scope :for_batch_update, ->(**params) {
    return [] if params.empty?
    includes(:candidates_closing_protocols, :election, :user, station: [local_area: :region])
      .where(params)
      .in_batches
  }
  scope :list, -> {
    live.includes(
      :election,
      station: [local_area: :region],
      user: [:public_organization]
    )
  }

  class << self
    def base_filter(params)
      query = params.reject { |_k, v| v.blank? }

      includes(:election, station: [local_area: :region], user: [:public_organization])
        .by_public_org(query.delete(:public_organization_id))
        .by_region(query.delete(:region_id))
        .by_local_area(query.delete(:local_area_id))
        .where(query)
        .live
    end

    def identical(params)
      query_params = params
      candidates_params = query_params.delete(:candidates_closing_protocols)

      includes(:candidates_closing_protocols)
        .joins(:election, :station)
        .distinct
        .where(params)
        .live
        .select { |p|
          p.candidates_closing_protocols.pluck(:candidate_id, :votes) == candidates_params.to_a
        }
    end

    def conflicted(params)
      query = ::ClosingProtocols::BuildConflictedQuery.call

      joins(:candidates_closing_protocols)
        .distinct
        .where(station_id: params[:station_id], election_id: params[:election_id], status: :unverified)
        .where(Arel.sql(query), params)
        .live
    end

    def search(params)
      includes(:election, station: [local_area: :region])
        .joins(:user)
        .where(users: {identifier: params[:identifier]})
        .live
    end
  end

  private

  def images_amount
    if images.size > MAX_IMAGES_AMOUNT
      errors.add(:images, I18n.t('errors.closing_protocol.images_amount', max_images_amount: MAX_IMAGES_AMOUNT))
    end
  end
end
