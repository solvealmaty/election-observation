class ObserversStation < ApplicationRecord
  enum certification: { unapproved: 0, approved: 1, rejected: 2 }

  mount_uploader :certificate, CertificateUploader

  belongs_to :station, optional: true
  belongs_to :user
  belongs_to :accreditor, class_name: 'User', foreign_key: 'accreditor_id', optional: true

  before_validation :set_election_group_id, on: :create

  validates_uniqueness_of :user, scope: :election_group_id, message: I18n.t('messages.observers_stations.user_exists')

  accepts_nested_attributes_for :user

  private

  def set_election_group_id
    self.election_group_id = station.election_group_id
  end
end
