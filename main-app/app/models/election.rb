class Election < ApplicationRecord
  enum status: %i[draft ready]

  belongs_to :election_group

  has_and_belongs_to_many :electoral_districts
  has_and_belongs_to_many :stations

  has_many :candidates, dependent: :destroy
  has_many :closing_protocols, dependent: :destroy

  validates :name, :date, presence: true

  def pinned!
    self.update!(pinned: true)
  end

  def candidates_exist?
    candidates.size > 0
  end

  def district_by_station(station)
    self.electoral_districts.find do |district|
      if district.level == 1
        true
      elsif district.level == 2
        district.scopeable.local_areas.flat_map(&:stations).include?(station)
      elsif district.level == 3
        district.scopeable.stations.include?(station)
      end
    end
  end
end
