class Results::Districts::PaginateStations < ApplicationInteractor
  uses_via_context :general_details, :stations_with_correct_protocols, :page

  def call
    general_details.merge!(
      paginated_stations: Kaminari.paginate_array(stations_with_correct_protocols).page(page)
    )
  end
end
