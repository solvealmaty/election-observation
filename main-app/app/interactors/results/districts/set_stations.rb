class Results::Districts::SetStations < ApplicationInteractor
  uses_via_context :general_details, :district, :election

  def call
    stations_with_correct_protocols = stations_with_protocols.where(closing_protocols: {status: 'correct'})

    context.stations_with_any_protocols = district_stations.where(
      closing_protocols: { election_id: election.id }
    ).distinct
    context.stations_with_correct_protocols = stations_with_correct_protocols
    context.general_details.merge!(
      district_stations_count: district_stations.count,
      stations_with_correct_protocols_count: stations_with_correct_protocols.count
    )
  end

  private

  def district_stations
    @district_stations ||= election.stations.includes(:closing_protocols, local_area: [:region])
                             .where(local_area_id: district.local_area_ids).distinct
  end

  def stations_with_protocols
    @stations_with_protocols ||= if general_details[:local_area].present?
                                   district_stations.where(
                                     local_area: general_details[:local_area],
                                     closing_protocols: { election_id: election.id }
                                   ).distinct
                                 elsif general_details[:region].present?
                                   district_stations.where(
                                     local_areas: { region: general_details[:region] },
                                     closing_protocols: { election_id: election.id }
                                   ).distinct
                                 else
                                   district_stations.where(closing_protocols: { election_id: election.id }).distinct
                                 end
  end
end
