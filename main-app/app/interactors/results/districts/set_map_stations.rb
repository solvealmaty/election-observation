class Results::Districts::SetMapStations < ApplicationInteractor
  uses_via_context :stations_with_correct_protocols, :stations_with_any_protocols

  def call
    context.red_stations  = collect_stations_info(stations_with_correct_protocols)
    context.gray_stations = collect_stations_info(stations_with_any_protocols - stations_with_correct_protocols)
  end

  private

  def collect_stations_info(stations)
    stations.map do |station|
      station.attributes.deep_symbolize_keys
        .slice(:id, :name, :address, :coordinates)
        .merge(full_address: station.full_address)
    end
  end
end
