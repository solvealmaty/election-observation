class Results::Districts::New < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::Results::Districts::SetCurrentRegion,
           ::Results::Districts::SetStations,
           ::Results::Districts::PaginateStations,
           ::Results::Districts::SetRegions,
           ::Results::Districts::SetMapStations,
           ::Results::Districts::CollectResults
end
