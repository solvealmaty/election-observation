class ::Results::Districts::SetCurrentRegion < ApplicationInteractor
  uses_via_context :region_id, :local_area_id

  def call
    context.general_details = {}
    context.general_details[:region]     = region_id.to_i > 0 ? Region.find(region_id) : nil
    context.general_details[:local_area] = local_area_id.to_i > 0 ? LocalArea.find(local_area_id) : nil
  end
end
