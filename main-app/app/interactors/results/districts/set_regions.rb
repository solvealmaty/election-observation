class Results::Districts::SetRegions < ApplicationInteractor
  uses_via_context :district, :stations_with_any_protocols, :general_details

  def call
    local_areas_all = stations_with_any_protocols.map(&:local_area).uniq.sort_by(&:name)

    case district.scopeable_type
    when 'Country'
      regions = local_areas_all.map(&:region).uniq.sort_by(&:name)
      region = general_details[:region] || regions.first
      general_details.merge!(
        regions: regions,
        local_areas: local_areas_all.select { |local_area| local_area.region == region }.sort_by(&:name)
      )
    when 'Region'
      general_details.merge!(local_areas: local_areas_all)
    end
  end
end
