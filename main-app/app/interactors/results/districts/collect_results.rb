class Results::Districts::CollectResults < ApplicationInteractor
  uses_via_context :district, :election

  def call
    context.district_results = DistrictResult.new(
      district,
      election
    )
  end
end
