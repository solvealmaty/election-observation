class ClosingProtocols::UpdateStatus < ApplicationInteractor
  uses_via_context :protocol, :status

  def call
    return unless status

    protocol.update!(status: status)
  end
end
