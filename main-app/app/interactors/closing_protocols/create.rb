class ClosingProtocols::Create < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::ClosingProtocols::Validate::Candidates,
           ::ClosingProtocols::Save,
           ::ClosingProtocols::Validate::Correct,
           ::ClosingProtocols::Identical::Find,
           ::ClosingProtocols::UpdateStatus,
           ::ClosingProtocols::Conflicted::Organize
end
