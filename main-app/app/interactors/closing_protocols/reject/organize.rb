class ClosingProtocols::Reject::Organize < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::ClosingProtocols::Reject::Update,
           ::ClosingProtocols::Identical::Organize
end
