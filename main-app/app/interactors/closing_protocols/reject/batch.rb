class ClosingProtocols::Reject::Batch < ApplicationInteractor
  uses_via_context :protocol

  def call
    ClosingProtocol
      .joins(:user)
      .where(user: protocol.user)
      .update_all(status: :incorrect)
  end
end
