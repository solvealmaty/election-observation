class ClosingProtocols::Reject::Update < ApplicationInteractor
  uses_via_context :protocol

  def call
    ::ClosingProtocols::UpdateStatus.call(protocol: protocol, status: :incorrect)
    context.status = :incorrect
  end
end
