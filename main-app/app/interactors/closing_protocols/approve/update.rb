class ClosingProtocols::Approve::Update < ApplicationInteractor
  uses_via_context :protocol

  def call
    ::ClosingProtocols::UpdateStatus.call(protocol: protocol, status: :correct)
    context.status = :correct
  end
end
