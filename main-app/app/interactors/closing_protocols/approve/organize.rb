class ClosingProtocols::Approve::Organize < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::ClosingProtocols::Approve::Update,
           ::ClosingProtocols::Identical::Organize
end
