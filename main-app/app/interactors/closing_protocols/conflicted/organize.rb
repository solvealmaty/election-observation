class ClosingProtocols::Conflicted::Organize < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::ClosingProtocols::Conflicted::Find,
           ::ClosingProtocols::Conflicted::Update
end
