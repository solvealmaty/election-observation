class ClosingProtocols::Conflicted::Find < ApplicationInteractor
  uses_via_context :protocol

  def call
    unless protocol.unverified?
      context.conflicted_protocols = []
      context.conflict = false
      return
    end

    conflicted_params = ::ClosingProtocols::CollectConflictedParams.call(protocol: protocol)
    conflicted = ::ClosingProtocol.conflicted(conflicted_params)
    is_empty = conflicted.empty?

    context.conflicted_protocols = is_empty ? [] : conflicted.to_a << protocol
    context.conflict = !is_empty
  end
end

