class ClosingProtocols::Conflicted::Update < ApplicationInteractor
  uses_via_context :protocol, :conflicted_protocols, :conflict

  def call
    return unless protocol.unverified?
    return if conflicted_protocols.empty?
    return if conflicted_protocols.pluck(:conflict).all?(conflict)

    ClosingProtocol
      .where(id: conflicted_protocols.pluck(:id))
      .update_all(conflict: conflict)
  end
end
