class ClosingProtocols::Validate::Candidates < ApplicationInteractor
  uses_via_context :params

  before do
    @election = Election.find(params[:election_id])
    @station = Station.find(params[:station_id])
    @candidate_ids = params[:candidates_closing_protocols_attributes].map { |cp| cp[:candidate_id].to_i }.sort
  end

  def call
    return if valid_candidates?

    context.fail!(message: I18n.t('errors.closing_protocol.invalid_candidates'))
  end

  private

  def valid_candidates?
    return false if district_candidates.blank?

    district_candidates.pluck(:id).sort == @candidate_ids
  end

  def district_candidates
    @district_candidates ||= district.present? ? district.candidates.where(election_id: @election.id) : []
  end

  def district
    @district ||= @election.district_by_station(@station)
  end
end
