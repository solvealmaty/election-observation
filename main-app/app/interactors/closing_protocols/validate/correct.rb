class ClosingProtocols::Validate::Correct < ApplicationInteractor
  uses_via_context :protocol

  def call
    context.status = correct_data? ? :unverified : :incorrect
  end

  private

  def correct_data?
    valid_bulletins_amount? && valid_voters_amount? && valid_total_bulletins?
  end

  def valid_bulletins_amount?
    protocol.total_bulletins == protocol.voters_bulletins + protocol.canceled_bulletins
  end

  def valid_voters_amount?
    total_votes = protocol.candidates_closing_protocols.reduce(0) do |sum, cp|
      sum += cp.votes
      sum
    end

    protocol.active_voters == protocol.invalid_bulletins + total_votes
  end

  def valid_total_bulletins?
    limit = (protocol.total_voters * ClosingProtocol::BULLETINS_THRESHOLD_PERCENT) / 100
    protocol.total_bulletins <= limit
  end
end
