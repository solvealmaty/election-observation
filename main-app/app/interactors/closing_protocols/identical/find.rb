class ClosingProtocols::Identical::Find < ApplicationInteractor
  uses_via_context :protocol, :status

  before do
    identical_params = ::ClosingProtocols::CollectIdenticalParams.call(protocol: protocol)
    @identical = ClosingProtocol.identical(identical_params)
  end

  def call
    context.identical_protocols = identical
    context.status = set_status
  end

  private

  attr_reader :identical

  def set_status
    return status if status != :unverified
    protocol.unverified? ? identical.first&.status : protocol.status
  end
end
