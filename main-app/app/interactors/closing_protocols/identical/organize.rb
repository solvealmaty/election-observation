class ClosingProtocols::Identical::Organize < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::ClosingProtocols::Identical::Find,
           ::ClosingProtocols::Identical::Update
end
