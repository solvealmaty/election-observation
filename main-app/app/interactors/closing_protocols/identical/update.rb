class ClosingProtocols::Identical::Update < ApplicationInteractor
  uses_via_context :identical_protocols, :status

  def call
    return if identical_protocols.empty?

    ClosingProtocol
      .where(id: identical_protocols.pluck(:id))
      .update_all(status: status, conflict: false)
  end
end
