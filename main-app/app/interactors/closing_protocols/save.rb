class ClosingProtocols::Save < ApplicationInteractor
  uses_via_context :params, :current_user

  def call
    protocol = ClosingProtocol.new(protocol_params)
    protocol.save!

    context.protocol = protocol
  end

  private

  def protocol_params
    params.merge(user_id: current_user.id)
  end
end
