class ClosingProtocols::Delete::Batch < ApplicationInteractor
  uses_via_context :protocol_ids

  def call
    ClosingProtocol.where(id: protocol_ids).update_all(deleted: true)
  end
end
