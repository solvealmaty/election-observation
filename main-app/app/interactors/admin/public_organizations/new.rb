class Admin::PublicOrganizations::New < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::Admin::PublicOrganizations::Build,
           ::Admin::PublicOrganizations::AssignRandomPassword,
           ::Admin::PublicOrganizations::SaveAndNotify
end
