class Admin::PublicOrganizations::RemoveCandidates < ApplicationInteractor
  uses_via_context :org

  def call
    org.candidates.delete(org.candidates)
  end
end
