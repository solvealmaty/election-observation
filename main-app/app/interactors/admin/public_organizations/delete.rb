class Admin::PublicOrganizations::Delete < ApplicationInteractor
  uses_via_context :org

  def call
    org.destroy!
  end
end
