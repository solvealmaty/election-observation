class Admin::PublicOrganizations::AssignRandomPassword < ApplicationInteractor
  uses_via_context :user

  def call
    user.password = Devise.friendly_token.first(8)
    # Confirmable module is disabled
    # user.skip_confirmation!
  end
end
