class Admin::PublicOrganizations::Build < ApplicationInteractor
  uses_via_context :org_params

  def call
    context.org = PublicOrganization.new(org_params).tap do |org|
      context.user = org.users.first
    end
  end
end
