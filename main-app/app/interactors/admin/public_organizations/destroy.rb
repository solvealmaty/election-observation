class Admin::PublicOrganizations::Destroy < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::Admin::PublicOrganizations::RemoveCandidates,
           ::Admin::PublicOrganizations::Delete
end
