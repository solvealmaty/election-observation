class Admin::PublicOrganizations::SaveAndNotify < ApplicationInteractor
  uses_via_context :org, :user

  def call
    org.save!
  end

  after do
    user.send_reset_password_instructions
  end
end
