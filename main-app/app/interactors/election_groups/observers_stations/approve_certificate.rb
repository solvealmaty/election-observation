class ElectionGroups::ObserversStations::ApproveCertificate <ApplicationInteractor
  include Transactable

  uses_via_context :observers_station, :accreditor_id

  def call
    if observers_station.certificate.present?
      observers_station.update!(certification: 'approved', accreditor_id: accreditor_id, rejection_reason: nil)
    end
  end
end
