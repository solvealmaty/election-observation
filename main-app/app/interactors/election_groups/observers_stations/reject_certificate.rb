class ElectionGroups::ObserversStations::RejectCertificate <ApplicationInteractor
  include Transactable

  uses_via_context :observers_station, :rejection_reason, :accreditor_id

  def call
    if observers_station.certificate.present?
      observers_station.update!(
        certification: 'rejected',
        rejection_reason: rejection_reason,
        accreditor_id: accreditor_id
      )
    end
  end
end
