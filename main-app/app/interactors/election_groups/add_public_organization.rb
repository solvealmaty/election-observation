class ElectionGroups::AddPublicOrganization < ApplicationInteractor
  uses_via_context :election_group, :public_organization

  def call
    orgs = election_group.public_organizations

    unless orgs.find_by(id: public_organization.id)
      orgs << public_organization
    end
  end
end
