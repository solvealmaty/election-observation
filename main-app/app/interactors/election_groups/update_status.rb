class ElectionGroups::UpdateStatus < ApplicationInteractor
  include Transactable

  uses_via_context :election_group

  def call
    return if election_group.published?

    if election_group.elections.find_by(status: 'draft')
      election_group.draft!
    else
      election_group.ready!
    end
  end
end
