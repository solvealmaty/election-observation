class Stations::CollectStations < ApplicationInteractor
  uses_via_context :election_group,
                   :public_organization_ids,
                   :public_organization_type,
                   :observer_type,
                   :region_id,
                   :local_area_id,
                   :station_ids

  before do
    @group_stations  = election_group.stations
                         .includes(
                           :observers_stations,
                           local_area: [:region],
                           observers: [public_organization: :candidates]
                         )
  end

  def call
    if station_ids.present?
      set_stations
    elsif local_area_id.present?
      set_local_area_stations
    else
      region_stations
    end
  end

  private 

  def set_stations
    context.filtered_stations = @group_stations.where(id: station_ids)
    context.region_stations = []
  end

  def set_local_area_stations
    local_area_stations = @group_stations.where(local_area_id: local_area_id)
    context.filtered_stations = local_area_stations
                                  .distinct
                                  .by_public_orgs(public_organization_ids)
                                  .by_org_type(public_organization_type, election_group.election_ids)
                                  .by_observers_type(observer_type, public_organization_ids)
    context.region_stations = local_area_stations
                                .distinct
                                .includes(:observers_stations)
                                .where.not(id: context.filtered_stations.ids)
  end

  def region_stations
    region_stations = @group_stations.where(local_areas: { region_id: region_id })
    context.filtered_stations = region_stations
                                  .distinct
                                  .by_public_orgs(public_organization_ids)
                                  .by_org_type(public_organization_type, election_group.election_ids)
                                  .by_observers_type(observer_type, public_organization_ids)
    context.region_stations = region_stations
                                .distinct
                                .includes(:observers_stations)
                                .where.not(id: context.filtered_stations.ids)
  end
end
