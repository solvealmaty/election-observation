class Stations::Filter < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::Stations::CollectStations,
           ::Stations::CollectStationsInfo
end
