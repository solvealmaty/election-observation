class Stations::CollectStationsInfo < ApplicationInteractor
  uses_via_context :filtered_stations, :region_stations,
                   :public_organization_ids,
                   :public_organization_type,
                   :observer_type,
                   :observer_id

  def call
    context.stations_info = {
      filteredStationsInfo: filtered_stations_info,
      regionStationsInfo: region_stations_info
    }
  end

  private

  def filtered_stations_info
    orgs = PublicOrganization.where(id: public_organization_ids)

    filtered_stations.map do |station|
      station_info(station).merge(
        orgs: orgs_names(orgs),
        orgType: I18n.t("cabinet.public_organizations.types.#{public_organization_type.present? ? public_organization_type : 'all'}"),
        observerType: I18n.t("cabinet.observers_stations.certification.#{observer_type.present? ? observer_type : 'all'}"),
        isObserverStation: is_observer_station?(station)
      )
    end
  end

  def region_stations_info
    region_stations.map do |station|
      station_orgs = station.observers.map(&:public_organization).uniq.compact
      station_info(station).merge(
        orgs: orgs_names(station_orgs),
        orgType: orgs_type(station_orgs),
        observerType: station_observer_type(station),
        isObserverStation: is_observer_station?(station)
      )
    end
  end

  def station_info(station)
    station.attributes.deep_symbolize_keys.slice(
      :id, :name, :coordinates, :address
    ).merge(fullAddress: station.full_address)
  end

  def orgs_names(orgs)
    return I18n.t('pages.stations.index.orgs_default_option') if orgs.empty?

    orgs.map(&:name).join(', ')
  end

  def orgs_type(orgs)
    affiliated_orgs_count = orgs.reject { |org| org.candidates.empty? }.count
    unaffiliated_orgs_count = orgs.select { |org| org.candidates.empty? }.count

    return I18n.t('pages.stations.index.options_all') if (affiliated_orgs_count > 0 && unaffiliated_orgs_count > 0) || (affiliated_orgs_count + unaffiliated_orgs_count == 0)
    I18n.t("cabinet.public_organizations.types.#{affiliated_orgs_count > 0 ? 'affiliated' : 'unaffiliated'}")
  end

  def station_observer_type(station)
    observers_stations = station.observers_stations

    return I18n.t('pages.stations.index.no_observers') if observers_stations.blank?

    with_certificate_count = observers_stations.select(&:approved?).count
    return I18n.t('cabinet.observers_stations.certification.certified') if with_certificate_count > 0

    with_unapproved_certificate_count = observers_stations.select { |s| s.certificate && !s.approved? }.count
    return I18n.t('cabinet.observers_stations.certification.uncertified') if with_unapproved_certificate_count > 0

    without_certificate_count = observers_stations.select { |s| s.certificate.nil? }.count
    return I18n.t('cabinet.observers_stations.certification.no_certificate') if without_certificate_count > 0
  end

  def is_observer_station?(station)
    station.observers.ids.include?(observer_id)
  end
end
