class Stations::ObserversStations::Update < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::PublicOrganizations::Users::Add,
           ::Stations::ObserversStations::UpdateCertificate,
           ::ElectionGroups::AddPublicOrganization
end
