class Stations::ObserversStations::UpdateCertificate < ApplicationInteractor
  uses_via_context :observers_station, :params

  def call
    observers_station.update!(
      certificate: params[:certificate],
      certification_request: params[:certification_request],
      certification: 'unapproved',
      accreditor_id: nil
    )

    context.message = I18n.t('messages.observers_stations.create.success')

    context.election_group = observers_station.station.election_group
    context.public_organization = observers_station.user.public_organization
  end
end
