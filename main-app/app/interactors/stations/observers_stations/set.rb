class Stations::ObserversStations::Set < ApplicationInteractor
  uses_via_context :station, :user, :params

  def call
    observers_station = find_station || station.observers_stations.new
    observers_station.assign_attributes(observers_station_params)
    observers_station.save!

    context_message(observers_station)

    context.election_group = station.election_group
    context.public_organization = user.public_organization
  end

  private

  def find_station
    ObserversStation.find_by(
      user: user,
      election_group_id: station.election_group_id
    )
  end

  def observers_station_params
    collection = {
      station_id: station.id,
      user_id: user.id,
      certification_request: params[:certification_request]
    }
    if params[:certificate]
      collection.merge!(certificate: params[:certificate], certification: 'unapproved', accreditor_id: nil)
    end
    collection
  end

  def context_message(observers_station)
    message_type = observers_station.certification_request ? 'request' : 'success'
    context.message = I18n.t("messages.observers_stations.create.#{message_type}")
  end
end
