class Stations::ObserversStations::Add < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::PublicOrganizations::Users::Add,
           ::Stations::ObserversStations::Set,
           ::ElectionGroups::AddPublicOrganization
end
