class Users::Verification::SendSms < ApplicationInteractor
  uses_via_context :user

  def call
    # Change sms provider to smsc.kz
    return unless Rails.env.production?

    SmsGateway::Request.call(:send_sms, {
       recipient: user.phone,
       text: "Код для подтверждения: #{verification_code}."
     })
  end
end
