class Users::Verification::GenerateCode < ApplicationInteractor
  uses_via_context :user

  def call
    user.update(verification_code: new_verification_code)
  end

  private

  def new_verification_code
    SecureRandom.random_number(10**6).to_s.rjust(6, '0')
  end
end
