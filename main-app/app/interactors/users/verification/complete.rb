class Users::Verification::Complete < ApplicationInteractor
  uses_via_context :user, :code

  def call
    return user.confirm if user.verification_code == code

    context.fail!(message: I18n.t('errors.user.verification.fail'))
  end
end
