class Users::Verification::New
  include Interactor::Organizer
  include Transactable

  organize ::Users::Verification::GenerateCode
end
