class Election::Save < ApplicationInteractor
  uses_via_context :election

  def call
    election.save!
  end
end
