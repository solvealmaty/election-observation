class Election::RemoveStaleCandidates < ApplicationInteractor
  uses_via_context :election, :candidates

  def call
    return if candidates.blank?

    election.candidates.destroy_all
  end
end
