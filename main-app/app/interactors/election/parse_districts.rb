require 'csv'

class Election::ParseDistricts < ApplicationInteractor
  DISTRICT_FILE_HEADERS = ['level_1;level_2;level_3;level_4;address;coordinates'].freeze

  uses_via_context :districts, :parsed_districts

  before do
    context.parsed_districts = []
  end

  def call
    return if districts.blank?

    validate_structure

    CSV.foreach(districts, col_sep: ';', headers: true) { |row| parsed_districts << row }
  end

  private

  def headers
    CSV.read(districts).first
  end

  def validate_structure
    context.fail!(message: I18n.t('messages.election.parse.failure')) if headers != DISTRICT_FILE_HEADERS
  end
end
