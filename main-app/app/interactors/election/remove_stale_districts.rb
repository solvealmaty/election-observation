class Election::RemoveStaleDistricts < ApplicationInteractor
  uses_via_context :election, :election_group, :districts

  def call
    return if districts.blank?

    election.stations.clear
  end
end
