class Election::SaveDistricts < ApplicationInteractor
  uses_via_context :parsed_districts, :election, :election_group

  def call
    parsed_districts.each do |row|
      country    = Country.find_or_initialize_by(name: row['level_1'])
      region     = country.regions.find_or_initialize_by(name: row['level_2'])
      local_area = region.local_areas.find_or_initialize_by(name: row['level_3'])
      station    = local_area.stations.find_or_initialize_by(name: row['level_4'])
      station.assign_attributes(
        address: row['address'],
        coordinates: row['coordinates'],
        election_group: election_group,
      )

      election.stations << station
      election.save!
    end
  end
end
