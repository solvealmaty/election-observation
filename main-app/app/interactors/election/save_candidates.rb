class Election::SaveCandidates < ApplicationInteractor
  uses_via_context :parsed_candidates, :election

  def call
    parsed_candidates.map do |row|
      ensure_district_levels(row)
      district = find_or_create_district(row['level'])

      election.candidates << Candidate.new(
        name: row['candidate'],
        info: row['additional_info'],
        electoral_district: district
      )

      election.save!

      election_districts = election.electoral_districts
      unless election_districts.exists?(id: district.id)
        election.electoral_districts << district
      end
    end
  end

  private

  def ensure_district_levels(row)
    load_country(row)
    load_region(row)
    load_local_area(row)
  end

  def load_country(row)
    @country = Country.find_or_create_by!(name: row['country'])
  end

  def load_region(row)
    return if row['region'].blank?

    @region = Region.find_or_create_by!(name: row['region'], country: @country)
  end

  def load_local_area(row)
    return if row['local_area'].blank?

    @local_area = LocalArea.find_or_create_by!(name: row['local_area'], region: @region)
  end

  def find_or_create_district(level)
    scopeable = case level.to_i
                when 1
                  @country
                when 2
                  @region
                when 3
                  @local_area
                end

    ElectoralDistrict.find_or_create_by!(scopeable: scopeable)
  end
end
