class Election::Update < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::Election::EditAttributes,
           ::Election::RemoveStaleDistricts,
           ::Election::RemoveStaleCandidates,
           ::Election::ParseDistricts,
           ::Election::ParseCandidates,
           ::Election::SaveDistricts,
           ::Election::SaveCandidates,
           ::Election::SetReady,
           ::Election::Save,
           ::ElectionGroups::UpdateStatus
end
