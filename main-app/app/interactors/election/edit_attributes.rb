class Election::EditAttributes< ApplicationInteractor
  uses_via_context :election, :election_params

  def call
    election.assign_attributes(election_params)
  end
end
