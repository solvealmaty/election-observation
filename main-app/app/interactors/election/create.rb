class Election::Create < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::Election::BuildElection,
           ::Election::ParseDistricts,
           ::Election::ParseCandidates,
           ::Election::SaveDistricts,
           ::Election::SaveCandidates,
           ::Election::SetReady,
           ::Election::Save,
           ::ElectionGroups::UpdateStatus
end
