class Election::SetReady < ApplicationInteractor
  uses_via_context :election

  def call
    return unless election.draft?

    election.ready! if election.candidates_exist? && election.stations.present?
  end
end
