class Election::BuildElection< ApplicationInteractor
  uses_via_context :election_params

  def call
    context.election = Election.new(election_params).tap do |e|
      context.election_group = e.election_group
    end
  end
end
