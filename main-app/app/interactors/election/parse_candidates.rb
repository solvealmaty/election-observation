class Election::ParseCandidates < ApplicationInteractor
  CANDIDATES_FILE_HEADERS = ['level;country;region;local_area;candidate;additional_info'].freeze

  uses_via_context :candidates, :election, :parsed_candidates

  before do
    context.parsed_candidates = []
  end

  def call
    return if candidates.blank?

    validate_structure

    CSV.foreach(candidates, col_sep: ';', headers: true) { |row| parsed_candidates << row }
  end

  private

  def headers
    CSV.read(candidates).first
  end

  def validate_structure
    context.fail!(message: I18n.t('messages.election.parse.failure')) if headers != CANDIDATES_FILE_HEADERS
  end
end
