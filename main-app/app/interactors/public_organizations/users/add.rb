class PublicOrganizations::Users::Add < ApplicationInteractor
  uses_via_context :user, :params

  def call
    if org.nil?
      context.fail!(message: I18n.t('messages.election_group.add_public_organization.error'))
    end

    user.update!(public_organization: org)
  end

  private

  def org
    @org ||= PublicOrganization.find_by(id: params[:public_organization_id]) || default_org
  end

  def default_org
    PublicOrganization.order(:created_at).first
  end
end
