class LocalAreas::ParentLevels::UpdateRegion < ApplicationInteractor
  uses_via_context :params, :local_area

  def call
    local_area.region.update!(name: params[:region][:name])
  end
end
