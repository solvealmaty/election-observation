class LocalAreas::ParentLevels::UpdateCountry < ApplicationInteractor
  uses_via_context :params, :local_area

  def call
    local_area.country.update!(name: params[:country][:name])
  end
end
