class LocalAreas::ParentLevels::Update < ApplicationInteractor
  include Interactor::Organizer
  include Transactable

  organize ::LocalAreas::ParentLevels::UpdateCountry,
           ::LocalAreas::ParentLevels::UpdateRegion,
           ::LocalAreas::ParentLevels::UpdateLocalArea
end
