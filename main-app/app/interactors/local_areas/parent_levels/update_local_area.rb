class LocalAreas::ParentLevels::UpdateLocalArea < ApplicationInteractor
  uses_via_context :params, :local_area

  def call
    local_area.update!(name: params[:name])
  end
end
