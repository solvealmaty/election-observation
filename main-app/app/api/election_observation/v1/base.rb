module ElectionObservation
  module V1
    class Base < Grape::API
      mount ElectionObservation::V1::Mobile::Base => :client

      add_swagger_documentation \
        doc_version: '1.0.0',
        host: '',
        base_path: 'v1/',
        info: {
          title: 'ElectionObservation API',
          contact_name: 'Election Observation',
          contact_email: '',
          contact_url: ''
        }
    end
  end
end
