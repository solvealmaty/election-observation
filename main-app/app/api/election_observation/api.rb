class ElectionObservation::Api < Grape::API
  format :json
  default_format :json
  formatter :json, Grape::Formatter::ActiveModelSerializers

  mount ElectionObservation::V1::Base => 'v1'
end
